var w = window;


function save_user_lang_in_db() {

  window.we_lang = window.localStorage.getItem('window_we_lang');

  
  if ( window.we_lang == null ) {
    
    // AKO NEMA JEZIKA U LOCAL STORAGE ONDA JE DEFAULT = HR !!!!!!!!
    window.we_lang = 'hr';
    window.localStorage.setItem('window_we_lang', 'hr');
  };
  
  var user_number = null;

  // pretvori user objekt u local storageu  u  pravi js objekt
  var local_user = JSON.parse(window.localStorage.getItem('cit_user'));

  if ( local_user ) {

    user_number = local_user.user_number;


    var lang_data = {
      user_number: user_number,
      lang: window.we_lang
    };


    $.ajax({
      type: 'POST',
      url: window.remote_we_url + '/set_user_lang',
      data: JSON.stringify(lang_data),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function(response) {

      console.log(' spremljen current user lang ------------------ !!!!!!!' )
      console.log(response);
    })
    .fail(function(error) {

      console.error(' ERROR U AJAXU za spremanje current user lang ------------------ !!!!!!!' )
      console.log(error);

    });


  }; // kraj ako postoji user u local storage

};

// inicijalno kad se aplikacija pali spremi jezik u remote bazu 

save_user_lang_in_db();



var get_call_button = {
  
hr: function() {  
    
var btn = 
`
kliknite broj ispod za pomoć<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px;">00385 92 3133 015</a>

`;   
    
return btn;  
    
},
  
en: function() {  
    
var btn = 
`
click number below to call support<br>
<a href="tel:00385923133015" 
style="padding: 5px;
text-decoration: none;
color: black;
background-color: #fff;
margin-top: 10px;
display: block;
border-radius: 20px;">00385 92 3133 015</a>

`;   
    
return btn;  
    
},  
  
};



function we_trans( selector, lang, data ) {

  
  
  
var lang_object = {

  /* ---------------------- PARK LIST PAGE  ---------------------- */
  
  session_expired: {
hr: 
`
<h5>Sesija istekla!</h5>
<p style="text-align: center;">
Vaša sesija je istekla.<br>Molimo ulogirajte se ponovo.
<br>
</p>
`,
en:
`
<h5>Session expired!</h5>
<p style="text-align: center;">
Your session has expired.<br>Please log in again.
<br>
</p>
`,
    
},
  
  
  id__barrier_map_btn_tooltip: {
    hr: 'PRONAĐITE VAŠU BARIJERU NA TLOCRTU PARKIRALIŠTA',
    en: 'FIND YOUR BARRIER ON THE PARKING FLOOR PLAN',
  },  
  
  id__park_list_header_label: {
    hr: 'Parkirališta',
    en: 'Parkings',
  },
  
  cl__park_available_label: {
    hr: 'slobodno',
    en: 'available',
  },
  cl__navigate_button_label: {
    hr: 'ruta',
    en: 'route',
  },
  id__sort_by_distance: {
    hr: 'Po udaljenosti',
    en: 'By distance',
  },
  id__sort_by_price: {
    hr: 'Po cijeni',
    en: 'By price',
  },
  id__sort_by_price: {
    hr: 'Po cijeni',
    en: 'By price',
  },
  
  
 /* ---------------------- ACCOUNT PAGE  ---------------------- */  
    
  id__account_header_label: {
    hr: 'Profil',
    en: 'Account',
  },  
  
  id__monthly_products_label: {
    hr: 'Pretplate:',
    en: 'Subscriptions',
  },  
  
  'cl__monthly_valid_to span': {
    hr: 'NEOGRANIČENO',
    en: 'UNLIMITED',
  },
  
  'id__account_balance_label': {
    hr: 'Stanje racuna',
    en: 'Balance',
  },
  
  'id__account_balance_label': {
    hr: 'Stanje racuna',
    en: 'Balance',
  },
  
  'id__pay_with_credit_card_btn': {
    hr: 'PLATI KARTICOM',
    en: 'CREDIT CARD',
  },
  
  'id__offer_drop_list_explanation': {
    hr: 'Odaberite nadoplatu računa (sva parkirališta) ili mjesečnu pretplatu za specifično parkiralište.',
    en: 'Select an ADD TO BALANCE (all parking lots) or a monthly subscription for a specific parking.',
  },
  id__for_balance: {
    hr: 'NADOPLATI STANJE',
    en: 'ADD TO BALANCE',
  },
  id__1_month_sub_span: {
    hr: '1 mjesec',
    en: '1 month',
  },
  id__2_month_sub_span: {
    hr: '2 mjeseca',
    en: '2 months',
  },
  id__3_month_sub_span: {
    hr: '3 mjeseca',
    en: '3 months',
  },
  
  id__skracenica_mj: {
    hr: 'mj',
    en: 'mth',
  },
  id__offer_desc_voucher: {
    hr: 'Nadoplatite stanje na računu u iznosu vaučera.',
    en: 'Increase balance in voucher amount',
  }, 
  
  id__offer_desc_card: {
    hr: 'Iznad upišite za koliko želite povećati stanje (Iznos bez popusta)',
    en: 'Enter above how much you want to increase the balance (Amount without discount)',
  }, 
  
  id__offer_desc_subs: {
    hr:
    `
    Ukoliko unaprijed platite 1 - 3 mjeseca, ostvarujete popust od u odnosu na regularno plaćanje.<br>
    <b>Pretplata ne garantira rezervaciju mjesta!</b>
    `,    
    en:
    `
    If you pay subscription 1-3 months in advance,<br>you will receive a discount.<br>
    <b>A subscription it's not guarantee for reservation!</b>
    `,

  },   
  
  
  v__voucher_number_input: {
    hr: 'UPIŠI KOD VOUCHERA',
    en: 'ENTER VOUCHER CODE',
  },
  
  v__add_to_balance_input: {
    hr: 'UPIŠI IZNOS NADOPLATE',
    en: 'ADD AMOUNT TO BALANCE',
  },

  v__add_discount_code: {
    hr: 'UPIŠI KOD ZA POPUST',
    en: 'ENTER DISCOUNT CODE',
  },
  
   
  id__add_discount_code_label: {
    hr: 'Kod za ostvarivanje popusta',
    en: 'Enter discount code',
  },
  
  id__enter_card_label: {
    hr: 'Odaberite postojeću karticu',
    en: 'Choose saved credit card',
  },
    
  id__pay_with_token_btn: {
    hr: 'PLATI ODABRANOM KARTICOM',
    en: 'PAY WITH SELECTED CARD',
  },

  id__open_ws_pay_IAB: {
    hr: 'PLATI NOVOM KARTICOM',
    en: 'PAY WITH NEW CARD',
  },
  
  id__enter_voucher_button: {
    hr: 'POTVRDI VOUCHER',
    en: 'USE VOUCHER',
  },
  
  id__no_cards: {
    hr: 'NEMA KARTICA',
    en: 'NO SAVED CARDS',
  },
  
  /* ---------------------- PARK PAGE  ---------------------- */  
  
  
  id__book_pp_btn_span: {
    hr: 'REZERVIRAJ',
    en: 'START RESERVATION',
  },
  
  
  id__cancel_booking_btn_span: {
    hr: 'PREKINI REZERVACIJU',
    en: 'STOP RESERVATION',
  },  

  id__enter_pp_btn_span: {
    hr: 'UĐI',
    en: 'ENTER',
  },
  
  id__exit_pp_btn_span: {
    hr: 'IZAĐI',
    en: 'EXIT',
  },
  
  id__open_barrier_btn_span: {
    hr: 'SPUSTI',
    en: 'OPEN',
  },  
  
  id__show_barrier_map_span: {
    hr: 'BARIJERE',
    en: 'BARRIERS',
  },
  
  id__close_barrier_btn_span: {
    hr: 'PODIGNI',
    en: 'CLOSE',
  },  

  id__ramp_not_open_txt_1: {
    hr: 'Rampa se nije otvorila ?',
    en: "Barrier didn't open ?",
  },
  
  id__ramp_not_open_txt_2: {
    hr: 'Kliknite tel. broj na vrhu stranice.',
    en: "Click phone number on page top.",
  },  
  
  id__book_time_passed_span: {
    hr: 'PROTEKLO:',
    en: 'TIME PASSED:',
  },
  
  id__book_spent_span: {
    hr: 'POTROŠENO:',
    en: 'SPENT:',
  },
    
  id__parking_timer_label: {
    hr: 'PARKING U TIJEKU',
    en: 'PARKING IN PROGRES',
  },
  
  /* ---------------------- SIDE MENU  ---------------------- */  
  
  id__lang_hr_span: {
    hr: 'HRVATSKI',
    en: 'HRVATSKI',
  },
  
  id__lang_en_span: {
    hr: 'ENGLISH',
    en: 'ENGLISH',
  },
  
  id__side_menu_choose_lang_label: {
    hr: 'Izaberi jezik:',
    en: 'Choose language:',
  },
  
  
  /* ---------------------- POPUPS  ---------------------- */  
  
id__cit_popup_modal_YES: {
hr:
`DA`,
en:
`YES`,
},
  
  
id__cit_popup_modal_NO: {
hr:
`NE`,
en:
`NO`,
},  
  
updating_data: {
hr:
`AŽURIRAM PODATKE`,
en:
`UPDATING DATA`,
},

resetting_app: {
hr:
`RESETIRAM APLIKACIJU`,
en:
`RESETTING APP`,
},

saving_new_data: {
hr:
`SPREMAM PODATKE`,
en:
`SAVING DATA`,
},  
  
  
  
popup_text_total_time: {
hr:
`Ukupno vrijeme:`,
en:
`Total time:`,
},
  
popup_text_total_cost: {
hr:
`Ukupni trošak:`,
en:
`Total cost:`,
},   
  
popup_nedostaje_user: {
hr:
`
<h5>Nedostaje korisnik</h5>
<p>
Za ispravan rad aplikacije potreban je login ili registracija korisnika.
<br>
</p>
`,
en:
`
<h5>No user!</h5>
<p>
Application requires user login or registration to function properly.
<br>
</p>
`,
},
  
popup_no_cards: {
hr:
`
<h5>Nemate spremljenu niti jednu karticu</h5>
<p style="text-align: left;">
1. Kliknite na gumb PLATI NOVOM KARTICOM koji će vas odvesti na web servis WSPAY.<br>
2. Izaberite opciju ŽELIM KORISTITI TOKEN<br>
3. Sljedeći put možete direktno platiti iz aplikacije.<br>
<br>
</p>
`,
en:
`
<h5>You dont have any cards saved</h5>
<p style="text-align: left;">
1. Click the PAY WITH NEW CARD button and it will take you to the WSPAY web service.<br>
2. Select the  "I WANT TO USE TOKEN" option<br>
3. Next time, you can pay directly from the app!<br>
</p>
`,
},
  
popup_no_voucher_code: {
hr:
`
<h5>Nedostaje podatak!</h5>
<p style="text-align: center;">
Potrebno upisati broj vouchera!<br>
<br>
</p>
`,
en:
`
<h5>Data missing!</h5>
<p style="text-align: center;">
You need to enter voucher number!<br>
<br>
</p>
`,
},
  
closest_open_time_title: {
hr: `Sorry :( Parking je zatvoren!`,
en: `Sorry :( Parking is closed!`,
},
  
closest_open_time_text: {
hr: `Otvara se za:`,
en: 'Opens in:',
},
  
popup_become_regular_user: {
hr: function (data) {
var html = 
`
<h5>Nema više ${data.tag.toUpperCase()} mjesta na pakingu ${data.park.pp_name.toUpperCase()}!</h5>
<p>
Želite li postati običan korisnik?
<br>
</p>
`  
return html;
},
en: function (data) {
var html = 
`
<h5>There is no ${data.tag.toUpperCase()} parking lots available in ${data.park.pp_name.toUpperCase()} park!</h5>
<p>
Do you want to switch to regular user mode?
<br>
</p>
`  
return html;  
},  
},
  
  
  
popup_sve_pretplate_kupljene: {
  
hr: function (data) {
var html = 
`
<h5 style="margin: 20px 0 0">Sve pretplate za parking ${data.pp_name } su kupljene!</h5>
<p>
Trenutno ne možete kupiti pretplatu jer nema dostupnih mjesta. Ako želite rezervirati parking kada se mjesto oslobodi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`  
return html;
},
en: function (data) {
var html = 
`
<h5 style="margin: 20px 0 0">All ${data.pp_name } subscriptions have been purchased!</h5>
<p>
You cannot purchase a subscription at this time because there are no spaces available. If you wish to book parking when the space becomes available<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`  
return html;  
},  
},
  
  
popup_nemate_lovu_ili_pretplatu: {
hr:
`
<h5>Neuspjeli booking!</h5>
<p>
Nažalost nemate dovoljno sredstava na stanju ili niste pretplaćeni za ovo parkiralište!
<br>
</p>
`,
en:
`
<h5>Booking failed!</h5>
<p>
Unfortunately you do not have enough funds or you have not subscribed to this parking lot!
<br>
</p>
`,
},

popup_amount_nije_ispravan: {
hr:
`
<h5>Iznos nije ispravan</h5>
<p>
Molimo upišite pozitivan cijeli broj bez decimala.<br>
Minimalan iznos je 20 kn.
</p>
`,
en:
`
<h5>Amount is incorrect</h5>
<p>
Please enter a positive integer without decimals. <br>
Minimum amount is 20 kn.
</p>
`,
},
    
popup_greska_prilikom_bookinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške prilikom rezervacije :(<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
There was an error while booking :( <br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},
   
popup_greska_prilikom_parkinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške na serveru prilikom ulaska u parking :(<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
There was an error on server while entering parking :( <br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
  
popup_ramp_not_open: {
hr:
`
<h5 style="margin-bottom: 0;">Rampa ne reagira!</h5>
<p style="margin: 0;">
Molimo probajte još jednom kliknuti gumb.. Ukoliko se rampa i drugi put ne otvori<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">Barrier not responding!</h5>
<p style="margin: 0;">
Please try clicking the button again. If the ramp doesn't open again<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
  
  
popup_ramp_is_broken: {
hr:
`
<h5 style="margin-bottom: 0;">Rampa je u kvaru!</h5>
<p style="margin: 0;">
Ukoliko je rampa u zatvorenom položaju i ne možete izaći ili imate bilo koji drugi problem<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:

`
<h5 style="margin-bottom: 0;">Barrier malfunction!</h5>
<p style="margin: 0;">
If the ramp is in the closed position and you can't get out or you have any other problem<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
  
  
  
  
popup_ramp_is_maint: {
hr:
`
<h5 style="margin-bottom: 0;">Rampa izvan pogona zbog održavanja!</h5>
<p style="margin: 0;">
Ukoliko je rampa u zatvorenom položaju i ne možete izaći ili imate bilo koji drugi problem<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:

`
<h5 style="margin-bottom: 0;">Barrier not responding due to maintenance!</h5>
<p style="margin: 0;">
If the barrier is in the closed position and you can't get out or you have any other problem<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},    
  
    
  
popup_ramp_is_broken_booking: {
hr:
`
<h5 style="margin-bottom: 0;">Rampa je u kvaru!</h5>
<p style="margin: 0;">
Trenutno nije moguće rezervirati parking jer je rampa u kvaru. Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:

`
<h5 style="margin-bottom: 0;">Barrier malfunction!</h5>
<p style="margin: 0;">
It is currently not possible to reserve parking due to ramp malfunction. Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
 
  
  
popup_napon_too_low: {
hr:
`
<h2>Rampa je otvorena</h2>
<p>
Rampa je u otvorenom položaju zbog nedostatka električne struje.
<br>
</p>

`,
en: 
`
<h2>Barrier is open</h2>
<p>
The barrier is in the open position due to lack of electricity.
<br>
</p>
`,
}, 
  
  
popup_infra_red: {
hr:
`
<h2>Senzor kretanja aktiviran!</h2>
<p>
Aktiviran je senzor kretanja. Vjerojatno je netko prošao ispod rampe. Molimo pričekajte 10 sekundi i probajte ponovo.
<br>
</p>

`,
en: 
`
<h2>Motion sensor is activated!</h2>
<p>
The motion sensor is activated. Probably someone passed under the barrier. Please wait 10 seconds and try again.
<br>
</p>
`,
}, 
    
  
  
popup_fixed_close_state: {
hr:
`
<h2>Rampa je trajno zatvorena</h2>
<p>
Rampa se ne može otvoriti do daljnjega. Molimo vas za strpljenje.Ako ne možete izaći<br>
${get_call_button[w.we_lang]()}<br>
<br>
</p>

`,
en: 
`
<h2>Barrier permanently closed</h2>
<p>
The ramp cannot be opened until further notice. We ask for your patience. If you can't get out<br>
${get_call_button[w.we_lang]()}<br>
</p>
`,
},
  
popup_fixed_open_state: {
hr:
`
<h2>Rampa je trajno otvorena</h2>
<p>
Rampa je otvorena do daljnjega. Slobodno uđite. Ako se rampa kasnije zatvori, prilikom izlaska molimo nazovite broj na vrhu ekrana. Ili:<br>
${get_call_button[w.we_lang]()}<br>
<br>
</p>

`,
en: 
`
<h2>Barrier permanently open</h2>
<p>
The ramp is open until further notice. Feel free to enter. If the ramp closes later, please call the number on top of the screen when exiting. Or:<br>
${get_call_button[w.we_lang]()}<br>
</p>
`,
},  
  
barrier_not_open: {
hr:
`
<h5 style="margin-bottom: 0;">Barijera ne reagira!</h5>
<p style="margin: 0;">
Molimo probajte još jednom kliknuti gumb.. Ukoliko se barijera i drugi put ne otvori<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">Barrier not responding!</h5>
<p style="margin: 0;">
Please try clicking the button again. If the barrier doesn't open again<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
   
  
popup_nema_vise_mjesta: {
hr:
`
<h5>UH OH !</h5>
<p>
Upravo je zadnje mjesto zauzeto :(<br>
Probajte malo kasnije ....
<br>
</p>

`,
en:
`
<h5>UH OH !</h5>
<p>
Just now the last lot was taken :( <br>
Try again a little later….
<br>
</p>
`,
},  


  
popup_sve_barijere_zauzete: {
hr:
`
<h5>UH OH !</h5>
<p>
Upravo je zadnja barijera zauzeta :(<br>
Probajte malo kasnije ....
<br>
</p>

`,
en:
`
<h5>UH OH !</h5>
<p>
Just now the last barrier was taken :( <br>
Try again a little later….
<br>
</p>
`,
},  

  
popup_wait_1_min_to_exit: {
hr:
`
<h5>Molimo pričekajte 1 min!</h5>
<p>
Zbog sigurnosti elektronike i motora rampe, za izlazak je potrebno pričekati 1 minutu nakon ulaska.<br>
<br>
</p>
`,
en:
`
<h5>Please wait 1 min!</h5>
<p>
After opening barrier, to ensure the safety of the motor and electronics, it is necessary to wait 1 minute to exit.<br>
<br>
</p>
`,
},  
    
  
  
popup_wait_10_sec: {
hr:
`
<h5>Molimo pričekajte 10 sekundi!</h5>
<p>
Zbog sigurnosti elektronike i motora barijere, za ponovno aktiviranje je potrebno pričekati 10 sekundi!<br>
<br>
</p>
`,
en:
`
<h5>Please wait 10 sec!</h5>
<p>
After moving barrier, to ensure the safety of the motor and electronics, it is necessary to wait 10 seconds to move barrier again!<br>
<br>
</p>
`,
},  
    

  
postoji_vise_bookinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Zabilježeno je više rezervacija za isti parking. Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
Multiple reservations recorded for same parking. Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
}, 
  
popup_postoji_vise_parkinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Zabilježeno je više ulazaka u ovaj parking, ali bez izlaska! Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
There is more then one entry in this parking lot, but no exits! Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
}, 

  
  
vec_postoji_booking: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Zabilježeno je da već imate rezervaciju za ovaj parking. Za sva pitanja molimo<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
It is recorded taht you already booked this parking. If you have any questions<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
  
nema_bookinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Podatak o rezervaciji koju želite prekinuti je već obrisan! Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
The reservation  you want to cancel has already been deleted! Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
  
nema_parkinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu :(<br>
Podatak o ulazku u parkiralište je već obrisan. Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error :( <br>
Parking entry already deleted! Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
     
popup_update_products_error: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu prilikom ažuriranja stanja računa! Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error on account balance update. Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  

popup_greska_prilikom_CANCEL_bookinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu prilikom stopiranja rezervacije. Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error on reservation canceling. Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
     
      

popup_greska_prilikom_EXIT_parkinga: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške u sustavu prilikom stopiranja parkinga! Molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error on stopping parking session! Please<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
     
popup_app_nije_pronasao_BLE: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Aplikacija nije napravila konekciju s rampom<br>
Molimo probajte ponovo. Ako se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Application did not connect with barrier <br>
Please try again. If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
     
popup_BLE_is_OFF: {
hr:
`
<h5 style="margin-top: 0; margin-bottom: 7px;">UKLJUČITE BLUETOOTH I GPS</h5>
<p style="margin: 0; text-align: left; font-size: 13px;">
Za ispravan rad aplikacije potrebno je uključiti ova da servisa na ovaj način:<br>
<br>
1. Izađite is WePark aplikacije<br>
2. Otvorite postavke vašeg mobitela<br>
3. Uključite ova dva servisa<br>
</p>  
  
<div style="display: flex; justify-content: center; margin: 5px 0 0;">
  
<img style="width: auto; height: 60px; margin: 5px;" src="img/BLE_icon.png" />
<img style="width: auto; height: 60px; margin: 5px;" src="img/GPS_icon.png" />
</div>
<div style="font-size: 11px; text-align: center;">
  <span style=" font-size: 30px;
                position: relative;
                top: 13px;
                margin-right: 3px;
                font-weight: 700;
                color: #989898;
                line-height: 10px;
                display: inline-block;">*</span>

  Izgled ikona neće biti identičan.
</div>  


<p style="margin: 5px 0 0; text-align: left; font-size: 13px;">
4. Ponovo otvorite WePark aplikaciju<br>
5. Uživajte u parkingu :)<br>
</p>  
`,
en:
`
<h5 style="margin-top: 0; margin-bottom: 7px;">TURN ON BLUETOOTH & GPS</h5>
<p style="margin: 0; text-align: left; font-size: 13px;">
In order for the application to work properly, you must enable this services:<br>
<br>
1. Exit WePark application<br>
2. Open your settings<br>
3. Activate these two services<br>
</p>  
  
<div style="display: flex; justify-content: center; margin: 5px 0 0;">
  
<img style="width: auto; height: 60px; margin: 5px;" src="img/BLE_icon.png" />
<img style="width: auto; height: 60px; margin: 5px;" src="img/GPS_icon.png" />
</div>
<div style="font-size: 11px; text-align: center;">
  <span style=" font-size: 30px;
                position: relative;
                top: 13px;
                margin-right: 3px;
                font-weight: 700;
                color: #989898;
                line-height: 10px;
                display: inline-block;">*</span>

  Icons design will not be identical.
</div>  


<p style="margin: 5px 0 0; text-align: left; font-size: 13px;">
4. Reopen the WePark application<br>
5. Enjoy your parking :)<br>
</p> 
`,
},  
    
  
  
popup_GPS_is_OFF: {
hr:
`
<h5 style="margin-top: 0; margin-bottom: 7px;">UKLJUČITE GPS</h5>
<p style="margin: 0; text-align: left; font-size: 13px;">
Za ispravan rad aplikacije potrebno je uključiti GPS na ovaj način:<br>
<br>
1. Izađite is WePark aplikacije<br>
2. Otvorite postavke vašeg mobitela<br>
3. Uključite GPS servis<br>
</p>  
  
<div style="display: flex; justify-content: center; margin: 5px 0 0;">
  <img style="width: auto; height: 60px; margin: 5px;" src="img/GPS_icon.png" />
</div>
<div style="font-size: 11px; text-align: center;">
  <span style=" font-size: 30px;
                position: relative;
                top: 13px;
                margin-right: 3px;
                font-weight: 700;
                color: #989898;
                line-height: 10px;
                display: inline-block;">*</span>

  Izgled ikone neće biti identičan.
</div>  


<p style="margin: 5px 0 0; text-align: left; font-size: 13px;">
4. Ponovo otvorite WePark aplikaciju<br>
5. Uživajte u parkingu :)<br>
</p>  
`,
en:
`
<h5 style="margin-top: 0; margin-bottom: 7px;">TURN ON GPS</h5>
<p style="margin: 0; text-align: left; font-size: 13px;">
In order for the application to work properly, you must enable GPS service:<br>
<br>
1. Exit WePark application<br>
2. Open your settings<br>
3. Activate GPS service<br>
</p>  
  
<div style="display: flex; justify-content: center; margin: 5px 0 0;">
  
  <img style="width: auto; height: 60px; margin: 5px;" src="img/GPS_icon.png" />
</div>
<div style="font-size: 11px; text-align: center;">
  <span style=" font-size: 30px;
                position: relative;
                top: 13px;
                margin-right: 3px;
                font-weight: 700;
                color: #989898;
                line-height: 10px;
                display: inline-block;">*</span>

  Icon design will not be identical.
</div>  


<p style="margin: 5px 0 0; text-align: left; font-size: 13px;">
4. Reopen the WePark application<br>
5. Enjoy your parking :)<br>
</p> 
`,
},  
    
  
popup_nemate_google_ili_apple_maps: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p>
Aplikacija nije pronašla niti Google Maps (Android) niti Apple Maps (iOS). <br>
Stoga koristite "običan" Google Maps bez navigacije s ograničenim mogućnostima. <br>
Ukoliko želite koristiti napredne opcije navigacije molimo instalirajte jednu od gore navedenih aplikacija.
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p>
The application didn't found either Google Maps (Android) or Apple Maps (iOS).<br>
Therefore, app will use "ordinary" Google Maps without navigation feature.<br>
If you want to use advanced navigation options, please install one of the above applications.
<br>
</p>
`,
},  
       
  
  
popup_predaleko_od_rampe: {
hr:
`
<h5 style="margin-bottom: 0;">RAMPA PREVIŠE UDALJENA</h5>
<p>
Previše ste udaljeni od rampe! <br>
Za otvaranje rampe potrebno je biti u neposrednoj blizini!
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">TOO FAR!</h5>
<p>
You are too far from the ramp!<br>
To open the ramp you need to be close by!
<br>
</p>
`,
},  
  
     
popup_ERROR_on_BLE_scan: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
prilikom skeniranja rampe! <br>
Molimo probajte ponovo. Ako se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
System error! <br>
When scanning the barrier <br>
Please try again. If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
 
     
popup_no_amount_for_card_pay: {
hr:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="text-align: left; margin: 0;">
<span style="text-align: center; display: block;">Nedostaju podaci.</span>
1. Odaberite plaćanje voucherom ili kreditnom karticom.<br>
2. Zatim odaberite nadoplatu ili mjesečnu pretplatu za određeno parkiralište.<br>
3. Za nadoplatu s karticom potrebno je upisati iznos nadoplate.<br>
</p>

`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="text-align: left; margin: 0;">
<span style="text-align: center; display: block;">Data missing.</span>
1. Select a voucher or credit card payment. <br>
2. Then select a "add to balance" or a monthly payment for the selected parking. <br>
3. To increase balance via card payment, enter the amount in middle field. <br>
</p>
`,
},  
 

     
popup_pay_with_token_success: {
hr:
`
<h5 style="margin: 0;">Uplata je uspješna!</h5>
<p style="margin: 0;">
Račun će biti poslan na vašu email adresu.<br>
Zatvorite ovaj prozor i uživajte u super parking mjestu :)<br>
</p>

`,
en:
`
<h5 style="margin: 0;">Payment is successful!</h5>
<p style="margin: 0;">
The bill will be sent to your email address.<br>
Close this window and enjoy your parking space :)<br>
</p>
`,
},  
  
popup_pay_with_token_error:  {
hr: function(data) {
  
var html =   
`
<h5 style="margin-bottom: 0;">Greška u sustavu!</h5>
<p style="margin: 0;">
${ data || '' }<br>
Provjerite točnost svih podataka koje ste upisali.<br>
Ukoliko se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;
 
return  html;
  
},
en: function(data) {
  
  
var html =   
`
<h5 style="margin-bottom: 0;">System Error!</h5>
<p style="margin: 0;">
${ data || '' }<br>
Check the accuracy of all the information you have entered.<br>
If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;

return html;  
  
},
},
  

popup_pay_with_voucher_success: {
hr: function(data) {
  
var html  =  
`
<h5 style="margin-bottom: 0;">Uspješna registracija vouchera!</h5>
<p style="margin: 0;">
${data}<br>
Zatvorite ovaj prozor i uživajte u super parking mjestu :)<br>
<br>
</p>
`
return html;

},
en: function(data) {
  
var html  =  
`
<h5 style="margin-bottom: 0;">Voucher Validated!</h5>
<p style="margin: 0;">
${data}<br>
Close this window and enjoy a great parking space :)<br>
</p>
`
return html;

},
},    
    

popup_pay_with_voucher_error: {
hr: function(data) {
  
var html  =  '';
if ( data == '' ) {  
  
html = 
    `
<h5 style="margin: 0;">Došlo je do greške!</h5>
<p>
Molimo vas zatvorite ovaj prozor i probate ponovo.<br>
Provjerite točnost svih podataka koje ste upisali.<br>
Ukoliko se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;
  
} 
else {
  
html = 
`
<h5 style="margin: 0;">Voucher Greška</h5>
<p style="margin: 0;">
${data}<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;  

  
};

return html;

},
en: function(data) {
  
var html  =  '';
  
if ( data == '' ) {  
  
html = 
    `
<h5 style="margin: 0;">System Error!</h5>
<p>
Please close this window and try again. <br>
Check the accuracy of all the information you have entered.<br>
If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;
  
} 
else {
  
html = 
`
<h5 style="margin: 0;">Voucher Error</h5>
<p style="margin: 0;">
${data}<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;  

  
};

return html;

},
},    
    
popup_sva_polje_za_reg_pass_min_6: {
hr:
`
<h5 style="margin: 0;">Pogrešan upis!</h5>
<p>
Popunite sva polja za registraciju!<br>
Podaci ne smiju biti kraći od 6 znakova.<br>
</p>
`,
en:
`
<h5 style="margin: 0;">Wrong entry!</h5>
<p>
Fill in all the registration fields! <br>
Minimum length of each data is 6 characters. <br>
</p>
`,
},  
        
popup_email_nije_dobar: {
hr:
`
<h5 style="margin-bottom: 0;">Email nije ispravan!</h5>
<p style="margin-bottom: 0;">
Molimo vas provjerite ispravnost email adrese.<br>
Ukoliko ste sigurni da je email ispravan<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin-bottom: 0;">
Please check that your email address is correct.<br>
If you are sure email is correct<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
   
popup_lozinke_nisu_iste: {
hr:
`
<h5 style="margin-bottom: 0;">Kriva lozinka!</h5>
<p style="margin-bottom: 0;">
Lozinka koju ste ponovili nije ista kao lozinka u prvom polju!<br>
Ili je jedno od polja ostalo neupisano. Za sva pitanja<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">Wrong Password!</h5>
<p style="margin-bottom: 0;">
The password you repeated is not the same as the password in the first field! <br>
Or one of the fields is left empty. If you have any questions<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  
   
popup_error_reset_pass: {
hr:
`
<h5 style="margin-bottom: 0;">Došlo je do greške!</h5>
<p style="margin-bottom: 0;">
Molimo vas probajte ponovo poslati zahtjev za promjenu lozinke.<br>
Provjerite točnost svih podataka koje ste upisali.<br>
Ako se greška dogodi ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">Wrong Password!</h5>
<p style="margin-bottom: 0;">
Please try resending your password reset request.<br>
Check the accuracy of all the information you have entered.<br>
If the error occurs again<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
},  

popup_enter_pass_for_token: {
hr:
`
<h5 style="margin: 0;">Potrebna lozinka!</h5>
<p>
Za direktno plaćanje putem tokena (spremljene kartice), potrebno je upisati vašu lozinku.<br>
<br>
<div class="we_label" id="pass_for_token_label" style="clear: left; float: left;">Lozinka</div>
<input class="we_account_input" id="pass_for_token" type="password" autocomplete="off" required="" />  

</p>
`,
en:
`
<h5 style="margin: 0;">Password required!</h5>
<p>
For direct token payment (saved credit cards), you must enter your app password.<br>
<br>
<div class="we_label" id="pass_for_token_label" style="clear: left; float: left;">Password</div>
<input class="we_account_input" id="pass_for_token" type="password" autocomplete="off" required="" />  

</p>
`,
},  
  
popup_pass_for_token_error: {
hr: function(data) {
  
var html = 
    `
<h5 style="margin: 0;">Greška!</h5>
<p>
${data}<br>
Provjerite točnost svih podataka koje ste upisali.<br>
Ukoliko se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;

return html;

},
en: function(data) {
  
  
var html = 
`
<h5 style="margin: 0;">Error!</h5>
<p>
${data}<br>
Please close this window and try again.<br>
Check the accuracy of all the information you have entered.<br>
If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;

return html;

},
}, 
  
popup_no_user_by_email: {
hr: 
`
<h5 style="margin: 0;">Greška!</h5>
<p>
U bazi nismo našli korisnika s ovom email adresom.<br>
Provjerite točnost svih podataka koje ste upisali.<br>
Ukoliko se greška ponovi<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en: 
`
<h5 style="margin: 0;">Error!</h5>
<p>
We did not find a user with this email address in the database.<br>
Check the accuracy of all the information you have entered.<br>
If the error persists<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`
}, 
  
popup_reset_pass_check_email: {
hr: 
`
<h5 style="margin: 0;">Uspješan zahtjev za reset lozinke!</h5>
<p>
Na vašu email adresu je poslan email s uputama.<br>
Kliknom na gumb u emailu potvrdit ćete promjenu lozinke.<br>
</p>
`,
en: 
`
<h5 style = "margin: 0;">Successful password reset request! </h5>
<p>
Email for has been sent to your email address. <br>
By clicking on the button inside email you will confirm your password change. <br>
</p>
`
}, 
  
popup_discount_code_error: {
hr: 
`
<h5 style="margin: 0;">Greška!</h5>
<p>
Došlo je do greške na serveru prilikom pretrage koda za popust.<br>
Molimo vas probajte ponovo. Ukoliko opet dođe do greške molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en: 
`
<h5 style="margin: 0;">Error!</h5>
<p>
Server encountered an error while searching for the discount code.<br>
Please try again. If an error occurs again<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`
}, 
  
popup_discount_code_not_found: {
hr: 
`
<h5 style="margin: 0;">Greška!</h5>
<p>
Nismo našli ovaj kod u bazi :(<br>
Molimo vas probajte ponovo. Ukoliko opet dođe do greške molimo Vas<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en: 
`
<h5 style="margin: 0;">Error!</h5>
<p>
We did not find this code in the database :( <br>
Please try again. <br>
If an error occurs again<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`
}, 
  
popup_discount_used: {
hr: 
`
<h5 style="margin: 0;">Greška!</h5>
<p>
Ovaj kod je već iskorišten :(. Za sva pitanja<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`,
en: 
`
<h5 style="margin: 0;">Error!</h5>
<p>
This code is already used :(. If you have any questions<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`
}, 
  
popup_discount_code_success: {
hr: function(data) {
  
var html = 
`
<h5 style="margin: 0;">${data}% popusta!</h5>
<p>
Kod za popust je ispravan i s njim ćete ostvariti popust od ${data}%<br>
</p>
`;

return html;

},
en: function(data) {
  
  
var html = 
`
<h5 style="margin: 0;">${data}% discount!</h5>
<p>
The discount code is correct and you will receive a ${data}% discount!<br>
</p>
`;

return html;

},
}, 
 

popup_discount_nedovoljan_iznos: {
hr: function(data) {
  
var html = 
`
<h5 style="margin: 0;">Došlo je do greške!</h5>
<p>
Nemate dovoljan iznos za ovu pretplatu. Iznos koji ste upisali je ${data.iznos_uplate}kn, a cijena pretplate je ${data.prava_cijena}kn!<br>
Ukoliko imate pitanja<br>
${get_call_button[w.we_lang]()}
</p>
`;

return html;

},
en: function(data) {
  
var html = 
`
<h5 style="margin: 0;">Error!</h5>
<p>
You didn't enter sufficient amount for this subscription. The amount you have entered is ${data.iznos_uplate}kn and the subscription price is ${data.prava_cijena}kn! <br>
If you have questions<br>
${get_call_button[w.we_lang]()}
</p>
`;

return html;

},
}, 
 
  
/* ---------------------------- LOGIN FORM  ---------------------------- */  
  
  id__user_logout_button: {
    hr: 'ODJAVA',
    en: 'LOGOUT',
  },  
  
  id__msg_stari_korisnici_span: {
    hr: 
`
OBAVEZNA PROMJENA LOZINKE<br>
Ako ste korisnik stare verzije aplikacije, molimo vas kliknite LOGIN, zatim "Promijeni lozinku" i upišite svoju email adresu i novu lozinku koju želite.
<br><br>Unaprijedlili smo način osiguravanja lozinke te je ovaj korak neophodan kako bi osigurali maksimalnu zaštitu vaših podataka! 

`,
    en:
`
PASSWORD CHANGE REQUIRED<br>
If you are an previous app version user, please click LOGIN, then "Change password" and enter your email address and new password you prefer.
<br><br>We have improved the password security method and this step is necessary to ensure the maximum protection of your data!

`
,
  },
  
  
  id__user_name_label: {
    hr: 'Nadimak',
    en: 'Username',
  },  
  
  id__user_full_name_label: {
    hr: 'Ime i Prezime',
    en: 'Full Name',
  },  
  
  id__user_reg_plate_label: {
    hr: 'Dodaj registraciju',
    en: 'Add licence plate',
  },   
  
  id__user_reg_plate_menu_label: {
    hr: 'Dodaj registraciju',
    en: 'Add licence plate',
  },     
  
  vec_upisana_rega: {
    hr: 'Već ste upisali ovu registraciju.',
    en: 'You already saved this licence plate.',
  },   
  
  
  id__user_pass_label: {
    hr: function (data) {
      if ( !data ) { return 'Lozinka' };
      if ( data ) { return 'Upišite novu lozinku koju želite.' };
          
    },
    en: function (data) {
      if ( !data ) { return 'Password' };
      if ( data ) { return 'Enter new Password' };
          
    },
  },    
  
  id__repeat_user_pass_label: {
    hr: function (data) {
      if ( !data ) { return 'Ponovite lozinku' };
      if ( data ) { return 'Ponovite novu lozinku' };
          
    },
    en: function (data) {
      if ( !data ) { return 'Repeat Password' };
      if ( data ) { return 'Repeat new Password' };
          
    },
  },   
  
  
  user_new_pass_label: {
    hr: 'Upišite novu lozinku koju želite.',
    en: 'Enter the new password you want.',
  }, 
  
  
  id__user_register_button: {
    hr: 'REGISTRACIJA',
    en: 'REGISTER',
  },   
  
  id__show_register_form: {
    hr: 'REGISTRACIJA',
    en: 'REGISTER',
  },   
  
  
  id__i_have_account: {
    hr: 'Već imam račun',
    en: 'I have an Account',
  },
  
  id__send_email_reset_user_or_pass: {
    hr: 'POŠALJI EMAIL ZA POTVRDU',
    en: 'SEND CONFIRMATION EMAIL',
  },  
  
  id__forgot_user_pass_btn: {
    hr: 'Zaboravili ste lozinku?',
    en: 'Forgot password?',
  }, 
  
  id__text_under_register_btn: {
    hr: 'ili',
    en: 'or',
  },  
  
  
ask_for_r1: {
hr: function(data) {
  
  
  var html = 
`
<h5 style="margin: 0;">Želite R1 račun?</h5>
<p>
Za pravnu osobu potrebni su sljedeći podaci:<br>

<div id="business_forma" style="width: 100%; height: auto;">   
  
  <div class="we_label" id="business_oib_input_label" style="clear: left; float: left;">OIB Tvrtke</div>
  <input class="we_account_input" id="business_oib_input" type="text" autocomplete="off" required="" />   

  
  <div class="we_label" id="business_name_input_label" style="clear: left; float: left;">Naziv Tvrtke</div>
  <input class="we_account_input" id="business_name_input" type="text" autocomplete="off" required="" />  

  
  <div class="we_label" id="business_address_input_label" style="clear: left; float: left;">Adresa Tvrtke</div>
  <input class="we_account_input" id="business_address_input" type="text" autocomplete="off" required="" /> 

  
  <div class="we_label" id="business_mjesto_input_label" style="clear: left; float: left;">Poštanski broj i Mjesto Tvrtke</div>
  <input class="we_account_input" id="business_mjesto_input" type="text" autocomplete="off" required="" /> 
</div>

</p>
`;
return html;

},
en: function(data) {
  
  
  var html = 
`
<h5 style="margin: 0;">Generate receipt for company?</h5>
<p>
The following information is required for a business entity:<br>

<div id="business_forma" style="width: 100%; height: auto;">  
  
  <div class="we_label" id="business_oib_input_label" style="clear: left; float: left;">Company OIB</div>
  <input class="we_account_input" id="business_oib_input" type="text" autocomplete="off" required="" />   

  
  <div class="we_label" id="business_name_input_label" style="clear: left; float: left;">Company Name</div>
  <input class="we_account_input" id="business_name_input" type="text" autocomplete="off" required="" />  

  
  <div class="we_label" id="business_address_input_label" style="clear: left; float: left;">Company Address</div>
  <input class="we_account_input" id="business_address_input" type="text" autocomplete="off" required="" /> 

  
  <div class="we_label" id="business_mjesto_input_label" style="clear: left; float: left;">Company Postal Num. & City</div>
  <input class="we_account_input" id="business_mjesto_input" type="text" autocomplete="off" required="" /> 
</div>

</p>
`;
return html;

}
},   
  
popup_business_podaci_error: {
hr:
`
<h5>Greška poslovnih podataka!</h5>
<p>
Došlo je do greške u spremanju poslovnih podataka tvrtke!
<br>
</p>
`,
en:
`
<h5>Business Data Error!</h5>
<p>
An error occurred while saving your business information!
<br>
</p>
`,
  
},   
  
  
ime_usera_zauzeto: {
hr:
`
<h5 style="margin-bottom: 0;">SOORRY</h5>
<p style="margin: 0;">
Ovo korisničko ime je zauzeto :(<br><br>
Probaj neko drugo...<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">SOORRY</h5>
<p style="margin: 0;">
This username is taken :(<br><br>
Try another name ...<br>
</p>
`,
  
},   
  
  
  
  
email_usera_zauzet: {
hr:
`
<h5 style="margin-bottom: 0;">SOORRY</h5>
<p style="margin: 0;">
Ovaj email je zauzet :(<br><br>
Ako si se već registirao s tim mailom, probaj se prijaviti sa tim korisničkim računom...<br>
Ako si zaboravio ime i lozinku klikni na Promjena lozinke<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">SOORRY</h5>
<p style="margin: 0;">
This email is taken :( <br> <br>
If you already registered with this email, try to sign in with that account ... <br>
If you forgot your name and password click Change Password<br>
</p>
`,
  
},   
    
  
popup_registration_error: {
  
hr: function (data) { 

var html =   
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
Došlo je do greške prilikom registracije:<br>
${data}<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;

return html;

},
  
  
en: function (data) {
  
var html =   
`
<h5 style="margin-bottom: 0;">UH OH !</h5>
<p style="margin: 0;">
There was an registration error on server :( <br>
${data}<br>
${get_call_button[w.we_lang]()}
<br>
</p>
`;
  
return html;  
},
},  
  
  
welcome_new_user: {
hr:
`
<h5>Bok ${ window.cit_user ? window.cit_user.name : '' }!</h5>
<p>
Molim te pogledaj pristigli email i potvrdi registraciju tako da klikneš na veliki plavi gumb "POTVRDITE"<br>(na dnu email-a)<br><br>
Želimo ti ugodno korištenje WePark aplikacije!<br>
</p>
`,
en:
`
<h5>Hi ${ window.cit_user ? window.cit_user.name : '' }!</h5>
<p>
Please check email inbox and confirm registration by clicking big blue "CONFIRM" button <br> (bottom of email)<br><br>
We wish you the best experience using WePark app!<br>
</p>
`,
  
},  
  
  
  
pass_pp_iako_ramp_not_open: {
hr:
`
<h5 style="margin: 5px 0;">Rampa ne reagira</h5>
<p>
Aplikacija nije registrirala otvaranje rampe!
<br>
Jeste li već prošli kroz rampu?
<br>
Ako niste ušli, probajte još jednom. Ako drugi put ne bude radilo<br>
${get_call_button[w.we_lang]()}
</p>
`,
en:
`
<h5 style="margin: 5px 0;">Barrier not responding</h5>
<p>
App has not registered the barrier opening!
<br>
Have you already passed the barrier?
<br>
If you didn't, please try again. If it's not wotking again<br>
${get_call_button[w.we_lang]()}
</p>
`
  
},  
  
  
potvrda_bookinga: {
  
hr: function(data) {
  
var html =   
`
<h5>Rezervacija se naplaćuje!</h5>
<p>
Trenutna cijena razervacije po satu iznosi ${data}kn.
<br>
Jeste li sigurni da želite rezervirati?
<br>
</p>
`
return html;
},
en: function(data) {
  
var html =   
`
<h5>We charge for booking!</h5>
<p>
The current hourly rate is ${data}kn.
<br>
Are you sure you want to make a reservation?
<br>
</p>
`
return html;
},
  
  
},
  
  
covid_19_free_park: {
hr:
`
<h5>Besplatan parking</h5>
<p>
Besplatan parking na našim lokacijama:
<br>
Heinzlova (blizu Policijske stanice) i<br>
Prisavlje (kod HRT-a)<br>
za sve koji ne mogu ostati kod kuće.
<br>
</p>
`,
en:
`
<h5>Besplatan parking</h5>
<p>
Besplatan parking na našim lokacijama:
<br>
Heinzlova (blizu Policijske stanice) i<br>
Prisavlje (kod HRT-a)<br>
za sve koji ne mogu ostati kod kuće.
<br>
</p>
`
},   
  
  
  
popup_error_jel_nova_rega: {
  
hr:
`
<h5>Greška u provjeri</h5>
<p>
Došlo je do greške prilikom provjere registracije!
<br>
Molimo probajte ponovo.
</p>
`,
en:
`
<h5>Error</h5>
<p>
An error occurred while checking the licence plate!
<br>
Please try again.
</p>
`
},   
  
popup_vec_postoji_reg: {
  
hr:
`
<h5>Registraciju već netko koristi</h5>
<p>
Neki drugi korisnik je već upisao ovu registracijsku oznaku!
<br>
Samo jedan korisnik može koristiti istu reg. oznaku. 
</p>
`,
en:
`
<h5>Licence plate already in use</h5>
<p>
Another user has already registered this licence plate!
<br>
Only one user can use the same plate.
</p>
`
},   
  
plates_updated_on_all_ramps: {
  
hr:
`
<h5>Uspješno spremljeno</h5>
<p>
Registracije su ažurirane!
<br>
</p>
`,
en:
`
<h5>Update finished</h5>
<p>
Licence plates are updated!
<br>
</p>
`
},  
  
  
ramp_in_process: {
  
hr:
`
<h5>Rampa aktivna!</h5>
<p>
Rampa je u procesu gibanja!
<br>
Molimo pričekajte 5-6 sekundi jer je rampa u procesu gibanja tj. netko već prolazi kroz rampu.
<br>
</p>
`,
en:
`
<h5>Barrier active!</h5>
<p>
The barrier is in the process of opening/closing!
<br>
Please wait 5-6 seconds because the barrier is in the process of moving i.e. someone is already going through the barrier.
<br>
</p>
`
},   
  
  
  
error_on_saving_plates: {
  
hr:
`
<h5>Nije spremljeno</h5>
<p>
Nažalost tablice nisu spremljene u bazu.
<br>
Molimo probajte ponovo.
<br>
</p>
`,
en:
`
<h5>Update error</h5>
<p>
Licence plates not updated!
<br>
Please try again.
<br>
</p>
`
},  
    
  
obavezna_barem_jedna_rega: {
  
hr:
`
<h5>Obavezan podatak</h5>
<p>
Obavezno je upisati barem jednu registracijsku oznaku!<br>
Upišite registracijsku oznaku i kliknite 
<i class="fas fa-plus" 
style=" padding: 0 7px;
        background-color: #2cbcdc;
        height: 20px;
        line-height: 20px;
        color: white;
        border-radius: 10px;"></i> gumb pokraj!
<br>
</p>
`,
en:
`
<h5>Required information</h5>
<p>
It is mandatory to enter at least one licence plate!<br>
Enter the licence plate and click the 
<i class="fas fa-plus" 
style=" padding: 0 7px;
        background-color: #2cbcdc;
        height: 20px;
        line-height: 20px;
        color: white;
        border-radius: 10px;"></i> button!
<br>
</p>
`
}, 

  
popup_dosadno_1_rega: {
  
hr:
`
<h5 style="margin-bottom: 0;">Registracijska oznaka</h5>
<p style="font-size: 14px; font-weight: 600;">
Potrebno je upisati barem jednu registraciju jer će WePark ubrzo koristiti kamere za ulazak na parkiralište.<br>
Kliknete ikonu <i class="fas fa-bars"></i> u gornjem desnom kutu. Zatim upišite registraciju u polje "Dodaj registraciju" i kliknite 
<i class="fas fa-plus" 
style=" padding: 0 7px;
        background-color: #2cbcdc;
        height: 20px;
        line-height: 20px;
        color: white;
        border-radius: 10px;"></i> gumb pokraj.
<br>
</p>
`,
en:
`
<h5 style="margin-bottom: 0;">Licence plate required</h5>
<p style="font-size: 14px; font-weight: 600;">
At least one licence plate is required as WePark will soon be using cameras for entering into parking lot.<br>
Click the menu icon <i class="fas fa-bars"></i> in the upper right corner. Then enter the licence in the "Add licence plate" field and click the 
<i class="fas fa-plus" 
style=" padding: 0 7px;
        background-color: #2cbcdc;
        height: 20px;
        line-height: 20px;
        color: white;
        border-radius: 10px;"></i> button next to it.
<br>
</p>
`
  
}, 
  
    
};
  
window.global_we_trans_object = lang_object;  
  
  if ( typeof lang_object[selector][lang] == 'function') {
      
    if ( data || data == 0 ) return lang_object[selector][lang](data);
    if ( !data ) return lang_object[selector][lang]();
  } else {
    return lang_object[selector][lang];
  };

  
  
}; // kraj we trans funkcije


function run_we_trans() {
    
  $.each(window.global_we_trans_object, function(elem_selector, langs) {
    
    var sign = '';
    var selector = elem_selector;
  
    if (elem_selector.substring(0,4) == 'id__') {
      sign = '#';
      selector = elem_selector.replace('id__', '');
    };
    
    if (elem_selector.substring(0,4) == 'cl__') {
      sign = '.';
      selector = elem_selector.replace('cl__', '');
    };
    
    $(sign+selector).html(langs[window.we_lang]);
    
  });
  
  
  
  /* ------------------- OVO JE ZA PROMJENU PLACEHOLDER TEXTOVA U INPUT POLJIMA NA ACCOUNT PAGE ------------------ */
  // loadaj account module po njegovo url-u
  var account_module = window['js/account/account_module.js'];
  
  $.each(account_module.desc_input_place_holders, function(elem_id, stari_text ) {
    // postavi novi jezik na svaki property ( property odgovara idju od input polja )
    var novi_text = window.we_trans( 'v__'+elem_id, window.we_lang);
    account_module.desc_input_place_holders[elem_id] = novi_text;
    
    $('#' + elem_id).attr('value', novi_text );
    
    // provjeri koji je stari tj trenutni jezik
    var current_placeholeder_lang = $('#' + elem_id).attr('data-lang').replace('lang_', '');
    
    var current_text = window.we_trans( 'v__'+elem_id, current_placeholeder_lang);
    // ako input ima value koji je jednak placeholder text ali na jeziku koji je current
    // ili ako je value on input polja prazno
    if ( $('#'+elem_id).val() == current_text || $('#'+elem_id).val() == '' ) {
      // onda promjeni stari placeholder text u novi text
      $('#' + elem_id).val(novi_text);
    };
    
    // na kraju postavi novi jezik u data atribut za sljedeće provjere
    $('#' + elem_id).attr('data-lang', 'lang_'+window.we_lang);
    
  });
  
  
  
  /* ------------------- OVO JE ZA PROMJENU ŠIRINE KADA SE PROMJENI TEXT U DROP LISTAMA  ------------------ */
  
  // daj malo vremena da se promjeni tekst
  setTimeout( function() {
    var item_list = $('#sort_park_places .we_drop_list_items');
    // vidi širinu container za listu
    var list_width = item_list.width();
    // parent element tj ono što izgleda kao input element raširi da bude iste širine kao lista
    // TO MORAM NAPRAVITI JER JE LISTA APSOLUTNO POZICIONIRANA I ZBOG TOGA PARENT SE NEĆE RAZVUĆI NA ŠIRINU LISTE !!!!!!!
    $('#sort_park_places').width(list_width);
    
    
    var item_list = $('#choose_offer_list .we_drop_list_items');
    // vidi širinu container za listu
    var list_width = item_list.width();
    // parent element tj ono što izgleda kao input element raširi da bude iste širine kao lista
    // TO MORAM NAPRAVITI JER JE LISTA APSOLUTNO POZICIONIRANA I ZBOG TOGA PARENT SE NEĆE RAZVUĆI NA ŠIRINU LISTE !!!!!!!
    $('#choose_offer_list').width(list_width);
    
    
  }, 100);

  
};



