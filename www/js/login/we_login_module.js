var module_object = {
  create: function (data, parent_element, placement) {
    var component_id = null;


    var initial_display = 'none';

    // napravio sam tipfeler i umjesto first sam napisao frist :)
    // sad cu obrisati key sa tipfelerom i napisati novi key kako treba
    var we_frist_time_login = window.localStorage.getItem('we_frist_time_login');
    if (we_frist_time_login !== null) {
      window.localStorage.removeItem('we_frist_time_login');
      window.localStorage.setItem('we_1_time_login', 'true');
    };

    var we_1_time_login = window.localStorage.getItem('we_1_time_login');


    if (we_1_time_login !== null) {

      initial_display = 'block';
      $("#msg_stari_korisnici_box").css('display', 'none');

    } else {

      /*
      -----------------------------------------------------------------------------------------
      VAŽNO -------> NIKADA VIŠE NEMOJ PRIKAZIVATI MSG ZA STARE KORISNIKE !!!!!
      ostavio sam ovo ako se kasnije pokaže potreba da iskoristim taj div za nešto drugo
      -----------------------------------------------------------------------------------------
      */
      // prije je bilo block
      $("#msg_stari_korisnici_box").css('display', 'none');

    };

    // samo skraćujem jer mi se neda pisati :)
    var w = window;

    var component_html =
      `
<div class="we_label" id="user_name_label" style="clear: left; float: left; display: ${initial_display};">
  ${w.we_trans( 'id__user_name_label', w.we_lang) }
</div>
<input class="we_account_input" id="user_name" style="display: ${initial_display};" type="text" autocomplete="off" required>

<div id="full_name_and_email_section" style="display: ${initial_display};">
  
  <div class="we_label" id="user_full_name_label" style="clear: left; float: left;">
    ${w.we_trans( 'id__user_full_name_label', w.we_lang) }
  </div>
  <input class="we_account_input" id="user_full_name" type="text" autocomplete="off" required>

  <div class="we_label" id="user_email_label" style="clear: left; float: left;">E-mail</div>
  <input class="we_account_input" id="user_email" type="email" autocomplete="off" required>


<div id="reg_plate_sub_section" style="float: left; width: 100%; clear: both; margin-bottom: 10px;">  

  <div id="reg_plate_label_and_input_box" style="float: left; width: 100%; clear: both; margin-bottom: 10px;">

    <div class="we_label" id="user_reg_plate_label" style="clear: left;float: left;width: 80%;">
      ${w.we_trans( 'id__user_reg_plate_label', w.we_lang) }
    </div>
    <input class="we_account_input reg_plate_input" id="user_reg_plate" type="text" autocomplete="off" 
           style="width: 80%; float: left; margin: 0;" required>
    <div class="account_form_btn add_reg_plate_btn" id="add_reg_plate_btn"><i class="fas fa-plus"></i></div>

  </div>


</div>    


</div>

<div id="pass_section" style="height: auto; overflow: hidden; display: ${initial_display};">

  <div class="we_label" id="user_pass_label" style="clear: left; float: left;">
    ${w.we_trans( 'id__user_pass_label', w.we_lang )}
  </div>
  <input class="we_account_input" id="user_pass" type="password" autocomplete="off" required>  

  <div class="we_label" id="repeat_user_pass_label" style="clear: left; float: left;">
    ${w.we_trans( 'id__repeat_user_pass_label', w.we_lang) }
  </div>
  <input class="we_account_input" id="repeat_user_pass" type="password" autocomplete="off" required>  

</div>


  <!--
  <div class="we_label" id="account_address_label" style="clear: left; float: left;">Address</div>
  <input class="we_account_input" id="account_address" type="text" autocomplete="off">

  <div class="we_label" id="account_city_label" style="clear: left; float: left;">City</div>
  <input class="we_account_input" id="account_city" type="text" autocomplete="off">

  <div class="we_label" id="account_postal_label" style="clear: left; float: left;">Postal Code</div>
  <input class="we_account_input" id="account_postal" type="text" autocomplete="off">  

  <div class="we_label" id="account_country_label" style="clear: left; float: left;">Country</div>
  <input class="we_account_input" id="account_country" type="text" autocomplete="off">
  -->

  <div id="logout_and_edit" style="display: flex; align-items: center;">

    <div style="display: none; flex: 1; margin: 20px 5px 0;" class="account_form_btn" id="user_logout_button">
      ${w.we_trans( 'id__user_logout_button', w.we_lang) }
    </div>

    <!-- <div style="display: none; flex: 1; margin: 20px 5px 0;" class="account_form_btn" id="user_edit_button">EDIT</div> -->

  </div>



  <div id="login_register_save_container" style="display: ${initial_display};">

    <div style="display: none;" class="account_form_btn" id="user_login_button">LOGIN</div>

    <div style="display: none;" class="account_form_btn" id="user_register_button">
      ${w.we_trans( 'id__user_register_button', w.we_lang) }
    </div>


    <div style="display: none;" class="account_form_btn" id="user_save_new_data_button">SAVE NEW DATA</div>

    <div  style="display: none; background-color: transparent;"
          class="account_form_btn" id="i_have_account">${w.we_trans( 'id__i_have_account', w.we_lang) }</div>


    <div  style="display: ${initial_display == 'block' ? 'flex' : 'none' }; font-size: 13px;" 
          class="account_form_btn" id="send_email_reset_user_or_pass">
      ${w.we_trans( 'id__send_email_reset_user_or_pass', w.we_lang) }
    </div>


    <div  style="display: ${initial_display == 'block' ? 'flex' : 'none' }; background-color: transparent;"
          class="account_form_btn" id="forgot_user_pass_btn">
      ${w.we_trans( 'id__forgot_user_pass_btn', w.we_lang) }
    </div>



  </div>


  <div id="show_login_or_register_box" style="display: ${initial_display == 'none' ? 'block' : 'none' }">


    <div id="msg_stari_korisnici_box" style="display: none;">
      <i class="fas fa-exclamation-triangle"></i>
      <span id="msg_stari_korisnici_span">
        ${w.we_trans( 'id__msg_stari_korisnici_span', w.we_lang) }
      </span>
    </div>

    <div id="show_register_form">${w.we_trans( 'id__show_register_form', w.we_lang) }</div>

    <span id="text_under_register_btn" style="font-size: 10px; margin-bottom: 2px; display: block;">
      ${w.we_trans( 'id__text_under_register_btn', w.we_lang) }
    </span>

    <div id="show_login_form">LOGIN</div>


  </div>



  `;


    wait_for(`$('#user_login_button').length > 0`, function () {


      var this_module = window[module_url];

      var popup_modal = window['js/popup_modal/cit_popup_modal.js'];

      var show_popup_modal = popup_modal.show_popup_modal;

      this_module.check_user();

      if (window.cit_user) this_module.monitor_finger_click(true);

      if ( !window.car_plate_registration_list_in_progress) {
        window.car_plate_registration_list_in_progress = true;
        generate_car_plate_list('registration');
      };


      $('#add_reg_plate_btn').off('click');
      $('#add_reg_plate_btn').on('click', function (e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        // nemoj pozivati poseban request za update registracija
        // zato jer je to novi user i nema smisla
        // to ionako radim na serveru kada spremim ovog novog usera !!!!
        window.update_car_plates_in_db = false;
        
        // ako čitam string iz input polja
        // ovaj regex znači ostavi samo alfanumeric ali ipak ostavi i hrvatske znakove !!!
        var car_plate_text = $('#user_reg_plate').val().replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '');
        // moram dodatno obrisati underscore jer iznekog razloga on se zvanično tretira kao alphanumeric ????? !!!!!!!
        car_plate_text = car_plate_text.replace(/\_/g, '');
        car_plate_text = car_plate_text.toUpperCase();

        if ( car_plate_text.length < 5 ) return;
        
        check_jel_nova_rega(null, car_plate_text)
        .then(
          function(result) { if (result == true) global_add_car_plate('registration') },
          function(fail) { conole.error(fail) }
        );
        
      });


      $('#user_login_button').off('click');
      $('#user_login_button').on('click', function (e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.user_login();
      });


      $('#forgot_user_pass_btn').off('click');
      $('#forgot_user_pass_btn').on('click', function (e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.show_recover_user_or_pass();
      });



      $('#user_register_button').off('click');
      $('#user_register_button').on('click', function (e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.user_registration();
      });

      /* //////////////////////// EVENT LSITENER PRESS ENTER KEY ON PASS FIELD (to login) //////////////////////// */
      $('#user_pass').off('keypress');
      $("#user_pass").on('keypress', function (e) {
        
        /*
        e.stopPropagation();
        e.preventDefault();
        */
        
        if (e.keyCode === 13) this_module.user_login();
      });

      $('#user_logout_button').off('click');
      $("#user_logout_button").on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.user_logOUT();
      });


      $('#show_register_form').off('click');
      $("#show_register_form").on('click', function(e) { 
        
        e.stopPropagation();
        e.preventDefault();
        this_module.show_reg_form();
      });


      $('#show_login_form').off('click');
      $("#show_login_form").on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.show_log_form();
      });


      $('#i_have_account').off('click');
      $("#i_have_account").on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.show_log_form();
      
      });


      $('#send_email_reset_user_or_pass').off('click');
      $("#send_email_reset_user_or_pass").on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        this_module.run_user_pass_reset();
      });


    }, 500000);




    if (parent_element) {
      cit_place_component(parent_element, component_html, placement);
    };

    return {
      html: component_html,
      id: component_id
    };


  },

  scripts: function () {


    // VAŽNO !!!!  
    // module_url se definira na početku svakog injetktiranja modula u html
    // to se nalazi u main_script.js unutra funkcije get_module  
    var this_module = window[module_url];


    function monitor_finger_click(turn_on) {
      var monitor_state = window.localStorage.getItem('we_monitor_state');

      function prati_sve_klikove(e) {
        // ako je klik na bilo koji element unutar account forme
        // onda ignoriraj klik
        if ( $(e.target).closest('#account_form').length == 0 ) {
          console.log('big_page klik za monitor !!!!!!!!!!!!!!!!!!!!!!');
          this_module.check_user();
        };
      };


      if ( turn_on && monitor_state !== 'on') {

        window.localStorage.setItem('we_monitor_state', 'on');
        $(window).on('click', prati_sve_klikove);

      } else {

        $(window).off('click', prati_sve_klikove);
        console.log('STOPIRAN monitor ---------------------------');
        window.localStorage.removeItem('we_monitor_state');
      };

    }; // end of monitor mouse move
    this_module.monitor_finger_click = monitor_finger_click;

    /* /////////////////// START //////////////////////// WHEN LOGGED OUT  /////////////////////////////////////////// */

    function user_loggedOut_UI(formUI) {

      $("#account_balance_box").css('display', 'none');
      $("#monthly_products_box").css('display', 'none');

      $("#forgot_user_pass_btn").css('display', 'flex');


      $("#user_pass_label").text(w.we_trans('id__user_pass_label', w.we_lang));
      $("#repeat_user_pass_label").text(w.we_trans('id__repeat_user_pass_label', w.we_lang));

      $("#account_form input").css({
        'border-color': '#fff',
        'pointer-events': 'all'
      });

      // show login / register form

      if (formUI == 'login_form') {

        $("#user_login_button").css('display', 'flex');

        $("#i_have_account").css('display', 'none');


        $("#user_register_button").css('display', 'none');
        $("#user_edit_button").css('display', 'none');
        $("#user_logout_button").css('display', 'none');
        $("#user_save_new_data_button").css('display', 'none');

        $("#send_email_reset_user_or_pass").css('display', 'none');


        $("#full_name_and_email_section").css({
          'height': '0px',
          'max-height': '0px',
          'pointer-events': 'none'
        });

        $("#pass_section").css({
          'height': '200px',
          'max-height': '48px',
          'pointer-events': 'all'
        });

        $("#repeat_user_pass").css({
          'display': 'none'
        });

        $("#repeat_user_pass_label").css({
          'display': 'none'
        });


        // ------------------ PRIKAŽI SAMO REGISTER LINK !!!!!!  
        $("#show_login_or_register_box").css({
          'display': 'block',
          'margin-top': '20px'
        });

        $("#show_register_form").css({
          'display': 'block',
        });

        $("#text_under_register_btn").css({
          'display': 'none',
        });

        $("#show_login_form").css({
          'display': 'none',
        });

      };

      if (formUI == 'register_form') {



        $("#i_have_account").css('display', 'flex');

        $("#user_login_button").css('display', 'none');


        $("#user_register_button").css('display', 'flex');

        $("#user_edit_button").css('display', 'none');
        $("#user_logout_button").css('display', 'none');
        $("#user_save_new_data_button").css('display', 'none');
        $("#send_email_reset_user_or_pass").css('display', 'none');

        /*
        $("#full_name_and_email_section").css({
          'height': '150px',
          'max-height': '150px',
          'pointer-events': 'all'
        });
        */

        $("#full_name_and_email_section").css({
          'display': 'block',
          'max-height': 'unset',
          'height': 'auto',
          'pointer-events': 'all'
        });




        $("#user_full_name_label").css('display', 'block');
        $("#user_full_name").css('display', 'block');

        $("#reg_plate_sub_section").css('display', 'block');





        $("#pass_section").css({
          'height': '200px',
          'max-height': '96px',
          'pointer-events': 'all'
        });


        $("#repeat_user_pass").css({
          'display': 'block'
        });

        $("#repeat_user_pass_label").css({
          'display': 'block'
        });


      };

      // ---------------------------- SAKRIJ REGISTRACIJSKI INPUT GUMBE I REMOVAJ LISTU
      $("#reg_plate_menu_section").css({
        'display': 'none',
      });
      $(`#reg_plate_menu_section .reg_plate`).remove();




    }; // end of user loggedOut UI
    this_module.user_loggedOut_UI = user_loggedOut_UI;

    /* /////////////////// START //////////////////////// WHEN LOGGED IN  /////////////////////////////////////////// */


    window.last_time_check_user_products = 0;


    function set_GUI_for_logged_user() {

      // $("#account_balance_box").css('display', 'flex');
      // $("#monthly_products_box").css('display', 'flex');

      $("#user_name").val(window.cit_user.name);


      $("#account_form input").css({
        'border-color': 'transparent',
        'pointer-events': 'none'
      });

      $("#pass_section").css({
        'height': '0px',
        'max-height': '0px',
        'pointer-events': 'none'
      });


      $("#full_name_and_email_section").css({
        'height': '0px',
        'max-height': '0px',
        'pointer-events': 'none'
      });


      $("#user_logout_button").css('display', 'flex');


      $("#user_login_button").css('display', 'none');
      $("#user_register_button").css('display', 'none');
      $("#user_edit_button").css('display', 'none');
      $("#user_save_new_data_button").css('display', 'none');
      $("#send_email_reset_user_or_pass").css('display', 'none');
      $("#forgot_user_pass_btn").css('display', 'none');




      // ---------------------- SAKRIJ I LOGIN I REGISTER LINK
      $("#show_login_or_register_box").css({
        'display': 'none',
      });


      $("#show_register_form").css({
        'display': 'none',
      });

      $("#text_under_register_btn").css({
        'display': 'none',
      });

      $("#show_login_form").css({
        'display': 'none',
      });


    };

    this_module.set_GUI_for_logged_user = set_GUI_for_logged_user;

    
    
    function user_loggedIn_UI() {

      window.user_logged = true;

      $("#user_pass_label").text(w.we_trans('id__user_pass_label', w.we_lang));
      $("#repeat_user_pass_label").text(w.we_trans('id__repeat_user_pass_label', w.we_lang));


      // ako je prošlo više od 120 sekundi
      if (Date.now() - window.last_time_check_user_products > 120 * 1000) {

        window.last_time_check_user_products = Date.now();

        get_user_products_state(window.cit_user.user_number).then(
          function (response) {
            // svaki put kad kliknem na gumb za park list  uvijek iznova kreiram listu
            var account_module = window['js/account/account_module.js'];

            account_module.create_monthly_list(response);
            account_module.update_user_balance(response);
            account_module.create_card_num_list(response);

            setTimeout(function () {
              account_module.initial_setup_of_customer_credit_card_list();
            }, 300);

            account_module.initial_setup_of_choose_offer_list();

          },
          function (error) {
            console.log(error);
          }
        );
      }

      setTimeout(function () {

        this_module.set_GUI_for_logged_user();


      }, 500);


      setTimeout(function () {

        // ---------------------------- PRIKAŽI REGISTRACIJSKI INPUT GUMBE I LISTU
        $("#reg_plate_menu_section").css({
          'display': 'block',
        });


        if ( !window.car_plate_menu_list_in_progress) {
          window.car_plate_menu_list_in_progress = true;
          generate_car_plate_list('menu');
        };
        
        
      }, 1500);


    }; // end of user  loggedIn UI
    this_module.user_loggedIn_UI = user_loggedIn_UI;


    function user_registration() {

      // disable gumb za reg da nebude dva klika za redom 
      $("#user_register_button").addClass('cit_disable');


      $("#msg_stari_korisnici_box").css('display', 'none');


      var popup_modal = window['js/popup_modal/cit_popup_modal.js'];

      var show_popup_modal = popup_modal.show_popup_modal;


      var all_registration_fileds = ['user_name', 'user_pass', 'repeat_user_pass', 'user_full_name', 'user_email'];
      var too_short = "";
      var pass_repeat_same = false;

      $('#account_form .we_account_input').css('border', '');

      for (var i = 0; i < all_registration_fileds.length; i++) {
        if ($('#' + all_registration_fileds[i]).val().length < 6) {
          too_short += all_registration_fileds[i] + ', ';
          $('#' + all_registration_fileds[i]).css('border', '1px solid #ff0058');
        };
      };

      if (too_short !== "") {
        popup_sva_polje_za_reg_pass_min_6()

        $("#user_register_button").removeClass('cit_disable');

        return;
      };


      if (

        $('#user_pass').val() == '' ||
        $('#repeat_user_pass').val() == '' ||
        $('#user_pass').val() !== $('#repeat_user_pass').val()

      ) {
        popup_lozinke_nisu_iste();
        $('#account_form .we_account_input').css('border', '');
        $('#user_pass').css('border', '1px solid #ff0058');
        $('#repeat_user_pass').css('border', '1px solid #ff0058');

        $("#user_register_button").removeClass('cit_disable');

        return;
      };

      var mail = $('#user_email').val();
      var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if (is_email_ok === -1 || mail.indexOf('.') === -1) {

        popup_email_nije_dobar();
        $('#account_form .we_account_input').css('border', '');
        $('#user_email').css('border', '1px solid #ff0058');
        
        
        $("#user_register_button").removeClass('cit_disable');
        return;
      };


      var user_car_plates = window.localStorage.getItem('user_car_plates');
      if (user_car_plates) user_car_plates = JSON.parse(user_car_plates);

      // ako user nije ništa niti upisao !!!!
      if ( !user_car_plates ) {
        popup_obavezna_barem_jedna_rega();
        $("#user_register_button").removeClass('cit_disable');
        return;
      };

      // resetiraj na null ako je zapisan kao prazan array
      if ( $.isArray(user_car_plates) && user_car_plates.length == 0) {
        popup_obavezna_barem_jedna_rega();
        
        $("#user_register_button").removeClass('cit_disable');
        return;
      };
      

      var time_now = Date.now();

      var user_form_data = {

        user_saved: null,
        user_edited: null,

        saved: time_now,
        edited: time_now,
        last_login: time_now,

        user_address: '',
        user_tel: '',

        business_name: '',
        business_address: '',
        business_email: '',
        business_tel: '',
        business_contact_name: '',

        oib: null,
        iban: null,
        gdpr: true,

        /* START ---------- OVO SU SVE TIME STAMPOMVI */
        gdpr_saved: time_now,
        gdpr_accassed: time_now,
        user_deleted: null,
        /* END ---------- OVO SU SVE TIME STAMPOMVI */

        car_plates: user_car_plates,
        customer_tags: null,
        balance: 0,
        customer_acquisition: null,
        customer_vip: null,
        park_places_owner: null,
        name: $('#user_name').val(),
        full_name: $('#user_full_name').val(),
        password: $('#user_pass').val(),
        hash: "",
        email: $('#user_email').val(),
        email_confirmed: false,
        welcome_email_id: "",
        roles: {
          admin: false,
          we_app_user: true,
          super_admin: false
        }
      };


      $.ajax({
          type: 'POST',
          url: window.remote_we_url + '/create_user',
          data: JSON.stringify(user_form_data),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (user) {


          $('#user_pass').val('');
          $('#repeat_user_pass').val('');
          $("#user_register_button").removeClass('cit_disable');

          if (user.success == true) {

            window.localStorage.setItem('cit_user', JSON.stringify(user));
            window.cit_user = window.localStorage.getItem('cit_user');
            window.cit_user = JSON.parse(window.cit_user);

            this_module.user_loggedIn_UI();

            console.log("user is logged in !!!!");
            // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
            monitor_finger_click(true);

            popup_welcome_new_user();

            setTimeout(function () {

              $('#i_have_account').css('display', 'none');

              $('#monthly_products_box').css('display', 'flex');
              $('#account_balance_box').css('display', 'flex');

            }, 300);



          } else {


            $('#user_pass').val('');
            $('#repeat_user_pass').val('');


            if (user.name_taken == true) {
              
              $("#user_register_button").removeClass('cit_disable');
              popup_ime_usera_zauzeto();
              return;
            };


            if (user.email_taken == true) {
              
              $("#user_register_button").removeClass('cit_disable');
              popup_email_usera_zauzet();
              return;
            };


            var poruka_s_servera = user.msg;

            popup_registration_error(poruka_s_servera);
            
            $("#user_register_button").removeClass('cit_disable');


          };
        })
        .fail(function (error) {

          $("#user_register_button").removeClass('cit_disable');

          popup_registration_error();

        })


    }
    this_module.user_registration = user_registration;


    function user_login() {

      $("#user_login_button").addClass('cit_disable');

      $("#msg_stari_korisnici_box").css('display', 'none');

      var user_form = {
        name: $('#user_name').val(),
        password: $('#user_pass').val(),
      };


      $.ajax({
          type: 'POST',
          url: window.remote_we_url + '/authenticate',
          data: JSON.stringify(user_form),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (user) {

          $("#user_login_button").removeClass('cit_disable');

          $('#user_pass').val('');
          $('#repeat_user_pass').val('');

          if (typeof user.success !== 'undefined' && user.success !== false) {

            $("#account_balance_box").css('display', 'flex');
            $("#monthly_products_box").css('display', 'flex');


            // set user data to local storage
            window.localStorage.setItem('cit_user', JSON.stringify(user));
            // get user data back from local storage  --- to be sure all is ok
            window.cit_user = window.localStorage.getItem('cit_user');
            // make js object from local storage data
            window.cit_user = JSON.parse(window.cit_user);

            save_user_lang_in_db();


            // animate user form ( collapse form and show user name ...and show log out button ) 
            this_module.user_loggedIn_UI();

            console.log("user is logged in !!!!");
            // START MONITORING MOUSE FOR MOVMENT AND CHECKING USER TOKEN !!!!
            monitor_finger_click(true);



            setTimeout(function () {


              $('#i_have_account').css('display', 'none');

              $('#monthly_products_box').css('display', 'flex');
              $('#account_balance_box').css('display', 'flex');


            }, 300);





          } else {

            $('#user_pass').val('');
            $('#repeat_user_pass').val('');
            // response property succsess == false
            // alert(user.msg);
            alert('AUTENTIKACIJA NIJE USPJELA.\n\n KRIVO IME KORISNIKA ILI KRIVA LOZINKA !');

          };
        })
        .fail(function (error) {
          $("#user_login_button").removeClass('cit_disable');
          alert("Error on user loging !!!");
        });

    }; // end of user login
    this_module.user_login = user_login;

    function user_logOUT() {

      window.user_logged = false;

      $("#msg_stari_korisnici_box").css('display', 'none');

      window.all_user_products = {
        balance_sum: 0,
        monthly_arr: []
      };

      monitor_finger_click(false);
      window.cit_user = null;
      window.localStorage.removeItem('cit_user');

      $('#account_form input[type="password"]').val('');
      this_module.user_loggedOut_UI('login_form');

    };
    this_module.user_logOUT = user_logOUT;

    

function check_token() {

  $("#msg_stari_korisnici_box").css('display', 'none');

  return new Promise( function( resolve, reject ) { 

    window.cit_user = window.localStorage.getItem('cit_user');

    if (!window.cit_user) {
      console.error('nema usera unutar check token !!!!!')
      resolve(null);
      return;
    };

    window.cit_user = JSON.parse(window.cit_user);

    // get token from GLOBAL user object
    var token_object = {
      token: window.cit_user.token
    };

    // get expiration from GLOBAL user object
    var token_expiration = Number(window.cit_user.token_expiration);
    // get diff from current time
    var remaining_time = token_expiration - Date.now();

    // if expiration time is in less then 5 min !!!!
    // and greater then 0
    if (remaining_time > 0 && (remaining_time < 5 * 60 * 1000)) {

      // then make special request for prologing token duration
      // actually it is creation of brand new token !!!!!!  
      $.ajax({
          type: 'POST',
          url: window.remote_we_url + '/prolong',
          data: JSON.stringify(token_object),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (new_token_object) {
          // usually when token check fails then instead of token 
          // I get property success: false
          // so I handle that like error !!!!
          if (new_token_object.success == false) {

            popup_session_expired();
            cit_user = null;
            window.localStorage.removeItem('cit_user');
            resolve('expired');

          } else {
            // write new token data to GLOBAL cit_user object
            cit_user.token = new_token_object.token;
            cit_user.token_expiration = Number(new_token_object.token_expiration);

            // write in local storage user object with new token data
            window.localStorage.setItem('cit_user', JSON.stringify(cit_user));

            resolve('prolonged');

          }
        })
        .fail(function (error) {
          alert("Greška kod produženja korisničkog tokena !!!!");
          resolve(null);
        })

    } else if (remaining_time <= 0) {

      popup_session_expired();
      this_module.user_loggedOut_UI('login_form');
      window.cit_user = null;
      window.localStorage.removeItem('cit_user');
      resolve(null);

    } else {
      // ako je sve ok  tj ako token ima više od 5 minuta da zastari
      // onda ne treba raditi ništa
      resolve('ok');

    };

  }); // kraj promisa

};
this_module.check_token = check_token;

    function check_user() {

      
      // ako nema usera
      if (!window.cit_user) {
        // pogledaj u local storage
        window.cit_user = window.localStorage.getItem('cit_user');
        if (window.cit_user) window.cit_user = JSON.parse(window.cit_user);
      };

      // ako nema usera niti u local storage
      if (!window.cit_user) {
        this_module.user_loggedOut_UI('register_form');
        window.user_logged = false;
        return;
      };

      if (!window.cit_user) return;

      // try {
      this_module.check_token().then(function (token_state) {


        if (window.cit_user && token_state == null) {
          this_module.user_loggedOut_UI('login_form');
          window.user_logged = false;
        };


        if (window.cit_user && token_state == 'expired') {
          this_module.user_loggedOut_UI('login_form');
          window.cit_user = null;
          window.localStorage.removeItem('cit_user');
          window.user_logged = false;
        };


        if (window.cit_user && window.user_logged !== true && (token_state == 'prolonged' || token_state == 'ok')) {
          wait_for(`$('#account_form').length > 0`, function () {
            this_module.user_loggedIn_UI();
          }, 500000);
        };

      });

      /*  
      }
      catch(err) {
        console.log(err);
      }
      */

    }; // end of check user function
    this_module.check_user = check_user;

    function edit_user() {

    };
    this_module.edit_user = edit_user;


    function show_reg_form() {

      $("#msg_stari_korisnici_box").css('display', 'none');

      $("#user_pass_label").text(w.we_trans('id__user_pass_label', w.we_lang));
      $("#repeat_user_pass_label").text(w.we_trans('id__repeat_user_pass_label', w.we_lang));

      $("#repeat_user_pass_label").css('display', 'block');
      $("#repeat_user_pass").css('display', 'block');

      $("#pass_section").css({
        'display': 'block',
        'max-height': 'unset',
        'height': 'auto',
        'pointer-events': 'all'
      });


      // window.localStorage.setItem('we_frist_time_login', 'true');

      window.localStorage.setItem('we_1_time_login', 'true');


      $("#show_login_or_register_box").css('display', 'none');

      $("#user_name_label").css('display', 'block');
      $("#user_name").css('display', 'block');


      $("#pass_section").css('display', 'block');

      $("#login_register_save_container").css('display', 'block');

      $("#user_login_button").css('display', 'none');
      $("#forgot_user_pass_btn").css('display', 'flex');
      $("#user_register_button").css('display', 'flex');
      $("#send_email_reset_user_or_pass").css('display', 'none');


      $("#full_name_and_email_section").css({
        'display': 'block',
        'max-height': 'unset',
        'height': 'auto',
        'pointer-events': 'all'
      });


      $("#user_full_name_label").css('display', 'block');
      $("#user_full_name").css('display', 'block');

      $("#reg_plate_sub_section").css('display', 'block');



      // ---------------------- PRIKAŽI SAMO LOGIN LINK
      $("#show_login_or_register_box").css({
        'display': 'block',
        'margin-top': '20px'
      });


      $("#show_register_form").css({
        'display': 'none',
      });

      $("#text_under_register_btn").css({
        'display': 'none',
      });

      $("#show_login_form").css({
        'display': 'block',
      });




    };
    this_module.show_reg_form = show_reg_form;


    function show_log_form() {

      $("#msg_stari_korisnici_box").css('display', 'none');

      $("#user_pass_label").text(w.we_trans('id__user_pass_label', w.we_lang));
      $("#repeat_user_pass_label").text(w.we_trans('id__repeat_user_pass_label', w.we_lang));



      // window.localStorage.setItem('we_frist_time_login', 'true');

      window.localStorage.setItem('we_1_time_login', 'true');

      // $("#show_login_or_register_box").css('display', 'none');



      // ----------------------- PRIKAŽI SAMO REGISTER LINK
      $("#show_login_or_register_box").css({
        'display': 'block',
        'margin-top': '20px'
      });

      $("#show_register_form").css({
        'display': 'block',
      });

      $("#text_under_register_btn").css({
        'display': 'none',
      });

      $("#show_login_form").css({
        'display': 'none',
      });



      $("#i_have_account").css('display', 'none');


      $("#repeat_user_pass_label").css('display', 'none');
      $("#repeat_user_pass").css('display', 'none');

      $("#pass_section").css('height', '50px');


      $("#user_name_label").css('display', 'block');
      $("#user_name").css('display', 'block');

      $("#full_name_and_email_section").css('display', 'none');
      $("#pass_section").css('display', 'block');

      $("#login_register_save_container").css('display', 'block');

      $("#user_login_button").css('display', 'flex');
      $("#forgot_user_pass_btn").css('display', 'flex');
      $("#user_register_button").css('display', 'none');
      $("#send_email_reset_user_or_pass").css('display', 'none');


    };
    this_module.show_log_form = show_log_form;


    function show_recover_user_or_pass() {

      $("#msg_stari_korisnici_box").css('display', 'none');

      $("#full_name_and_email_section").css('display', 'block');


      $("#user_name_label").css('display', 'none');
      $("#user_name").css('display', 'none');


      $("#user_full_name_label").css('display', 'none');
      $("#user_full_name").css('display', 'none');

      $("#reg_plate_sub_section").css('display', 'none');


      $("#user_email_label").css('display', 'block');
      $("#user_email").css('display', 'block');


      $("#pass_section").css('display', 'block');

      $("#user_pass_label").css('display', 'block');
      $("#user_pass").css('display', 'block');

      $("#repeat_user_pass_label").css('display', 'block');
      $("#repeat_user_pass").css('display', 'block');


      /*
      $("#user_pass_label").text('Upišite novu lozinku koju želite.');
      $("#repeat_user_pass_label").text('Ponovite novu lozinku.');
      */

      $("#user_pass_label").text(w.we_trans('id__user_pass_label', w.we_lang, true));
      $("#repeat_user_pass_label").text(w.we_trans('id__repeat_user_pass_label', w.we_lang, true));


      $("#full_name_and_email_section").css({
        'height': '60px',
        'max-height': '60px',
        'pointer-events': 'all'
      });


      $("#pass_section").css({
        'height': '100px',
        'overflow': 'hidden',
        'display': 'block',
        'max-height': '100px',
        'pointer-events': 'all'
      });

      $("#send_email_reset_user_or_pass").css('display', 'flex');


      $("#user_register_button").css('display', 'none');
      $("#user_edit_button").css('display', 'none');
      $("#user_logout_button").css('display', 'none');
      $("#user_save_new_data_button").css('display', 'none');
      $("#user_login_button").css('display', 'none');

      $("#i_have_account").css('display', 'none');

      $("#forgot_user_pass_btn").css('display', 'none');




      // ---------------------- PRIKAŽI I REGISTER I LOGIN LINK 
      $("#show_login_or_register_box").css({
        'display': 'block',
        'margin-top': '20px'
      });


      $("#show_register_form").css({
        'display': 'block',
      });

      $("#text_under_register_btn").css({
        'display': 'block',
      });

      $("#show_login_form").css({
        'display': 'block',
      });


    };
    this_module.show_recover_user_or_pass = show_recover_user_or_pass;


    function run_user_pass_reset() {
      $("#msg_stari_korisnici_box").css('display', 'none');

      var mail = $('#user_email').val();
      var is_email_ok = mail.search(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

      if (is_email_ok === -1 || mail.indexOf('.') === -1) {
        popup_email_nije_dobar();
        $('#account_form .we_account_input').css('border', '');
        $('#user_email').css('border', '1px solid #ff0058');
        return;
      };


      var pass = $('#user_pass').val();

      if (

        $('#user_pass').val() == '' ||
        $('#repeat_user_pass').val() == '' ||
        $('#user_pass').val() !== $('#repeat_user_pass').val()

      ) {
        popup_lozinke_nisu_iste();
        $('#account_form .we_account_input').css('border', '');
        $('#user_pass').css('border', '1px solid #ff0058');
        $('#repeat_user_pass').css('border', '1px solid #ff0058');
        return;
      };


      $('#send_email_reset_user_or_pass').css('pointer-events', 'none');

      var data_for_reset_pass = {
        user_email: mail,
        new_pass: pass
      };


      $('#account_form .we_account_input').css('border', '');

      $.ajax({
          type: 'POST',
          url: window.remote_we_url + '/user_pass_reset',
          data: JSON.stringify(data_for_reset_pass),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
        .done(function (response) {

          $('#send_email_reset_user_or_pass').css('pointer-events', 'all');

          window[response.msg]();

          if (response.success == true) {
            this_module.show_log_form();
            $('#user_pass').val('');
            $('#repeat_user_pass').val('');
          };

        })
        .fail(function (error) {

          window[error.msg]();
          $('#send_email_reset_user_or_pass').css('pointer-events', 'all');
          console.log(error);

        });

    };
    this_module.run_user_pass_reset = run_user_pass_reset;


    this_module.cit_loaded = true;

  }


};
