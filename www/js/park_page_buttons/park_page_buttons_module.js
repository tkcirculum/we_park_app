var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    
    var this_module = window[module_url]; 
    
    
    wait_for('true == true', function() {
      
      
    var component_id = null;
    
    

    // samo skraćujem jer mi se neda pisati :)   
    var w = window; 
      
    var component_html =
`
<div id="park_place_buttons_box">

  <div id="select_book_duration" class="cit_show">
    <div id="minus_book_duration_btn"><i class="fas fa-minus-circle"></i></div>
    <div id="display_book_duration"><div id="book_duration_num">1</div>h</div>
    <div id="plus_book_duration_btn"><i class="fas fa-plus-circle"></i></div>


   <!-- <div id="booking_explanation">REZERVACIJA SE NAPLAĆUJE PO ODABRANOM TRAJANJU</div>-->

  </div>

  <div id="book_timer" class="cit_hide"> <!--OVDJE INJEKTIRAM PROTEKLO VRIJEME BOOKINGA--> </div>

  <div class="pp_button cit_animate cit_hide" id="ramp_not_open_btn">
    <!--<i class="fas fa-exclamation-triangle"></i>-->
    <div id="ramp_not_open_txt_1">${ w.we_trans( 'id__ramp_not_open_txt_1', w.we_lang) }</div>
    <div id="ramp_not_open_txt_2">${w.we_trans( 'id__ramp_not_open_txt_2', w.we_lang) }</div>
    <!--<a href="tel:00385923133015" id="help_tel_num"><i class="fas phone-alt"></i> 00385 92 3133 015</a>-->
  </div> 


  <div id="book_and_stop_book_box" style="margin-top: 0;">

    <div class="pp_button cit_animate cit_show" id="book_pp_btn" 
         style="background-color: #0e68b4; 
                flex: 1;
                position: absolute;
                top: 0;
                left: 50%;
                margin: 10px;
                margin-left: -125px;
                width: 250px;">

      <i class="fas fa-book"></i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>

      <span id="book_pp_btn_span">${w.we_trans( 'id__book_pp_btn_span', w.we_lang )}</span>
    </div>

    <div class="pp_button cit_animate cit_hide" id="cancel_booking_btn" 
         style="background-color: #e8412f;
                flex: 1;
                position: absolute;
                top: 0;
                left: 50%;
                margin: 10px;
                margin-left: -125px;
                width: 250px;">

      <i class="fas fa-times"></i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>

      <span id="cancel_booking_btn_span">${w.we_trans( 'id__cancel_booking_btn_span', w.we_lang )}</span>
    </div>

    <!--          
    <div class="pp_button cit_animate cit_hide" id="cancel_booking_btn" 
         style="background-color: transparent; color: #e8412f; flex: 1; position: absolute; top: 0; left: 0; margin: 10px;">


      <i class="fas fa-times"></i>
      <i class="fas fa-cog fa-spin" style="display: none;"></i>

      REZERVACIJA U TIJEKU
    </div>
    -->


  </div> 

  <div id="start_park_and_stop_park_box" style="margin-top: 10px;">

    <div id="left_side_pp_buttons" style="display: flex; align-items: center; justify-content: center; flex: 1;">

      <div id="enter_pp_btn" class="pp_button cit_animate cit_show" style="background-color: #0e68b4;">

        <!-- <i class="fas fa-chevron-right"></i> -->
        <i class="ramp_icon_box"><img src="img/ramp_icon.svg"></i>
        <i class="fas fa-cog fa-spin" style="display: none;"></i>

        <span id="enter_pp_btn_span">
          ${w.we_trans( 'id__enter_pp_btn_span', w.we_lang )}
        </span>

      </div>

      <div id="exit_pp_btn" class="pp_button cit_animate cit_hide" style="background-color: #e8412f;">

        <!-- <i class="fas fa-chevron-left"></i> -->
        <i class="ramp_icon_box"><img src="img/ramp_icon.svg"></i>
        <i class="fas fa-cog fa-spin" style="display: none;"></i>

        <span id="exit_pp_btn_span">
          ${w.we_trans( 'id__exit_pp_btn_span', w.we_lang )}
        </span>

      </div>

    </div>

    <div id="right_side_pp_buttons" style="display: flex; align-items: center; justify-content: center; flex: 1;">

      <div class="pp_button cit_animate cit_show" id="open_barrier_btn" style="background-color: #0e68b4;">

        <div id="blink_sto_je_ovo">ŠTO JE OVO ?</div>
        
        <!--<i class="fas fa-chevron-right"></i>-->
        <i class="barrier_icon_box"><img src="img/barrier_icon.svg"></i>
        <i class="fas fa-cog fa-spin" style="display: none;"></i>

        <span id="open_barrier_btn_span">
          ${w.we_trans( 'id__open_barrier_btn_span', w.we_lang )}
        </span>

      </div>

      <div class="pp_button cit_animate cit_hide" id="close_barrier_btn" style="background-color: #e8412f;">

        <!--<i class="fas fa-chevron-left"></i>-->
        <i class="barrier_icon_box"><img src="img/barrier_icon.svg"></i>
        <i class="fas fa-cog fa-spin" style="display: none;"></i>

        <span id="close_barrier_btn_span">
          ${w.we_trans( 'id__close_barrier_btn_span', w.we_lang )}
        </span>

      </div>
      
      
          
        <!-- 
        <div id="show_barrier_map" class="pp_button cit_animate" style="background-color: #0e68b4;">

        <!-- <i class="fas fa-chevron-left"></i> -->
        <!--<i class="fas fa-map" style="margin-top: -7px;"></i>

        -->
      
        <!--
        <i class="barrier_icon_box"><img src="img/all_barriers_icon.svg"></i>
        <i class="fas fa-cog fa-spin" style="display: none;"></i>

        <span id="show_barrier_map_span">
          ${w.we_trans( 'id__show_barrier_map_span', w.we_lang )}
        </span>

      </div>
      -->
      
      
      
      

    </div>
    
    
    
  </div>

</div>

`;
      
      
  var this_module = window[module_url];     
   
  
      // ovo je arbitrarni uvijet  - uzeo sam kao uvijet da se pojavi exit_pp_btn  ....
  wait_for( `$('#exit_pp_btn').length > 0`, function() {
    
    var this_module = window[module_url];

    
    // ZA SADA NIŠTA OVDJE NISAM STAVIO 
    
    
  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 10*1000 );         

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  // var show_popup_modal = window['/main_area/cit_popup_modal/cit_popup_modal.js'].show_popup_modal;
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    
    this_module.cit_data = {
      
    };
    
  };
  
  
  this_module.cit_loaded = true;
 
}
  
  
};    

