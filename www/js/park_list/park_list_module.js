var module_object = {
  create: function( data, parent_element, placement ) {
    
  var this_module = window['js/park_list/park_list_module.js']; 

    // postavi prvo varijablu da nije gotov user update
    window.user_update_done = false;

    if ( window.cit_user && window.cit_user.user_number ) {
      // napravi user update samo ako postoji već user u memoriji
      update_user_from_db(window.cit_user.user_number);
    } else {
      // ako trenutno nema usera onda ga naravno ne mogu update-ati !!!!!!
      // pa zato odmah prebacim ovu varijablu da je true
      // tako da ispod wait for krene dalje !!!!!
      window.user_update_done = true;
    };

  // nije mi bitno jel user zapravo update ili ne  - samo da je proces završen
  // a znam da je proces završen po ovoj winow varijabli ispod:
  wait_for('window.user_update_done == true', function() {

    var this_module = window['js/park_list/park_list_module.js']; 
      
    
    // VAŽNO --- varijablu data dobijem unutar index.js unutar click eventa na park_list_button
    // točnije dovijem data koji je array svih parkinga !!!!
    console.log('---------------------- park_list_array');
    console.log(data);
      
    var component_id = null;
      
    function create_park_list_row(park) {

      var trenutna_cijena_parkinga = null;
      
      var park_place_open = 'true';
      var row_style = '';

      if ( typeof park.pp_current_price !== 'undefined' && park.pp_current_price !== null ) {
        
        trenutna_cijena_parkinga = zaokruzi(park.pp_current_price, 1);
        
      } else {
        
        // napisi da je parking zatvoren jedino ako nije neki specijalni user kao owner ili free ili super_admin
        var user_status = get_user_status(park);
        
        if ( user_status == '' || user_status == null ) {
          row_style = `style="background-color: #efefef;"`;
          park_place_open = 'false';
        };
        
      };

      var avail_count = park.pp_available_out_num;

      var w = window;
      
      var row_html =

    `
      <div  class="park_list_row" 
            id="park_list_row_${park._id}"
            data-pp_row_id="park_list_row_${park._id}"
            ${row_style}
            data-park_place_open="${park_place_open}"
            data-current_price="${trenutna_cijena_parkinga}" >
        <div class="park_list_row_part_1">
          <div class="park_available_count">${avail_count}</div>
          <div class="park_available_label">${ w.we_trans( 'cl__park_available_label', w.we_lang) }</div>
        </div>
        <div class="park_list_row_part_2">

          <div class="park_name">${ park.pp_name.toUpperCase() }</div>
          <div class="park_address">${park.pp_address}</div>
          

          <div class="park_distance_and_state">
            <div class="park_distance"> -- km / -- mil</div>
            
            <!-- <div class="park_indoor">indoor <i class="fas fa-warehouse"></i></div> -->
            

            <!-- OVDJE STAVLJAM INDICATOR JEL PARKIRAN / BOOKIRAN OD STRANE OVOG USERA  -->

          </div>

        </div>

        <div class="park_list_row_part_3">
          <div class="navigate_icon_buton"><i class="fas fa-route"></i></div>
          <div class="navigate_button_label">route</div>
        </div>


        <div class="park_list_row_part_4">

          <div class="park_place_price">
            ${ trenutna_cijena_parkinga !== null ? trenutna_cijena_parkinga : '' }
          </div>
          <div class="park_place_currency">kn/h</div>
        </div>
      </div>

    `;


      return row_html;

    };
    this_module.create_park_list_row = create_park_list_row;

    function create_all_park_rows(data) {

      var all_park_rows = '';
      
      $.each( data, function( park_index, park_object ) {
        
        // ako nije ULOGIRAN PRESKOČI 15 i 16
        if ( !window.cit_user && park_object.pp_sifra !== '15' && park_object.pp_sifra !== '16') {
          // ako user nije login onda ne prikazuj tesni parking
          all_park_rows += create_park_list_row(park_object);
        }
        else if (
          // ako JE ULOGIRAN I TO JE 15
          // onda prikaži park samo ovim userima
          window.cit_user && park_object.pp_sifra == '15' 
          &&
          ( 
            window.cit_user.user_number == 777 ||
            window.cit_user.user_number == 9   ||
            window.cit_user.user_number == 999 ||
            window.cit_user.user_number == 540 ||
            window.cit_user.user_number == 8   ||
            window.cit_user.email.indexOf('tehnozavod.hr') > -1
          )
           
        ) {
          // ako je user login i ako je user 777 Toni ili user 9 Karlo
          // onda prikaži tesni parking
          all_park_rows += create_park_list_row(park_object);
        }
        else if ( 
          window.cit_user && park_object.pp_sifra == '16' 
          &&
          window.cit_user.email.toLowerCase().indexOf('t.ht.hr') > -1 ) {
          // ako je 16 park i ako je t-com user
          all_park_rows += create_park_list_row(park_object);
        }
        else {
          // ako je ulogiran ali i ako nije niti 15 niti 16
          // onda prikaži park
          
          if ( window.cit_user && park_object.pp_sifra !== '15' && park_object.pp_sifra !== '16' ) {
            all_park_rows += create_park_list_row(park_object);
          };
          
        }; 
      });

      return all_park_rows;
    };
    this_module.create_all_park_rows = create_all_park_rows;
      
    var w = window;
    var component_html =
`
<div class="big_page_header">
  <div id="park_list_header_label" style="flex: 1;">
    ${window.we_trans( 'id__park_list_header_label', window.we_lang) }
  </div>
  
  <!-- START DROP LIST ELEMENT -->
  <div class="we_drop_list" id="sort_park_places" style="margin-right: 12px;">
    <div class="we_drop_list_arrow"><i class="fas fa-caret-down"></i></div>
    
    <div class="we_drop_list_items">
      <div class="we_drop_list_item" id="sort_by_distance">${w.we_trans( 'id__sort_by_distance', w.we_lang) }</div>
      <div class="we_drop_list_item" id="sort_by_price">${w.we_trans( 'id__sort_by_price', w.we_lang) }</div>

      <!--
      <div class="we_drop_list_item" id="sort_by_distance_indoor">By Distance only indoor</div>
      <div class="we_drop_list_item" id="sort_by_price_indoor">By Price only indoor</div>
      -->
      
    </div>
  </div>
  
  <!-- END DROP LIST ELEMENT -->
  
  <div class="we_menu_icon"><i class="fas fa-bars"></i></div>

</div>

<div id="park_list_box">

  ${ create_all_park_rows(data) }

</div>



`;
      
      
  var this_module = window['js/park_list/park_list_module.js']; 
    
    
  function initial_setup_of_sort_pp_drop_list() {
    
    var item_list = $('#sort_park_places .we_drop_list_items');
    // vidi širinu container za listu
    var list_width = item_list.width();
    // parent element tj ono što izgleda kao input element raširi da bude iste širine kao lista
    // TO MORAM NAPRAVITI JER JE LISTA APSOLUTNO POZICIONIRANA I ZBOG TOGA PARENT SE NEĆE RAZVUĆI NA ŠIRINU LISTE !!!!!!!
    $('#sort_park_places').width(list_width);

    // skrati listu po visini tako da se vidi zamo prvi item
    // -1 je tu samo zato da ne prelazi bottom bijelu crtu
    var parent_height = $('#sort_park_places').height() - 1;
    item_list.height(parent_height);
    
  };
  this_module.initial_setup_of_sort_pp_drop_list = initial_setup_of_sort_pp_drop_list;
    
  
  function register_all_events_in_park_list() {
    
    // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
    wait_for( `$('#sort_park_places').length > 0`, function() {

      // $('.cit_tooltip').tooltip();

      setTimeout(function() {

        var this_module = window['js/park_list/park_list_module.js'];

        this_module.initial_setup_of_sort_pp_drop_list();
        
        
        window.current_we_page = 'park_list_button';
        console.log(window.current_we_page);

        $('#sort_park_places').off('click');
        $('#sort_park_places').on('click', this_module.choose_pp_sorting );


        $('.park_list_row').off('click');
        $('.park_list_row').on('click', this_module.check_book_i_park_i_open_park_place );      


        $('.navigate_icon_buton').off('click');
        $('.navigate_icon_buton').on('click', this_module.show_route_to_park_place ); 


        this_module.write_distance_in_all_park_rows();
        

        wait_for(`window.cit_user !== null && typeof window.cit_user !== "undefined"`, function() {
          get_active_booking_or_parking( { pp_sifra: 'all' }, window.cit_user.user_number );
        }, 30*1000);

      }, 100);

    }, 5000000 );      
  };
  this_module.register_all_events_in_park_list = register_all_events_in_park_list;  

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  }; 
  
  setTimeout( function() {  
    this_module.register_all_events_in_park_list();    
  }, 300);

    
    
    return {
      html: component_html,
      id: component_id
    };
      
  }, 500000 );  // kraj wait for user update done == true
  
  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // 'js/park_list/park_list_module.js' se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window['js/park_list/park_list_module.js']; 
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) this_module.cit_data = {};
    
  window.last_dosadno_1_rega = window.localStorage.getItem('last_dosadno_1_rega');
  
  // ako je null onda pretvori u 0 - ako nije null onda pretvori u broj od toga
  window.last_dosadno_1_rega = (window.last_dosadno_1_rega == null) ? 0 : Number(window.last_dosadno_1_rega);
  
  
  
  function open_park_place(kliknuti_park_red_element, clicked_pp_data, book_or_park_state ) {
    
    // isto tako inicijalno sakrij gumb za prikazivanje tlocrta od parkinga
    $('#show_barrier_map').removeClass('cit_show');
    
    
    if (window.cit_user) {
      
      if (
        
        $.isArray(window.cit_user.car_plates) == false 
        ||
        ($.isArray(window.cit_user.car_plates) == true && window.cit_user.car_plates.length == 0)
         
        ) {
        
        // ako sam zadnji puta prije tri dana pokazao poruku da treba upisati regu
        if ( Date.now() - window.last_dosadno_1_rega > 1000*60*60*24*3 ) {
          
          window.last_dosadno_1_rega = Date.now();
          window.localStorage.setItem('last_dosadno_1_rega', String(window.last_dosadno_1_rega) );
          
          popup_dosadno_1_rega();
          
        };
      };

    };
  
    add_we_history(kliknuti_park_red_element.id);
    
    
    // get trenutnu top poziciju reda
    var row_top = $(kliknuti_park_red_element).offset().top;
    
    // get dom objekt od info diva
    var park_place_info = $('#park_place_info');
    
    // get data div unutar info fiva
    var park_place_row_data = $('#park_place_row_data');
    // obriši sve u njemu
    park_place_row_data.html('');
    
    // clone red i postavi ga u data div
    $(kliknuti_park_red_element).clone().prependTo( park_place_row_data );
    
    // pošto sam klonirao cijeli red moram ponovo registrirati click na 
    // navigate buttonu !!!!!!
    // to radim u time outu jer želim dati vremena javascriptu da generira novi klonirani element !!!!
    setTimeout( function() {
      
      $('#park_place_row_data .park_list_row').removeAttr('id');
      
      $('#park_place_info .navigate_icon_buton').off('click');
      $('#park_place_info .navigate_icon_buton').on('click', this_module.show_route_to_park_place ); 
      
      
    }, 100);

    // pozicioniram info div točno na mjesto reda kojeg sam kliknuo
    // ali bez animacije ( jer nisam dao klasu cit_animate )
    park_place_info.css('transform', `translate(0, ${row_top}px)`);
    

    // re-render  park page 
    var park_page = window['js/park_page/park_page_module.js']
    park_page.create( clicked_pp_data, $('#park_place_page')); 
    
    setTimeout( function () { 
      // dodaj stranici sa listom ove css klase tako da ju izbacim iz viewa
      // -----> hide i top  koje pomiču page prema gore
      
      // VAŽNO !!!! nisam oduzeo cit_active klasu list page-u što znači da 
      // ju tretiram još uvijek kao aktivnu !!!!!
      
      $('#park_list_page').addClass('cit_hide cit_top');
      
      // stavio sam ovo u timeout za svaki slučaj da bude zadnje koje je upisano
      // prebacujem data set u modul za specifičan parking tj page za specifičan parking
      
      // park_page.cit_data = clicked_pp_data;
      
      
      // get_active_booking_or_parking(clicked_pp_data, cit_user.user_number);
      
      setup_park_page_GUI(book_or_park_state, clicked_pp_data);

      
      var user_num = window.cit_user ? window.cit_user.user_number : null;
      
      if ( user_num ) {
        
        var up_or_down = null;


        if ( 

          clicked_pp_data.barriers             &&
          $.isArray(clicked_pp_data.barriers)  &&
          clicked_pp_data.barriers.length > 0 

        ) {

          get_db_barrier_state(clicked_pp_data.pp_sifra, user_num)
          .then(
          function(get_bar) {
            
            // ako ima rezerviranu barijeru - može je vidjeti na mapi kako blinka u crvenoj boji !!!!!
            set_blink_me_on_map(get_bar.name);

            // PAZI - ovo NIJE TOGGLE  !!!!!!!
            // pa je up or down isto stanje  tj ne mijenjam argument
            // koje sam dobio od get barrier state
            if ( get_bar.state == null || get_bar.state == 'booked' ) {
              up_or_down = 'up';
            } else if ( get_bar.state == 'parked' ) {
              up_or_down = 'down';
            };

            set_barrier_html( get_bar.name, up_or_down );

            $('#right_side_pp_buttons').css('display', 'flex');
            
            if ( clicked_pp_data.main_ramp == true ) {
              $('#left_side_pp_buttons').css('display', 'flex');
            } else {
              $('#left_side_pp_buttons').css('display', 'none');
            };
            
            
            // nemoj prikazati label "što je ovo" ako je user toni ili karlo
            // ili ako je netko iz t coma
            if ( 
              window.cit_user && 
              ( window.cit_user.user_number == 777 ||
                window.cit_user.user_number == 9   ||
                window.cit_user.email.toLowerCase().indexOf('t.ht.hr') > -1
              ) 
            ) {

              // nemoj prikazati label ŠTO JE OVO AKO JE TZM ILI AKO JE TESTNI PARKING
              // nemoj prikazati label ako je t com parking 
              if ( 
                clicked_pp_data.pp_sifra == '6'  || 
                clicked_pp_data.pp_sifra == '15' ||
                clicked_pp_data.pp_sifra == '16'
              ) {

                if ( 
                    window.cit_user && 
                    ( window.cit_user.user_number == 777 ||
                      window.cit_user.user_number == 9   ||
                      window.cit_user.email.toLowerCase().indexOf('t.ht.hr') > -1
                    ) 
                  ) {
                  
                  $('#show_barrier_map').addClass('cit_show');
                  // u startu bude null pa ga pretvorim u 0
                  var barrier_map_tooltip = Number( window.localStorage.getItem('barrier_map_tooltip') );
                  if ( barrier_map_tooltip <= 2 ) {
                    // prikaži tooltip
                    $('#barrier_map_btn_tooltip_triangle').addClass('cit_show');
                    var tooltip_html = `${ window.we_trans( 'id__barrier_map_btn_tooltip', window.we_lang) }`;  
                    $('#barrier_map_btn_tooltip').html(tooltip_html);
                    // sakrij tooltip nakon 5 sec
                    setTimeout(function() { $('#barrier_map_btn_tooltip_triangle').removeClass('cit_show');  }, 1000*5 );
                    window.localStorage.setItem('barrier_map_tooltip', String( barrier_map_tooltip + 1 ));
                  };
                  
                };
                
                /*
                if ( clicked_pp_data.barriers && clicked_pp_data.barriers.length > 1 ) {
                  $('#open_barrier_btn').css('display', 'none');
                  $('#close_barrier_btn').css('display', 'none');
                  $('#show_barrier_map').css('display', 'flex');
                };
                */

                $('#blink_sto_je_ovo').css('display', 'none');
                
              } else {
                
                // ako nije 5  ili 15 ili 16 parking
                $('#blink_sto_je_ovo').css('display', 'flex');
              };

            } 
            else {
              // ako nema usera i nije user ---> niti karlo niti toni niti netko iz tcoma
              $('#blink_sto_je_ovo').css('display', 'flex');
            };


          },
          function(error) { console.log(error) }    

          ); // kraj od then-a od get db barrier data 

        } 
        else {

          // SADA PRIKAZUJEM GUMB UVIJEK ALI SAMO SAKRIJEM MALU LABEL TRAKU "ŠTO JE OVO"
          // SAMO NA PARKINZIMA KOJA NEMAJU BARIJERU NA KLIK SE OTVORI REKLAMA

          // $('#right_side_pp_buttons').css('display', 'none');
          
          $('#left_side_pp_buttons').css('display', 'flex');
          $('#right_side_pp_buttons').css('display', 'flex');
          $('#blink_sto_je_ovo').css('display', 'flex');

        };

      };

    }, 200);
    
    
    setTimeout( function () { 
      // sada dodaj klasu animate info containeru
      // do sada nije bilo te klase pa je se pojavio trenutno na istoj visini kao kliknuti red
      park_place_info.addClass('cit_animate');
  
      var park_page_body_height = $('#park_place_body').height();
      // stavi info točno na pola visine stranice
      park_place_info.css('transform', `translate(0, ${park_page_body_height*0.4 + 40}px)`);
      
      var win_width = $(window).width();
      var win_height = $(window).height();
      
      
      var button_box_transform = 'rotateX(0deg)';
      var button_box_margin_top = '20px';
      
      if ( win_height > 568 ) {
        button_box_transform = 'rotateX(0deg)';
        button_box_margin_top = '20px';
      } else {
        button_box_transform = 'rotateX(0deg) translateY(24px) scale(0.85)';
        button_box_margin_top = '20px';
      };
      
      if ( win_height > 700 && win_height < 750 ) button_box_margin_top = '45px';
      if ( win_height >= 750 ) button_box_margin_top = '75px';
      
      // okreni u 3D prostoru div sa gumbima
      // do sada su bili okrenuti tako da div bude okmit na ekran i to je efektivno kao da sam sakrio div
      $('#park_place_buttons_box').css({
        'transform': button_box_transform,
        'margin-top': button_box_margin_top
      });

      // prikaži park place page 
      // to je gornji dio layouta koji u sebi ima samo header i sliku parkinga !!!!!!!
      $('#park_place_page')
        .addClass('cit_active')
        .removeClass('cit_hide cit_top'); 
      
    }, 400);
    
    
    setTimeout( function() {
      // animiraj gumb za pronalazak karte
      $('#park_place_info .park_list_row_part_3').addClass('cit_animate');
    }, 600);
    
  };
  this_module.open_park_place = open_park_place;
  
  
  function check_book_i_park_i_open_park_place(obj) {
    
    if ( !cit_user || !cit_user.user_number ) {
      popup_nedostaje_user();
      return;
    };
    
    
    
    // sakrij poruku rampa se nije otvorila ?
    $('#ramp_not_open_btn').addClass('cit_hide').removeClass('cit_show');
    

    // resetiraj sve gumbe koji su možda od prije zapeli u stanju progresa na park page
    set_button_in_progress_state(false, '#book_pp_btn');
    set_button_in_progress_state(false, '#cancel_booking_btn');
    set_button_in_progress_state(false, '#enter_pp_btn');
    set_button_in_progress_state(false, '#exit_pp_btn');
    
    set_button_in_progress_state(false, '#open_barrier_btn');
    set_button_in_progress_state(false, '#close_barrier_btn');
    
    
    // ova funkcija e može pozvati sa objektom parka kao argumentom 
    // ali se može pozvati i sa klikom 
    // u slučaju klika objekt je zapravo event objekt i nema property pp_sifra
    // zato provjeravam jel ima taj property da znam koja je vrsta poziva
    var arg_pp_objekt = null;
    if ( obj.pp_sifra ) {
      arg_pp_objekt = obj;
    };
      
    
    var kliknuti_park_red_element = null;
    var clicked_pp_id = null;
    
    // ako je ova funkcija pozvana sa klikom na park red u listi
    if ( arg_pp_objekt == null ) {
      
      kliknuti_park_red_element = this;
      clicked_pp_id = this.id.replace('park_list_row_', '');
      
      window.current_we_page = this.id;
      console.log(window.current_we_page);
      
    } else {
      // ako je ova func pozvana direktno sa park objektom kao argumentom
      kliknuti_park_red_element = $('#park_list_row_' + arg_pp_objekt._id)[0];
      
      window.current_we_page = 'park_list_row_' + arg_pp_objekt._id;
      console.log(window.current_we_page);
      
      clicked_pp_id = arg_pp_objekt._id;
    };
    
    
    // pronađi odgovovarajući data objekt po idju kojeg sam stavio u html id
    var clicked_pp_data = get_data_of_clicked_park_row( clicked_pp_id );
    
    
    var vec_je_park_ili_book = false;
    
    
    var book_or_park_active = null;

    get_active_booking_or_parking(clicked_pp_data, cit_user.user_number)
    .then( function( response ) {
      
      book_or_park_active = response;
      
      if ( !response.success || response.success !== true ) {
        return;
      };
      

      if ( book_or_park_active.booking.length > 0 ) {
        vec_je_park_ili_book = true;
        window.current_park_is_booked = true;
      } else {
        window.current_park_is_booked = false;
      };

      if ( book_or_park_active.parking.length > 0 ) {
        vec_je_park_ili_book = true;
        window.current_park_is_parked = true;
      } else {
        window.current_park_is_parked = false;
      };
    
        // ako park NIJE otvoren
        if ( $(kliknuti_park_red_element).attr('data-park_place_open') == 'false' ) {
          // provjeri jel tu već bookirao ili parkirao
          // ako je  - ----> daj mu da uđe na park page
          if ( vec_je_park_ili_book == true ) open_park_place(kliknuti_park_red_element, clicked_pp_data, book_or_park_active);
          // ako nema book ili park na ovom parkiralištu prikaži mu kad se otvara !!!
          if ( vec_je_park_ili_book == false ) popup_closest_open_time(clicked_pp_data);
          
        } else {
          
          // ako je park otvoren tj attr za open je true !!!!
          open_park_place(kliknuti_park_red_element, clicked_pp_data, book_or_park_active);
          
        };
      
      // kraj success od then-a za get active book or park
    })
    .catch(
    function(error){
      console.log(error);
        
    });
  
  };
  this_module.check_book_i_park_i_open_park_place = check_book_i_park_i_open_park_place;
  
  
  
  function open_regular_google_maps(arg_park) {
    
    window.user_stopirao_directions = false;
    
    var page_na_pocetku = window.current_we_page;
    
    $('#map_button').click();
    
    // ovo je funkcija koju pozivam ako user klikne na gumb STOP na velikom loadderu za mape
    
    function stop_direct() {
      // vrati se nazad na stranicu na kojoj si bio  
      $('#' + page_na_pocetku).click();
      
      window.user_stopirao_directions = true;
      reset_we_google_map();
      // zatvori loader
      show_we_global_loader(false);
    };
    
    
    show_we_global_loader(true, 'POZIVAM GOOGLE MAP', stop_direct );
    
    setTimeout( function() {

      // stopiraj sve ako je user malo prije kliknuo gumb za stopiranje
      // loadanja mape
      if ( window.user_stopirao_directions == true ) return;
      
      
      if ( window.we_user_lat !== null && window.we_user_lon !== null ) {

        var latlng_start = {lat: window.we_user_lat , lng: window.we_user_lon };
        var latlng_end = {lat: arg_park.pp_latitude, lng: arg_park.pp_longitude };

        var request = {
          origin: latlng_start,
          destination: latlng_end,
          travelMode: 'DRIVING',
          /*
          drivingOptions: {
            departureTime: new Date(),
            trafficModel: 'pessimistic'
          }
          */
        };

        window.we_directionsService.route(request, function(result, status) {
          if (status == 'OK') {

            window.we_directionsRenderer.setDirections(result);

            window.we_directionsRenderer.setMap(window.we_map);
            clear_all_pp_markers();

            // ugasi loader
            show_we_global_loader(false, 'POZIVAM GOOGLE MAP');

            $('#we_directions_panel_wrapper').css({
              'opacity': '1',
              'pointer-events': 'all'
            });

          };
        });

      };

      if ( window.we_user_lat == null || window.we_user_lon == null ) { 
        reset_we_google_map();
        // ugasi loader
        show_we_global_loader(false, 'POZIVAM GOOGLE MAP');
        console.error('NE MOŽE NAPRAVITI RUTU JER NEMA we_user_lat & we_user_lon !!!' );
      };

    }, 2*1000);
    
  };
  this_module.open_regular_google_maps = open_regular_google_maps;
  
  
  function show_route_to_park_place(e) {
   
    var clicked_pp_id = $(this).closest('.park_list_row').attr('data-pp_row_id');
    clicked_pp_id = clicked_pp_id.replace('park_list_row_', '');
    
    var park = get_data_of_clicked_park_row(clicked_pp_id);  
    var latlng_destination = park.pp_latitude + ',' + park.pp_longitude;
    
    if (  window.we_navigator !== null ) {
      launchnavigator.navigate(latlng_destination, {
        app: window.we_navigator
      });
    } else {
      
      popup_nemate_google_ili_apple_maps(park);
    };
  };
  this_module.show_route_to_park_place = show_route_to_park_place;
  
  function choose_pp_sorting(e) {
    
    // alert('clicked on sort');
    
        
    var item_list = $('#sort_park_places .we_drop_list_items');
    var parent_height = $('#sort_park_places').height() - 1;
    var arrow = $('#sort_park_places .we_drop_list_arrow');
    var list_height = item_list.height();
    
    
    if ( list_height == parent_height ) {
      
      var items_count = $('#sort_park_places .we_drop_list_item').length;
      $('#sort_park_places .we_drop_list_item').css('height', '30px');
      
      
      
      item_list.height( items_count * 30 );
      arrow.css('transform', 'rotate(180deg)');
      
      
    } else {
      var selected_item = e.target;
      
      
      // pazi da user nije možda kliknuo na malu strelicu pokraj
      if ( 
        $(selected_item).hasClass('fa-caret-down') == false &&
        $(selected_item).parent().hasClass('we_drop_list_items')
      ) {
        
        
        // napravi temp property za sortiranje jer ako ostavim na nekim da je cijena == null
        // onda će oni doći pvi na listu a to ne želim
        // zato pravim novi property gedje ce null pretvorim u veliki broj npr 9999999 ...itd
        $.each(window.global_park_list, function(pp_index, park) {
          
          if ( park.pp_current_price == null ) window.global_park_list[pp_index].price_za_sort = 999999999;
          if ( park.pp_current_price !== null ) window.global_park_list[pp_index].price_za_sort = park.pp_current_price;
          
        });
        
        
        // ako je user izabrao sort po udaljenosti i ako globalni array parkinga ima u sebi udaljenosti
        if ( selected_item.id == 'sort_by_distance' &&  window.global_park_list[0].distance_km ) {
          // sortiraj uzlazno po udaljenosti
          window.global_park_list.sort(window.by('distance_km', false, Number ) );
        };

        
        if ( selected_item.id == 'sort_by_price' ) {
          // sortiraj uzlazno po price
          window.global_park_list.sort(window.by('price_za_sort', false, Number ) );
        };
        
        var sorted_list_html = this_module.create_all_park_rows(window.global_park_list);
          
        $('#park_list_box').html(sorted_list_html);
          
        setTimeout(function() {
          this_module.register_all_events_in_park_list();
        }, 300);
        
      
        $('#sort_park_places .we_drop_list_item').css('height', '20px');

        item_list.prepend( $(selected_item) );

        setTimeout( function () {
          item_list.height(parent_height);
          arrow.css('transform', 'rotate(0deg)');
        }, 100);

      };
    };
    
  };
  this_module.choose_pp_sorting = choose_pp_sorting;
  
  
  
  
  function write_distance_in_all_park_rows() {
      
    var km_dist = '';
    var mil_dist = '';

    if ( !window.global_park_list || !window.we_user_lat ) return;
    
    
    /*
    --------------------------------------------------------------------
    pronalazim clone row od parka koji se nalazi na park page stranici
    --------------------------------------------------------------------
    */
    
    var park_page_row_id = null;
    if ( $('#park_place_row_data .park_list_row').length > 0 ) {
      park_page_row_id = $('#park_place_row_data .park_list_row').attr('data-pp_row_id');
      // ako postoji taj attribute odreži prefix
      if ( park_page_row_id ) park_page_row_id = park_page_row_id.replace('park_list_row_', '');
    };


    $.each(window.global_park_list, function( park_index, park ) {

      if ( park.pp_latitude && park.pp_longitude ) {

        km_dist = geo_distance(park.pp_latitude, park.pp_longitude).km;
        km_dist =  zaokruzi(km_dist, 1);
        window.global_park_list[park_index].distance_km = km_dist;


        mil_dist = geo_distance(park.pp_latitude, park.pp_longitude).mil;
        mil_dist =  zaokruzi(mil_dist, 1);
        window.global_park_list[park_index].distance_mil = mil_dist;

        var distance_html = `${km_dist} km / ${mil_dist} mil`;

        $(`#park_list_row_${park._id} .park_distance`).html(distance_html);
  
        

        /*
        --------------------------------------------------------------------
        AKO JE PP ROW ID ISTI KAO ID OD TRENUTNOG PARKA U OVOM LOOP-u 
        onda upiši distance
        --------------------------------------------------------------------
        */
        if ( park_page_row_id && park_page_row_id == park._id ) {
          $('#park_place_row_data .park_distance').html(distance_html);
        };

      };

    });// end of each park
    
    
  };
  this_module.write_distance_in_all_park_rows = write_distance_in_all_park_rows;
  
  this_module.cit_loaded = true;
 
}
  
  
};