var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    
    var this_module = window[module_url]; 
    
    
    wait_for('true == true', function() {
      
      
    var component_id = null;
      
    wait_for(`$('#account_form').length > 0`, function() {
      var login_module = window['js/login/we_login_module.js'];
      login_module.create('proba', $('#account_form'));
    }, 30000);  
      
      
      
    wait_for(`window.cit_user !== null && typeof window.cit_user !== "undefined"`, function() {
      
      $("#account_balance_box").css('display', 'flex');
      $("#monthly_products_box").css('display', 'flex');
      
    }, 10*1000);  
      
    
      
    var w = window; 
      
    var sub_list = '';  
    $.each( window.global_park_list, function( pp_index, park ) {
      
      if (park.published) {
        
        var sub_item =
`
<div class="we_drop_list_item" id="mj_pp_${park.pp_sifra}">${park.pp_name}</div>
`;
      
        sub_list += sub_item;
      
      };
        
    });  
      
      
    var component_html =
`
<div class="big_page_header">
  <div id="history_back_on_account_page" class="we_menu_back_arrow"><i class="fas fa-arrow-left"></i></div>
  <div id="account_header_label" style="flex: 1;">${w.we_trans( 'id__account_header_label', w.we_lang) }</div>
  <div class="we_menu_icon"><i class="fas fa-bars"></i></div>
</div>


<div id="account_page_body">

  <div id="monthly_products_box" style="display: none;">

  </div>
  
  <div id="account_balance_box" style="display: none;">
    
    <div id="account_balance_number">
      &nbsp;
      <!-- OVDJE IDE BALANCE IZNOS -->
    </div>
    <div id="account_balance_label">${w.we_trans( 'id__account_balance_label', w.we_lang) }</div>
    
    
    <div id="choose_offer_box">

      <div id="close_voucher_box" style="transform: translate(-50%, 0) scale(0);"><i class="fas fa-times"></i></div>
      <div id="close_credit_card_box" style="transform: translate(-50%, 0) scale(0);"><i class="fas fa-times"></i></div>
      
    
      
      <div id="offer_drop_list_explanation">
        ${w.we_trans( 'id__offer_drop_list_explanation', w.we_lang) }
      </div>
      
      <div class="we_drop_list" id="choose_offer_list" 
           style="font-size: 15px; margin: 0 auto; height: 24px; border-radius: 12px; width: 250px;">
        
        <div class="we_drop_list_arrow" style="right: 10px; top: 9px;"><i class="fas fa-caret-down"></i></div>

        <div class="we_drop_list_items" style="text-transform: uppercase; width: 210px;">

          <div class="we_drop_list_item" id="for_balance" style="color: #00f5ff;">
            ${w.we_trans( 'id__for_balance', w.we_lang) }
          </div>


          ${sub_list}


        </div>

      </div> 
      
      <div id="choose_duration_in_months" class="cit_radio_group cit_hide">
      
        <div id="1_month_sub" class="cit_radio_item cit_selected">
          <div class="cit_radio_circle"><div class="cit_radio_dot"></div></div>
        <span id="1_month_sub_span">${w.we_trans( 'id__1_month_sub_span', w.we_lang) }</span>
        </div>
        <div id="2_month_sub" class="cit_radio_item">
          <div class="cit_radio_circle"><div class="cit_radio_dot"></div></div>
          <span id="2_month_sub_span">${w.we_trans( 'id__2_month_sub_span', w.we_lang) }</span>
        </div>
        <div id="3_month_sub" class="cit_radio_item">
          <div class="cit_radio_circle"><div class="cit_radio_dot"></div></div>
          <span id="3_month_sub_span">${w.we_trans( 'id__3_month_sub_span', w.we_lang) }</span>
        </div>
      
      
      </div>
      
      
      <div id="cijena_odabranog_paketa">

      </div>
      
      <input style="color: #2dc1de; width: 250px; margin: 0 auto;" 
             class="we_account_input descriptive_input" 
             id="add_to_balance_input" 
             data-lang="lang_${w.we_lang}"
             type="text" autocomplete="off" value="${ w.we_trans( 'v__add_to_balance_input', w.we_lang) }" />
      
      <div id="offer_desc">
        Nadoplatite stanje na računu
      </div>
    </div>
    
    <div id="we_voucher_box">
      
      <input  style="color: #2dc1de" 
              class="we_account_input descriptive_input lang_${w.we_lang}" 
              id="voucher_number_input" 
              type="text" autocomplete="off"
              data-lang="lang_${w.we_lang}"
              value="${w.we_trans( 'v__voucher_number_input', w.we_lang) }">

      <div class="account_form_btn" id="enter_voucher_button">
        ${w.we_trans( 'id__enter_voucher_button', w.we_lang) }
      </div>

    </div>


    <div id="we_credit_card_box">
      
      <div class="we_label" id="add_discount_code_label" style="clear: left; float: left; text-align: center; margin-top: 5px;">
        ${w.we_trans( 'id__add_discount_code_label', w.we_lang) }
      </div>
      
      <div>
      <input style="color: #2dc1de; width: 250px; margin: 0 auto;" 
             class="we_account_input descriptive_input"
             id="add_discount_code" 
             type="text" autocomplete="off" 
             data-lang="lang_${w.we_lang}"
             value="${w.we_trans( 'v__add_discount_code', w.we_lang) }" />
      
      </div>
      <div class="we_label" id="enter_card_label" style="clear: left; float: left; text-align: center; margin-top: 5px;">
        ${w.we_trans( 'id__enter_card_label', w.we_lang) }
      </div>

      <div class="we_drop_list" id="customer_credit_card_list" style="font-size: 16px; margin: 0 auto; height: 24px; border-radius: 12px; width: 250px;">
        
        <div class="we_drop_list_arrow" style="right: 10px; top: 9px;"><i class="fas fa-caret-down"></i></div>
        <div class="we_drop_list_items" style="text-transform: uppercase;">

          <div class="we_drop_list_item" id="card_1">377504-----1111</div>
          <div class="we_drop_list_item" id="card_2">377504-----2222</div>
          <div class="we_drop_list_item" id="card_3">377504-----3333</div>

        </div>

      </div> 
      
      <div class="account_form_btn" id="pay_with_token_btn" style="flex: 1; max-height: 30px; display: flex;">
        ${w.we_trans( 'id__pay_with_token_btn', w.we_lang) }
      </div>
      
      
      <div class="account_form_btn" id="open_ws_pay_IAB" style="flex: 1; max-height: 30px; display: flex;">
        ${w.we_trans( 'id__open_ws_pay_IAB', w.we_lang) }
      </div>
      
    </div>

    <div id="voucher_and_credit_card_buttons_box">
      
      <div class="account_btn" id="show_voucher_box_btn" 
           style="display: flex; flex: 1; margin: 5px 5px 0px; font-size: 11px; font-weight: 700;">VOUCHER</div>

      <div class="account_btn" id="pay_with_credit_card_btn" 
           style="display: flex; flex: 1; margin: 5px 5px 0px; font-size: 11px; font-weight: 700;">
        ${window.we_trans( 'id__pay_with_credit_card_btn', window.we_lang ) }
      </div>
      
    </div>

  </div>

  <div id="account_form">

    

  </div>


</div>

`;
      
      
  var this_module = window[module_url];     
   
  function initial_setup_of_choose_offer_list() {
    
    var item_list = $('#choose_offer_list .we_drop_list_items');
    
    // vidi širinu container za listu
    var list_width = item_list.width();
    // parent element tj ono što izgleda kao input element raširi da bude iste širine kao lista
    // TO MORAM NAPRAVITI JER JE LISTA APSOLUTNO POZICIONIRANA I ZBOG TOGA PARENT SE NEĆE RAZVUĆI NA ŠIRINU LISTE !!!!!!!
    
    // $('#choose_offer_list').width(list_width);

    // ovo je specifičan slučaj jer ovdje želim fiksnu širinu
    $('#choose_offer_list').width(220);
    
    // skrati listu po visini tako da se vidi zamo prvi item
    // -1 je tu samo zato da ne prelazi bottom bijelu crtu
    
    
    var each_item = $('#choose_offer_list .we_drop_list_item');
    each_item.css('height', '24px');
    
    // var parent_height = $('#choose_offer_list').height() - 1;
    // item_list.height(parent_height);
    
    item_list.height(23);
    
  };
  this_module.initial_setup_of_choose_offer_list = initial_setup_of_choose_offer_list;    

  function initial_setup_of_customer_credit_card_list() {
    
    var item_list = $('#customer_credit_card_list .we_drop_list_items');
    
    // vidi širinu container za listu
    var list_width = item_list.width();
    // parent element tj ono što izgleda kao input element raširi da bude iste širine kao lista
    // TO MORAM NAPRAVITI JER JE LISTA APSOLUTNO POZICIONIRANA I ZBOG TOGA PARENT SE NEĆE RAZVUĆI NA ŠIRINU LISTE !!!!!!!
    
    // $('#choose_offer_list').width(list_width);

    // ovo je specifičan slučaj jer ovdje želim fiksnu širinu
    $('#customer_credit_card_list').width(220);
    
    
    $('#customer_credit_card_list .we_drop_list_items').width(210);
    
    
    var each_item = $('#customer_credit_card_list .we_drop_list_item');
    each_item.css('height', '24px');
    
    // skrati listu po visini tako da se vidi zamo prvi item
    // -1 je tu samo zato da ne prelazi bottom bijelu crtu
    
    
    // var parent_height = $('#choose_offer_list').height() - 1;
    // item_list.height(parent_height);
    
    item_list.height(23);
    
  };
  this_module.initial_setup_of_customer_credit_card_list = initial_setup_of_customer_credit_card_list;    
      
      
      // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#show_voucher_box_btn').length > 0`, function() {
    
    // $('.cit_tooltip').tooltip();
    var this_module = window[module_url];     
    
    window.current_we_page = 'account_button';
    console.log(window.current_we_page);
    
    $('#history_back_on_account_page').off('click');
    $('#history_back_on_account_page').on('click', we_history_back );
    
    $('#show_voucher_box_btn').off('click');
    $('#show_voucher_box_btn').on('click', this_module.show_voucher_box );
    
    $('#close_voucher_box').off('click');
    $('#close_voucher_box').on('click', this_module.close_voucher_box );    
    
    
    $('#pay_with_credit_card_btn').off('click');
    $('#pay_with_credit_card_btn').on('click', this_module.show_card_box );    
    
    $('#close_credit_card_box').off('click');
    $('#close_credit_card_box').on('click', this_module.close_card_box );    
    
    
    $('#choose_offer_list').off('click');
    $('#choose_offer_list').on('click', this_module.choose_offer );
    
    
    $('#choose_duration_in_months .cit_radio_item').off('click');
    $('#choose_duration_in_months .cit_radio_item').on('click', this_module.choose_duration );

    
    $('#customer_credit_card_list').off('click');
    $('#customer_credit_card_list').on('click', this_module.choose_credit_card );
    
          
    $('#enter_voucher_button').off('click');
    $('#enter_voucher_button').on('click', this_module.pay_with_voucher );
    
    
    $('.we_account_input.descriptive_input').off('click');
    $('.we_account_input.descriptive_input').on('click', this_module.click_on_desc_input );
    
    
    
    $('#add_to_balance_input').off('keyup input');
    $('#add_to_balance_input').on('keyup input', this_module.add_to_balance_input );
    
    
    $('#voucher_number_input').off('keyup input');
    $('#voucher_number_input').on('keyup input', this_module.voucher_number_input );
    
    
    
    
    $('.we_account_input.descriptive_input').off('blur');
    $('.we_account_input.descriptive_input').on('blur', this_module.reset_desc_input );
    
    /*  
    // PRIJE SAM KORISTIO IFRAME U KOJEM SAM IMAO URL OD WS PAY ALI TO NE NE VALJA JER
    // SU OVI IZ WS PAYA NAPRAVILI DA REDIRECT MIJENJA TOP WINDOW URL, A NE IFRAME URL
    // to efektivno znači da ws pay izbaci usera iz app konteksta jer se cordova app vrti na url-u localhost:8000/index.html
    
    // dakle kad ovi iz ws pay-a promijene top url user napusti web view od appa-a
    // TO NIJE DOBRO I ZATO SAD JA OTVORIM NOVI CHILD PROZOR UNUTAR APP SA IN APP BROWSER PLUGINOM !!! 
    
    $('#open_ws_pay_IAB').off('click');
    $('#open_ws_pay_IAB').on('click', this_module.show_card_iframe );
    
    */
    
    
    $('#pay_with_token_btn').off('click');
    $('#pay_with_token_btn').on('click', function() {
      
      if ( !window.we_selected_card ) {
        // ako nije odabrana niti jedna kartica s tokenom onda odaeri prvu
        window.we_selected_card = $('#customer_credit_card_list .we_drop_list_item').eq(0)[0].id;
      };

      if ( window.we_selected_card == 'no_cards' ) {
        $('#pay_with_token_btn').css('pointer-events', 'all');  
        popup_no_cards();
        return;
      }; 
      
      var is_token_payment = true;
      get_user_business_data_and_start_payment(is_token_payment);
      // popup_enter_pass_for_token(is_token_payment);
    });
      
    
    
    
    $('#open_ws_pay_IAB').off('click');
    // $('#open_ws_pay_IAB').on('click', this_module.before_in_app_browser_check_discount );
    $('#open_ws_pay_IAB').on('click', function() {
      var is_token_payment = false;
      get_user_business_data_and_start_payment(is_token_payment);
    });
    
    
    $('#close_iframe_box').off('click');
    $('#close_iframe_box').on('click', this_module.close_iframe_box );
    
    
    window.voucher_box_open = false;
    window.card_box_open = false;
    
    window.we_selected_offer = 'nadoplata';
    // inicijalno duration je none
    window.we_selected_duration = 'none';
    
    window.we_selected_offer_pp = 'none';
    window.we_selected_payment_type = 'voucher';
    window.we_selected_amount = 'none';
    
    window.we_discount = 'none';
    window.we_discount_id = 'none';
    
    this_module.initial_setup_of_choose_offer_list();
    this_module.initial_setup_of_customer_credit_card_list();
    
    /*  
    Prisavlje - 200 kn bez pdva, 250 kn s PDVom
    Vrbik - jednako kao Prisavlje
    
    Algebra - 240 kn bez PDva, 300 kn s PDVom
    Tehnozavod - jednako kao Algebra
    
    */
    
    
    window.cijene_parkinga = {
      "3": 280, // prisavlje --> 252
      "4": 280, // vrbik --> 252
      "5": 340, // algebra --> 306
      "6": 340, // tehnozavod --> 306
      "14": 292, // heinzelova --> 219 (sa popustom od 25% tj  292 * 0.75)
    };
    

  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 10*1000 );         

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 

  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    
    this_module.cit_data = {
      
      };
  };
  
  
  this_module.def_subs = {
    "14": "PRETPLATA",
  };
  
  
  this_module.desc_input_place_holders = {
    voucher_number_input: window.we_trans( 'v__voucher_number_input', window.we_lang),
    add_to_balance_input: window.we_trans( 'v__add_to_balance_input', window.we_lang),
    add_discount_code: window.we_trans( 'v__add_discount_code', window.we_lang )
  };  
  
  
  function show_voucher_box(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    
    $('#add_to_balance_input').addClass('cit_hide');
    
    this_module.initial_setup_of_choose_offer_list();
    
    window.voucher_box_open = true;
    window.card_box_open = false;
    
    window.we_selected_payment_type = 'voucher';
    
    var desc_text = `<span id="offer_desc_voucher">${window.we_trans( 'id__offer_desc_voucher', window.we_lang) }</span>`;
    
    $('#offer_desc').html(desc_text);
    
    
    $('#close_voucher_box').css('transform', 'translate(-50%, 0) scale(1)');
    $('#close_credit_card_box').css('transform', 'translate(-50%, 0) scale(0)');
    
    
    // add_we_history('deposit_btn');
    
    // reset voucher number text inside input
    $('#voucher_number_input').val( this_module.desc_input_place_holders.voucher_number_input );
    
    $('#show_voucher_box_btn').css('display', 'none');
    $('#pay_with_credit_card_btn').css('display', 'none');
    
    $('#monthly_products_box').css('display', 'none');
    $('#account_balance_number').css('display', 'none');
    $('#account_balance_label').css('display', 'none');
    $('#account_form').css('display', 'none');
    
    
    $('#choose_offer_box').css({
      'transform': 'scale(1)',
    });
    
    setTimeout(function() {
      
      $('#choose_offer_box').css({
        'height': '300px',
        'padding': '30px 0 0',
        'width': '290px',
        'overflow': 'visible',
        'opacity': '1'
      });
      
      $('#we_voucher_box').css({
        'height': '110px',
        'padding': '0 20px',
        'width': '290px'
      });
      
      
      
    }, 320);    
    
    
  };
  this_module.show_voucher_box = show_voucher_box;
  
  
  function close_voucher_box(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    window.voucher_box_open = false;
    window.card_box_open = false;
    
    
    $('#close_voucher_box').css('transform', 'translate(-50%, 0) scale(0)');
    $('#close_credit_card_box').css('transform', 'translate(-50%, 0) scale(0)');
    
    
    $('#we_voucher_box').css({
      'height': '0px',
      'padding': '0px 20px',
      'width': '290px'
    });
    
    $('#choose_offer_box').css({
      'transform': 'scale(0)',
      'opacity': '0'
    });
    
    setTimeout(function() {
      
      $('#choose_offer_box').css({
        'height': '0',
        'padding': '0',
        'width': '290px',
        'overflow': 'hidden'
      });
      
    }, 320);
      
    
    $('#show_voucher_box_btn').css('display', 'flex');
    $('#pay_with_credit_card_btn').css('display', 'flex');  
    
    $('#monthly_products_box').css('display', 'flex');
    $('#account_balance_number').css('display', 'block');
    $('#account_balance_label').css('display', 'block');
    $('#account_form').css('display', 'block');
    
    
    
  };
  this_module.close_voucher_box = close_voucher_box;
    
  
  function show_card_box(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    
    $('#close_voucher_box').css('transform', 'translate(-50%, 0) scale()');
    $('#close_credit_card_box').css('transform', 'translate(-50%, 0) scale(1)');
    
    
    this_module.initial_setup_of_choose_offer_list();
    this_module.initial_setup_of_customer_credit_card_list();
    
    window.voucher_box_open = false;
    window.card_box_open = true;
    
    window.we_selected_payment_type = 'card';
    
    
    var desc_text = `<span id="offer_desc_card">${window.we_trans( 'id__offer_desc_card', window.we_lang) }</span>`;
    
    $('#offer_desc').html(desc_text);
    
    
    // add_we_history('deposit_btn');
    
    // reset voucher number text inside input
    $('#voucher_number_input').val('UPIŠI BROJ VOUCHERA');
    
    $('#show_voucher_box_btn').css('display', 'none');
    $('#pay_with_credit_card_btn').css('display', 'none');
    
    
    $('#monthly_products_box').css('display', 'none');
    $('#account_balance_number').css('display', 'none');
    $('#account_balance_label').css('display', 'none');
    $('#account_form').css('display', 'none');
    
    if ( window.we_selected_offer == 'nadoplata' ) $('#add_to_balance_input').removeClass('cit_hide');

    
    
    $('#we_credit_card_box').css({
      'height': '250px',
      'padding': '0 20px 20px',
      'width': '290px',
      'overflow': 'visible'
    });
    
    $('#choose_offer_box').css({
      'transform': 'scale(1)'
      
      
    });
    
    setTimeout(function() {
      
      $('#choose_offer_box').css({
        'height': '300px',
        'padding': '30px 0 0',
        'width': '290px',
        'overflow': 'visible',
        'opacity': '1'
      });
      
    }, 320);    
    
    
  };
  this_module.show_card_box = show_card_box;
  
  
  function close_card_box(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    
    window.voucher_box_open = false;
    window.card_box_open = false;
    
    
    $('#close_voucher_box').css('transform', 'translate(-50%, 0) scale(0)');
    $('#close_credit_card_box').css('transform', 'translate(-50%, 0) scale(0)');
    
    
    $('#we_credit_card_box').css({
      'height': '0px',
      'padding': '0px 20px',
      'width': '290px',
      'overflow': 'hidden'
    });
    
    $('#choose_offer_box').css({
      'transform': 'scale(0)',
      'opacity': '0'
    });
    
    setTimeout(function() {
      $('#choose_offer_box').css({
        'height': '0',
        'padding': '0',
        'width': '290px',
        'overflow': 'hidden'
      });
    }, 320);
      
    
    $('#show_voucher_box_btn').css('display', 'flex');
    $('#pay_with_credit_card_btn').css('display', 'flex');  
    
    
    $('#monthly_products_box').css('display', 'flex');
    $('#account_balance_number').css('display', 'block');
    $('#account_balance_label').css('display', 'block');
    $('#account_form').css('display', 'block');
    
    
    
  };
  this_module.close_card_box = close_card_box;
  
  
  function create_monthly_list(user_products) {
    
    var monthly_list = '';
    var monthly_subs = user_products.monthly_arr;
    
    if ( !user_products || !user_products.monthly_arr ) {
      
      $('#monthly_products_box').html(monthly_list);
      return;
    };
    
    
    monthly_subs.sort( window.by('valid_from', false, Number ) );
    
    
    // BITNO !!!
    // može se dogoditi i zapravo će se često događati
    // da user plati da bi produžio pretplatu u trenutnku dok mu postojeća pretplata još uvijek vrijedi !!!
    // u tom slučaju će se u usr products pojaviti dvije pretplate za isti parking
    // razlika u pretplatama će biti što ova novija ima duži period tj valid to propery je veći
    // kada se to dogodi ja zapravo prepišem staru pretplatu sa ovom dužom novijom pretplatom
    
    // razlog zašto duplam pretplate je taj što želim evidentirati plaćanje i uzimanje pretplate u customer products !!!
    // ali ne želim da user vidi na ekranu više pretplata za isti parking !!!!
    
    window.monthly_subs_bez_ponavljanja = [];
    
    $.each( monthly_subs, function( monthly_index, monthly ) {
      
      var prepisao_postojeci_monthly = false;
      $.each( window.monthly_subs_bez_ponavljanja, function( unique_month_index, unique_month ) {
        // ako je sljedeći monthly isti kao neki koji sam već pronašao tj ako je za isti parking
        // onda provjeri jel taj sljedeći ima valid to timestamp veći od već spremljenog
        // ako ima onda prepiši već spremljeni sa ovim novim
        if (
          unique_month.pp_sifra == monthly.pp_sifra &&
          unique_month.valid_to < monthly.valid_to
          ) {
          
          var tag_u_unique_pretplati = unique_month.tag ? unique_month.tag : false;
          var novi_month_tag = monthly.tag ? monthly.tag : false;
          
          
          // ako su obje mjesečne pretplate sa tagom null tj ne postoje  tj  nisu TELE2 ili SIGNIFY
          if ( tag_u_unique_pretplati == false && novi_month_tag == false ) { 
            
            window.monthly_subs_bez_ponavljanja[unique_month_index] = monthly;
            prepisao_postojeci_monthly = true;
            
          } else {
            // ako obe mjesečne pretplate imaju isti tag
            if ( unique_month.tag == monthly.tag ) {
              window.monthly_subs_bez_ponavljanja[unique_month_index] = monthly;
              prepisao_postojeci_monthly = true;
            };
            
          };
         
        };
        
      }); // loop po unique mjesecnim pretplatama
      
      // ako nisam našao već postojeći monthly product onda ga dodaj kao novi product
      if ( prepisao_postojeci_monthly == false ) window.monthly_subs_bez_ponavljanja.push(monthly);
      
    }); // loop po mjesecnim pretplatama
    
    
    $.each( window.monthly_subs_bez_ponavljanja, function( monthly_index, monthly ) {
      
      var park_name = '';
      $.each( window.global_park_list, function( park_index, park_obj ) {
        if ( park_obj.pp_sifra == monthly.pp_sifra ) park_name = park_obj.pp_name;
      });

      var duration_html = '';
      var monthly_valid_to = `<span>${ window.we_trans( 'cl__monthly_valid_to span', window.we_lang ) }</span>`;
      
      if ( monthly.type == 'vip' ) {
        // duration_html = `<img class="we_inf_icon" src="img/infinity_icon.png" />`;
        duration_html = `VIP`;
        
      } else {
      
        var time_now = Date.now();
        var time_left = monthly.valid_to - time_now;

        var time_unit = 'd';
        var min_left = Math.floor( time_left / ( 1000*60 ) );
        var hours_left = Math.floor( time_left / ( 1000*60*60 ) );
        var days_left = Math.floor( time_left / ( 1000*60*60*24 ) );

        if ( days_left == 0 ) { days_left = hours_left; time_unit = 'h'};
        if ( hours_left == 0 ) { days_left = min_left; time_unit = 'm'};

      
        duration_html = `${days_left}<span>${time_unit}</span>`;
        monthly_valid_to = `exp: ${getMeADateAndTime(monthly.valid_to).datum}`;

      };
        
      
      var monthly_row = 
          `
          <div class="monthly_product" id="${monthly._id}">
            
            <div class="monthly_pp_icon_box">
              ${duration_html}
            </div>

            <div class="pp_name_and_valid_to_box">
              <div class="park_name">${park_name}</div>
              <div class="monthly_valid_to">
                ${monthly_valid_to}
              </div>
            </div>
          </div> 

          `;
      
      monthly_list += monthly_row;
      
    });
    
    monthly_list = 
      `<div id="monthly_products_label">
        ${window.we_trans( 'id__monthly_products_label', window.we_lang) }
      </div>` + monthly_list;
    
    $('#monthly_products_box').html(monthly_list);
    
  };
  this_module.create_monthly_list = create_monthly_list;

  
  function update_user_balance(user_products) {
    
    var monthly_list = '';
    var balance = user_products.balance_sum;
    
    var balance_html = `${ zaokruziIformatirajIznos(balance) }<div id="currency_label">kn</div>`;
    
    $('#account_balance_number').html(balance_html);
    
    
  };
  this_module.update_user_balance = update_user_balance;
  
  
  function create_card_num_list(user_products) {
    
    if ( user_products.cards && user_products.cards.length > 0 ) {
      
      var all_card_nums = "";
      $.each( user_products.cards, function( card_num_index, card ) {
      
        var card_num_row = `<div class="we_drop_list_item" id="${card.sign}">${card.num}</div>`;
      
        all_card_nums += card_num_row;
      });
       $('#customer_credit_card_list .we_drop_list_items').html(all_card_nums);

    } else {
      
$('#customer_credit_card_list .we_drop_list_items')
.html(
`<div class="we_drop_list_item" id="${'no_cards'}">
${window.we_trans( 'id__no_cards', window.we_lang) }
</div>`
);  
    };
    
    this_module.initial_setup_of_customer_credit_card_list();
    
  };
  this_module.create_card_num_list = create_card_num_list;
  
  
  function validate_amount_value() {
    
    var amount_is_ok = true;
    
    // ako nije pretplata za specifični parking
    // i ako postoji neki broj u polju za upis nadoplate 
    if (
      window.we_selected_offer_pp == 'none'   &&
      $('#add_to_balance_input').val() !== '' &&
      $('#add_to_balance_input').val() !== this_module.desc_input_place_holders.add_to_balance_input
    ) {
      window.we_selected_amount = $('#add_to_balance_input').val() + '';
    };

    if ( window.we_selected_amount !== 'none' ) {

      var provjera_koliko_ima_tocaka = window.we_selected_amount.indexOf('.');
      var provjera_koliko_ima_zareza = window.we_selected_amount.indexOf(',');
      var provjera_koliko_ima_minusa = window.we_selected_amount.indexOf('-');

      if ( 
        provjera_koliko_ima_tocaka > -1 ||
        provjera_koliko_ima_zareza > -1 ||
        provjera_koliko_ima_minusa > -1 ||
        !$.isNumeric(window.we_selected_amount) 
        ||
        ( $.isNumeric(window.we_selected_amount) && Number(window.we_selected_amount) < 20  )

      ) {

        $('#add_to_balance_input').val('');
        amount_is_ok = false;
        window.we_selected_amount = 'none';
        popup_amount_nije_ispravan();

      };
    
      // ako je user upisao neki iznos tj nije prazno polje
    } else {
      amount_is_ok = false;
    }; 
    
    return amount_is_ok;

    
  };
  this_module.validate_amount_value = validate_amount_value;
  
            
  function process_duration(month_selected) {      

    var text_cijene = '';

    var discount_koeficijent = 0.9;
    if ( Number( window.we_selected_offer_pp) == 14 ) discount_koeficijent = 0.75;

    if ( month_selected == '1_month_sub' ) {
      text_cijene = this_module.generate_offer_banner(1);
      // 10 posto popusta
      window.we_selected_amount = window.cijene_parkinga[window.we_selected_offer_pp]*discount_koeficijent + '';
      // ako user izabere pretplatu i 1 mj
      window.we_selected_duration = '1';
    };


    if ( this.id == '2_month_sub' ) {
      text_cijene = this_module.generate_offer_banner(2);
      // 20 posto popusta
      window.we_selected_amount = window.cijene_parkinga[window.we_selected_offer_pp]*discount_koeficijent*2 + ''; //*0.85*2 + '';
      // ako user izabere pretplatu i 2 mj
      window.we_selected_duration = '2';
    };


    if ( this.id == '3_month_sub' ) {
      text_cijene = this_module.generate_offer_banner(3);
      // 30 posto popusta
      window.we_selected_amount = window.cijene_parkinga[window.we_selected_offer_pp]*discount_koeficijent*3 + ''; //*0.8*3 + '';
      // ako user izabere pretplatu i 3 mj
      window.we_selected_duration = '3';
    };    


    return text_cijene;

  };
  this_module.process_duration = process_duration;
  
  
            
    
  function get_ponuda_count() {
    
    return new Promise(function(resolve, reject) {
    
      var tag = null;
      
      var ima_ovu_pretplatu_aktivnu = false;
              
      $.each( window.monthly_subs_bez_ponavljanja, function(sub_index, sub) {
        // ako u aktivnim pretplatama već ima ovu pretplatu
        if ( sub.pp_sifra == window.we_selected_offer_pp ) {
          ima_ovu_pretplatu_aktivnu = true;
          tag = sub.tag || null;
        };
      });
      
      
      // SAMO ako nije dobio tag od postojeće aktivne pretplate
      // onda ako postoji default sub uzmi tu sifru pp sifru
      if ( tag == null && this_module.def_subs[window.we_selected_offer_pp] ) tag = this_module.def_subs[window.we_selected_offer_pp];
      
      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: ( 
          window.remote_we_url                  + 
          '/get_active_pp_and_tag_products/'    +
          window.we_selected_offer_pp           + 
          '/' + (tag || 'none')                 + 
          '/' + Date.now()                      + // sada
          '/' + (Date.now() + (1000*60*60*24*30.5) ) // kraj je mjesec dana u prosjeku uzimam
        ),
        dataType: "json"
      })
      .done(function (response) {

        if ( response.success == true ) {

          var max_count = null;

          console.log(response);

          $.each( window.global_park_list , function(pp_ind, park_obj) {

            if ( park_obj.pp_sifra == window.we_selected_offer_pp ) {

              if ( tag !== null ) {

                max_count = park_obj.special_places[tag].count_out;

              } else {

                max_count = park_obj.pp_count_out_num;

              };

              var dostupno = max_count - response.count;
              
              
              // nemoj mu dati da plati novu pretplatu samo ako:
              // ima nula slobodnih pretplata
              // i ako user nema tu pretplatu aktivnu
              if ( dostupno <= 0 && ima_ovu_pretplatu_aktivnu == false ) {
                
                $('#pay_with_token_btn').css({ 'opacity': '0.3', 'pointer-events': 'none' });
                $('#open_ws_pay_IAB').css({ 'opacity': '0.3', 'pointer-events': 'none' });
                
                popup_sve_pretplate_kupljene({pp_name: park_obj.pp_name });
                resolve(false);
                
              } else {
                
                $('#pay_with_token_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
                $('#open_ws_pay_IAB').css({ 'opacity': '1', 'pointer-events': 'all' });
          
                resolve(true);
                
              };

            }; // END if da u listi parkinga pronađe ovu koju je user izabrao !!!

          });

        } else {
          console.error('greška pri provjeri slobodnih pretplata ------> SUCCESS == FALSE');  
          resolve(false);
        };

      })
      .fail(function (err) {
        console.error('greška pri provjeri slobodnih pretplata ------> REQUEST FAIL');
        console.log(err);
        resolve(false);
      });

    }); // kraj promisa  
      
  };
  this_module.get_ponuda_count = get_ponuda_count;
  
          
  
  
  function choose_offer(e) {
    
    // alert('clicked on sort');
    
    
    e.stopPropagation();
    e.preventDefault();
    
        
    var item_list = $('#choose_offer_list .we_drop_list_items');
    var parent_height = $('#choose_offer_list').height() - 1;
    var arrow = $('#choose_offer_list .we_drop_list_arrow');
    var list_height = item_list.height();
    
    // ako je visina liste i visina input polja ista to znači da je u stanju neaktivno
    if ( list_height == parent_height ) {
      
      // ----------------------- LISTA ZATVORENA PA JE OTVARAM -----------------------
      // ----------------------- LISTA ZATVORENA PA JE OTVARAM -----------------------
      // ----------------------- LISTA ZATVORENA PA JE OTVARAM -----------------------
      
      
      
      $('#choose_offer_box').css({
        'z-index': '999',
      });

      var items_count = $('#choose_offer_list .we_drop_list_item').length;
      $('#choose_offer_list .we_drop_list_item').css('height', '40px');
      
      
      
      item_list.height( items_count * 40 );
      item_list.css({
        'border': '1px solid #fff',
        'top': '0px'
      });
      arrow.css('transform', 'rotate(180deg)');
      
      
    } 
    else {
      
      
      // ----------------------- LISTA OTVORENA PA JE ZATVARAM -----------------------
      // ----------------------- LISTA OTVORENA PA JE ZATVARAM -----------------------
      // ----------------------- LISTA OTVORENA PA JE ZATVARAM -----------------------
      
      
      var selected_item = e.target;
      
      // pazi da user nije možda kliknuo na malu strelicu pokraj
      // mora biti neki item na listi
      
      if ( 
        $(selected_item).hasClass('fa-caret-down') == false &&
        $(selected_item).parent().hasClass('we_drop_list_items')
      ) {
        
        if ( selected_item.id == 'for_balance') {
          
          
          // ako je user izabrao nadoplatu omogući klikanje na plaćanje karticom
          $('#pay_with_token_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
          $('#open_ws_pay_IAB').css({ 'opacity': '1', 'pointer-events': 'all' });
          

          window.we_selected_offer = 'nadoplata';
          // ako user izabere nadoplatu onda je duration none
          window.we_selected_duration = 'none';
          // i parking id je none
          window.we_selected_offer_pp = 'none';
          
          // resetiraj iznos ako je ostao od prijašnjeg plaćanja
          window.we_selected_amount = 'none';
          $('#add_to_balance_input').val('');

          $('#choose_duration_in_months').addClass('cit_hide');

          // prikazi input za iznos samo ako je se nalaziš u formi za plaćanje karticom
          if ( window.card_box_open == true ) {
            $('#add_to_balance_input').removeClass('cit_hide');
          } else {
            // ako je plaćanje voucherom onda sakrij input za iznos
            $('#add_to_balance_input').addClass('cit_hide');
          };

          // obriši html sa opisom paketa pretplate
          $('#cijena_odabranog_paketa').html('');


          var desc_text = '';
          if ( window.voucher_box_open == true) 
            desc_text = `<span id="offer_desc_voucher">${window.we_trans( 'id__offer_desc_voucher', window.we_lang) }</span>`;
          if ( window.card_box_open == true) 
            desc_text = `<span id="offer_desc_card">${window.we_trans( 'id__offer_desc_card', window.we_lang) }</span>`;

          $('#offer_desc').html(desc_text);


        } 
        else {
          
          // AKO JE KLIKNUO NA NEKU OD PRETPLATA !!!
          // AKO JE KLIKNUO NA NEKU OD PRETPLATA !!!
          // AKO JE KLIKNUO NA NEKU OD PRETPLATA !!!
          
          window.we_selected_offer = 'pretplata';
          window.we_selected_offer_pp = selected_item.id.replace('mj_pp_', '');
          
          

          this_module.get_ponuda_count()
          .then(
          function(result) {
          
            
            if ( result == true ) {
            

                $('#choose_duration_in_months').removeClass('cit_hide');
                $('#add_to_balance_input').addClass('cit_hide');

                var month_selected = $('#choose_duration_in_months .cit_radio_item.cit_selected')[0].id;


                var discount_koeficijent = 0.9;
                if ( Number( window.we_selected_offer_pp) == 14 ) discount_koeficijent = 0.75;


                var text_cijene = this_module.process_duration(month_selected);


                $('#cijena_odabranog_paketa').removeClass('cit_visible');
                $('#cijena_odabranog_paketa').html(text_cijene);
                setTimeout( function() { $('#cijena_odabranog_paketa').addClass('cit_visible'); }, 50);

                var desc_text = `<span id="offer_desc_subs">${window.we_trans( 'id__offer_desc_subs', window.we_lang) }</span>`

                $('#offer_desc').html(desc_text);

            } else {
              
              // klikni nazad na NADOPLATU da se lista otvori
              setTimeout( function() { $('#for_balance').click(); }, 300);
              
              // klikni opet na NADOPLATU da se lista ZATVORI!!!!!
              setTimeout( function() { $('#for_balance').click(); }, 1000);
              
              
            };

            
          })
          .catch(
          function(err) {
          
          
          });
          

          // kraj jeli nadoplata ili sub
        }; 

        $('#choose_offer_list .we_drop_list_item').css('height', '24px');
        item_list.prepend( $(selected_item) );

        setTimeout( function() {
          item_list.css({
            'border': '0px solid #fff',
            'top': '1px'
          });
          item_list.height(parent_height);
          arrow.css('transform', 'rotate(0deg)');
          $('#choose_offer_box').css({
            'z-index': '111',
          });
        }, 100);
        
      }; // ako nije kliknio na strelicu
      
    }; // kraj else-a ako zatvaram listu
    
  };
  this_module.choose_offer = choose_offer;

    
  function get_avail_to_buy(arg_pp_sifra) {
    
    
    return new Promise( function(resolve, reject) {
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( window.remote_we_url + '/get_active_pp_and_tag_products/' + this_module.cit_data.pp_sifra + '/' + 'none' + '/' + Date.now() ),
      dataType: "json"
    })
    .done(function (response) {

      if ( response.success == true ) {
        
        var max_count = null;
        
        console.log(response);
        
        if ( arg_pp_sifra !== 'all' ) {
          
          $.each(window.global_park_list, function(pp_ind, park_obj) {
          
            if ( park_obj.pp_sifra == arg_pp_sifra ) {
              var max_avail = park_obj.pp_count_out_num;
              var avail_count = max_count - response.count;
              resolve(avail_count);
            };
            
          });
          
        } 
        else {
        
          
          $.each(window.global_park_list, function(pp_ind, park_obj) {

            var max_avail = park_obj.pp_count_out_num;

            $('#choose_offer_list .we_drop_list_item').each( function() {

              var pp_sifra_in_list = this.id.replace('mj_pp_', '');

              if ( park_obj.pp_sifra == pp_sifra_in_list) {

                var avail_count = max_count - response.count;

                $(this).find('.avail_to_buy').text( parseInt(avail_count) );


              }; // ako je našao odgovarajući red za park

            }); // loop svih redova u listi

          }); // lopp svih parkova
          

        };
        

      } else {
        
        console.error('greška pri provjeri slobodnih pretplata ------> SUCCESS == FALSE');  
        
      };

    })
    .fail(function (err) {
      
      console.error('greška pri provjeri slobodnih pretplata ------> REQUEST FAIL');  

    });
      
    }); // kraj promisa
    
    
  };
  this_module.get_avail_to_buy = get_avail_to_buy;
  
  
  function generate_offer_banner(month) {
    
    var price = window.cijene_parkinga[window.we_selected_offer_pp];
    var popust = 10; 
    
    var discount_perc = 10;
    if ( Number( window.we_selected_offer_pp) == 14 ) discount_perc = 25;
    
    
    if (month == 1) popust = discount_perc; 
    if (month == 2) popust = discount_perc; // prije je bilo 15
    if (month == 3) popust = discount_perc; // prije je bilo 20
    
    var ukupna_cijena = price * ((100 - popust)/100) * month;
    var mj_cijena = ukupna_cijena/month;
    
    var w = window;
    var banner_html = 
        `${ukupna_cijena} <span style="font-size: 11px;">kn</span>
        <br>
        <div class="offer_banner">${mj_cijena} kn / <span id="skracenica_mj">${w.we_trans( 'id__skracenica_mj', w.we_lang) }</span> 
        <div class="perc_circle">-${popust}%</div></div>
        `;
  
    return banner_html;
    
  };
  this_module.generate_offer_banner = generate_offer_banner;
  
  window.we_selected_offer_pp

  
  function choose_duration(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    
    
    $('#choose_duration_in_months .cit_radio_item').removeClass('cit_selected');
    $(this).addClass('cit_selected');
    
    
    var discount_koeficijent = 0.9;
    if ( Number( window.we_selected_offer_pp) == 14 ) discount_koeficijent = 0.75;

    var month_selected = this.id;
    var text_cijene = process_duration(month_selected);
    
    
    $('#cijena_odabranog_paketa').removeClass('cit_visible');
    $('#cijena_odabranog_paketa').html(text_cijene);
    setTimeout( function() { $('#cijena_odabranog_paketa').addClass('cit_visible'); }, 50);
    
  };
  this_module.choose_duration = choose_duration;
  
  
  function choose_credit_card(e) {
    
    // alert('clicked on sort');
    
    
    e.stopPropagation();
    e.preventDefault();
    
        
    var item_list = $('#customer_credit_card_list .we_drop_list_items');
    var parent_height = $('#customer_credit_card_list').height() - 1;
    var arrow = $('#customer_credit_card_list .we_drop_list_arrow');
    var list_height = item_list.height();
    
    
    if ( list_height == parent_height ) {
      
      
      $('#we_credit_card_box').css({
        'z-index': '999',
      });

      var items_count = $('#customer_credit_card_list .we_drop_list_item').length;
      $('#customer_credit_card_list .we_drop_list_item').css('height', '40px');
      
      
      
      item_list.height( items_count * 40 );
      item_list.css({
        'border': '1px solid #fff',
        'top': -(items_count * 40/2 ) + 'px'
      });
      arrow.css('transform', 'rotate(180deg)');
      
      
    } else {
      
      var selected_item = e.target;
      
      // pazim ako je user kliknuo na malu strelicu --- nju na smjem tretirati kao da je dio liste !!!!
      // pazi da user nije možda kliknuo na malu strelicu pokraj
      if ( 
        $(selected_item).hasClass('fa-caret-down') == false &&
        $(selected_item).parent().hasClass('we_drop_list_items')
      ) {
        
        window.we_selected_card = selected_item.id;

        $('#customer_credit_card_list .we_drop_list_item').css('height', '24px');

        item_list.prepend( $(selected_item) );

        setTimeout( function () {

          item_list.css({
            'border': '0px solid #fff',
            'top': '1px'
          });

          item_list.height(parent_height);
          arrow.css('transform', 'rotate(0deg)');

          $('#we_credit_card_box').css({
            'z-index': '111',
          });        


        }, 100);
      }

    };
    
    
    
  };
  this_module.choose_credit_card = choose_credit_card;

  
  function click_on_desc_input(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    if ( $(this).val() == this_module.desc_input_place_holders[this.id] ) $(this).val('');
    
    // ako se radi o voucher input polju onda promijeni css od cijelog page-a
    // tako da sa flex end pogurem sve elemente na dno stranice
    // ako to ne napravim onda input polje ostane ispod tipkovnice !!!
    if ( this.id == 'voucher_number_input' ) $('#account_page_body').css('justify-content', 'flex-end');
    
    $(this).css('color', '#fff');
    
  };
  this_module.click_on_desc_input = click_on_desc_input;
  
  function add_to_balance_input(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    clearTimeout( window.add_to_balance_timeout_id );
    
    window.add_to_balance_timeout_id = setTimeout( function() {
      this_module.validate_amount_value();
    }, 1500);  
    
  };
  this_module.add_to_balance_input = add_to_balance_input;
  
  
  function voucher_number_input(e) {
    
    e.stopPropagation();
    e.preventDefault();
 
    window.voucher_code = $(this).val();
    
  };
  this_module.voucher_number_input = voucher_number_input;
  
  
  function reset_desc_input(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    // kada se se tipkovnica spušta onda postavi flex css na sredinu  i to će pogurati sve elemente na stranici u centar
    // ( kada je tipkovnica otvorena onda je flex end tako da svi elementi budu pogurani na dno stranice )
    $('#account_page_body').css('justify-content', 'center');
    
    if ( $(this).val() == '' ) {
      $(this).val( this_module.desc_input_place_holders[this.id] );
      $(this).css('color', '#2dc1de');
    };
  };
  this_module.reset_desc_input = reset_desc_input;
  
  
  function show_card_iframe() {
    
    
    var is_amount_ok = this_module.validate_amount_value();
    if ( !is_amount_ok ) return;
    
    
    if ( window.we_selected_amount == 'none' ) {
      popup_no_amount_for_card_pay();
      return;
    };
    
    
    if (!cit_user) {
      popup_nedostaje_user();
      return;
    };
    

    $('#card_iframe').attr(
      'src', 
      `https://wepark.circulum.it:9000/pay_card_page?user_number=${cit_user.user_number}&offer=${window.we_selected_offer}&duration=${window.we_selected_duration}&pp=${window.we_selected_offer_pp}&amount=${window.we_selected_amount}`
    );

    $('.big_page.cit_active')
      .not('#card_iframe_box')
      .not('#park_place_page')
      .addClass('cit_hide cit_right')
      .removeClass('cit_active');

    $('#card_iframe_box')
      .addClass('cit_active')
      .removeClass('cit_hide cit_bottom');

  };
  this_module.show_card_iframe = show_card_iframe;
  
  
  function update_account_podatke() {
    
    get_user_products_state().then(
      function(response) {
        if ( response ) {
          this_module.create_monthly_list(response);
          this_module.update_user_balance(response);
          this_module.create_card_num_list(response);

          setTimeout(function() {this_module.initial_setup_of_customer_credit_card_list();}, 300);

          this_module.initial_setup_of_choose_offer_list();

        };
      },
      function(error) { console.log(error)}
    );    
    
    
  };
  this_module.update_account_podatke = update_account_podatke;
  
  
  function close_iframe_box(e) {
    e.stopPropagation();
    e.preventDefault();
    $('#account_button').click();
  };
  this_module.close_iframe_box = close_iframe_box;
  
  
  function send_token_ajax() {

    var data_for_token_pay = {
      user_number: cit_user.user_number,
      offer: window.we_selected_offer,
      duration: window.we_selected_duration,
      pp: window.we_selected_offer_pp,
      /* selected amount je uvijek cijeli broj bez obzira je upisan u polje za balance ili je cijena pretplate */
      amount: (window.we_selected_amount + '00'),
      sign: window.we_selected_card,
      discount: window.we_discount,
      discount_id: window.we_discount_id,
    };

    
    // return;

    show_we_global_loader(true, 'POZIVAM WSPAY SERVIS');

    $.ajax({
      type: 'POST',
      url: window.remote_we_url + '/pay_with_token',
      data: JSON.stringify(data_for_token_pay),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function(response) {
      
      $('#pay_with_token_btn').css('pointer-events', 'all');  

      if ( response.success == true ) {


        $('#close_credit_card_box').click();
        $('#add_to_balance_input').val('');
        $('#add_discount_code').val('');

        show_we_global_loader(false, 'POZIVAM WSPAY SERVIS');
        setTimeout( function() { popup_pay_with_token_success(); }, 300);

        setTimeout( function() { this_module.update_account_podatke(); }, 1000);

      } else {

        show_we_global_loader(false, 'POZIVAM WSPAY SERVIS');
        setTimeout( function() { popup_pay_with_token_error(response.msg || null ); }, 300);

      };


    })
    .fail(function(error) {
      $('#pay_with_token_btn').css('pointer-events', 'all');  
      show_we_global_loader(false, 'POZIVAM WSPAY SERVIS');
      setTimeout( function() { popup_pay_with_token_error( response.msg || null ); }, 300);
      console.log(error);
    });

    
  };
  this_module.send_token_ajax = send_token_ajax;
  
  function pay_with_token() {
    
    // return;
    
    
    $('#pay_with_token_btn').css('pointer-events', 'none');
    
    
    var is_amount_ok = this_module.validate_amount_value();
    if ( !is_amount_ok ) {
      popup_no_amount_for_card_pay();
      $('#pay_with_token_btn').css('pointer-events', 'all');
      return;
    };
    
       
    if ( !window.we_selected_card ) {
      // ako nije odabrana niti jedna kartica s tokenom onda odaeri prvu
      window.we_selected_card = $('#customer_credit_card_list .we_drop_list_item').eq(0)[0].id;
    };
    
    
    
    if ( window.we_selected_card == 'no_cards' ) {
      
      $('#pay_with_token_btn').css('pointer-events', 'all');  
      
      popup_no_cards();
      
      return;
    }; 
      

    // ako je kod polj za discount različito od defaultnog placeholder texta 
    // i ako je različito od praznog stringa
    if (
      $('#add_discount_code').val() !== this_module.desc_input_place_holders.add_discount_code
      &&
      $('#add_discount_code').val() !== ''
    ) {

      // provjeri jel kod za popust OK tako što pošaljem request !!!!!
      this_module.check_discount(  $('#add_discount_code').val() )
      .then( 
        function(is_ok) { if ( is_ok ) this_module.send_token_ajax();  }, 
        function(error) { console.log(error);  $('#pay_with_token_btn').css('pointer-events', 'all'); }
      );

    } 
    else {
      this_module.send_token_ajax();
    };

  };
  this_module.pay_with_token = pay_with_token;
    
  
  function pay_with_voucher() {
       
    $('#enter_voucher_button').css('pointer-events', 'none'); 
    
    
    if ( window.voucher_code == '' ) {
      $('#enter_voucher_button').css('pointer-events', 'all');
      popup_no_voucher_code();
      return;
    };
    
    
    var standardni_placeholder_text = this_module.desc_input_place_holders.voucher_number_input;
    
    if ( 
      $('#voucher_number_input').val() == standardni_placeholder_text ||
      $('#voucher_number_input').val() == ''
    ) {
      $('#enter_voucher_button').css('pointer-events', 'all');
      popup_no_voucher_code();
      return;
    };
    
    
    var data_for_voucher = {
      user_number: cit_user.user_number,
      offer: window.we_selected_offer,
      duration: window.we_selected_duration,
      pp: window.we_selected_offer_pp,
      code: window.voucher_code,
      price: window.we_selected_amount
    };


    show_we_global_loader(true, 'SAMO TRENUTAK');

    $.ajax({
      type: 'POST',
      url: window.remote_we_url + '/pay_with_voucher',
      data: JSON.stringify(data_for_voucher),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function(response) {

      $('#enter_voucher_button').css('pointer-events', 'all');

      if ( response.success == true ) {
        
        $('#close_voucher_box').click();
        $('#voucher_number_input').val('');

        show_we_global_loader(false, 'SAMO TRENUTAK');
        setTimeout( function() { popup_pay_with_voucher_success(response.msg); }, 300);

        this_module.update_account_podatke();

      } else {

        show_we_global_loader(false, 'SAMO TRENUTAK');
        var popup_msg = '';

        // ako nije neki sistemski error onda ga neće biti u responsu
        // to znači da je error ljudski tj da je voucher već iskorište ili je code za voucher krivi pa ga nisam mogao pronaći
        if ( !response.err ) popup_msg = response.msg;

        setTimeout( function() {
          popup_pay_with_voucher_error(popup_msg);
        }, 300);

      };


    })
    .fail(function(error) {
      $('#enter_voucher_button').css('pointer-events', 'all');
      show_we_global_loader(false, 'SAMO TRENUTAK');
      setTimeout( function() { popup_pay_with_voucher_error(); }, 300);
      console.log(error);
    });

  
    
  };
  this_module.pay_with_voucher = pay_with_voucher;
  
  // TODO - kasnije obrisati  VIŠE OVO NE KORISTIM !!!!!!!!!!!!!!!!!
  // TODO - kasnije obrisati  VIŠE OVO NE KORISTIM !!!!!!!!!!!!!!!!!
  // TODO - kasnije obrisati  VIŠE OVO NE KORISTIM !!!!!!!!!!!!!!!!!
  function before_in_app_browser_check_discount(is_token_payment) {
    
    // ako je kod različit od defaultnog placeholder texta 
    // i ako je različit od praznog stringa
    if (
      $('#add_discount_code').val() !== this_module.desc_input_place_holders.add_discount_code
      &&
      $('#add_discount_code').val() !== ''
    ) {

      this_module.check_discount(  $('#add_discount_code').val() )
      .then( 
        function(is_ok) { 
          if ( is_ok ) {
            this_module.show_in_app_browser(is_token_payment); 
          };
        }, 
        function(error) { console.log(error); }
      );

    } 
    else {
      
      this_module.show_in_app_browser(is_token_payment);
    };
    
  };
  this_module.before_in_app_browser_check_discount = before_in_app_browser_check_discount;
  
  
  function show_in_app_browser(is_token_payment) {
    

    var is_amount_ok = this_module.validate_amount_value();
    if ( !is_amount_ok ) {
      popup_no_amount_for_card_pay();
      return;
    };

    
    if ( window.we_selected_amount == 'none' ) { 
      popup_no_amount_for_card_pay();
      return;
    };
    
    if (!cit_user) {
      popup_nedostaje_user();
      return;
    };
    
    
    var card_sign = `&card_signature=none`;
    
    if (is_token_payment) card_sign = `&card_signature=${window.we_selected_card}`;
    
    
    // primjer
    // https://wepark.circulum.it:9000/pay_card_page?user_number=1009&offer=pretplata&duration=2&pp=5&amount=50000
    // https://wepark.circulum.it:9000/pay_card_page?user_number=1009&offer=nadoplata&duration=none&pp=none&amount=2000
    var url = 
window.remote_we_url + `/pay_card_page?` + 
`user_number=${cit_user.user_number}&` + 
`offer=${window.we_selected_offer}&` +
`duration=${window.we_selected_duration}&` + 
`pp=${window.we_selected_offer_pp}&`+ 
`amount=${window.we_selected_amount + '00' }&` + 
`discount=${window.we_discount}&` +
`discount_id=${window.we_discount_id}` + 
card_sign
    
    
    
    
    
    
    function in_app_exit(e) {
      
      console.log('exited in app browser');
      console.log(e);
      
      
      if ( 
        window.we_current_in_app_browser_url &&
        window.we_current_in_app_browser_url.indexOf('/payment_success') > -1
      ) {
        // klikni na park listu da user odmah vidi listu
        $('#close_credit_card_box').click();
        $('#add_to_balance_input').val('');
        $('#add_discount_code').val('');
        
        
        setTimeout( function() {
          this_module.update_account_podatke();
          window.we_current_in_app_browser_url = null;
        }, 500);
        
      };
      
    };
    
        
    function in_app_load_start(e) {
      
      console.log('load start in app browser');
      console.log(e);
      // primjer event objekta kojeg vraća 
      // {type: "loadstart", url: "https://formtest.wspay.biz/Authorization.aspx"}
      window.we_current_in_app_browser_url = e.url;
      
    };
    
    
    var in_app_browser_target = '_blank'; // '_self';
    var in_app_browser_options = 'location=yes';

    // ako je ovo iPhone
    if ( navigator.userAgent.indexOf('iPhone') > -1) {
      in_app_browser_options = 'location=yes,toolbar=yes';
    };
    
   
    // window.we_in_app_window = cordova.InAppBrowser.open(url, '_self', 'location=yes');  
    var inAppBrowserRef = cordova.InAppBrowser.open(url, in_app_browser_target, in_app_browser_options);

    inAppBrowserRef.addEventListener('exit', in_app_exit);
    inAppBrowserRef.addEventListener('loadstart', in_app_load_start);
    
  
  };
  this_module.show_in_app_browser = show_in_app_browser;
    
  
  function check_discount(dis_code) {
    
    var def = $.Deferred();
    
    $.ajax({
        type: 'GET',
        url: ( window.remote_we_url + '/check_discount_code/' + dis_code ),
        dataType: "json"
    })
    .done(function(response) {

        if ( response.success == true ) {
          
          
          // ne trebam prikazivati popup kada je uspješno provjerio jel kod za discount ok
          // window[response.msg](response.discount);
          
          window.we_discount = Number(response.discount);
          window.we_discount_id = dis_code;
          
          // ako je definiran amount
          if ( 
            window.we_selected_amount !== 'none' && 
            window.we_selected_amount !== ''     &&
            window.we_selected_amount !== null 
          ) {
            
              def.resolve(true);
            
          } else {
            // ako nije definiran amount iz nekog razloga
            def.resolve(false);
            
          };
          
        } else {
          
          if ( response.msg && typeof window[response.msg] == 'function' ) {
            window[response.msg]();
          };
          
          window.we_discount = 'none';
          window.we_discount_id = 'none';
          if ( response.err ) console.error(response.err);
          def.resolve(false);
        };
    })
    .fail(function(error) {
      window.we_discount = 'none';
      window.we_discount_id = 'none';
      console.error("ERROR ON CHECK DISCOUNT AJAX");
      console.error(error);
      popup_discount_code_error();
      def.resolve(false);
    });
      
    
    return def;
    
  };
  this_module.check_discount = check_discount;
  

  
  
  
  this_module.cit_loaded = true;
 
}
  
  
  
};