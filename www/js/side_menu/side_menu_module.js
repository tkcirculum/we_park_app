var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    
    var this_module = window['js/side_menu/side_menu_module.js']; 
    
    
    wait_for('true == true', function() {
      
      
    var component_id = null;
    
    

    // samo skraćujem jer mi se neda pisati :)   
    var w = window; 
      
    var component_html =
`
    <div id="side_nav_content">
      <img id="side_nav_logo" src="img/side_nav_logo.png" />
      
      <div id="side_menu_choose_lang_label" class="side_nav_row">${w.we_trans( 'id__side_menu_choose_lang_label', w.we_lang) }</div>
      
      <div id="lang_hr" class="side_nav_row we_lang_btn cit_active" style="padding-left: 0px">
        <img class="lang_flag" src="img/lang_hr.png" />
        <span id="lang_hr_span">${w.we_trans( 'id__lang_hr_span', w.we_lang) }</span>
      </div>
      
      <div id="lang_en" class="side_nav_row we_lang_btn" style="padding-left: 0px">
        <img class="lang_flag" src="img/lang_en.png" />
        <span id="lang_en_span">${w.we_trans( 'id__lang_en_span', w.we_lang) }</span>
      </div>
      
      <!--
      <div id="lang_de" class="side_nav_row we_lang_btn" style="padding-left: 0px">
        <img class="lang_flag" src="img/lang_de.png" />
        NJEMAČKI
      </div>
      -->
      
      <div id="reg_plate_menu_section" 
           style="float: left; width: 100%; clear: both; margin-bottom: 10px; padding: 10px 5px 0; box-sizing: border-box;">  

        <div id="reg_plate_label_and_input_in_menu" style="float: left; width: 100%; clear: both; margin-bottom: 10px; color: #444;">

          <div class="we_label" id="user_reg_plate_menu_label" style="clear: left;float: left; width: 80%; color: #444;">
            ${w.we_trans( 'id__user_reg_plate_menu_label', w.we_lang) }
          </div>
          <input class="we_account_input reg_plate_input" id="user_reg_plate_menu" type="text" autocomplete="off" 
                 style="width: 80%; float: left; margin: 0;" required>
          <div class="account_form_btn add_reg_plate_btn" id="add_reg_plate_btn_menu"><i class="fas fa-plus"></i></div>

        </div>


      </div>       
      
      
      
      
      <div id="screen_size_and_ver" class="side_nav_row">
        CSS screen: 360 X 750
      </div>
      
    </div>

`;
      
      
  var this_module = window['js/side_menu/side_menu_module.js'];     
   
  
      // ovo je arbitrarni uvijet  - uzeo sam prvi gumb u side menu (HRVATSKI) uvijet kad se pojavi ....
  wait_for( `$('#lang_hr').length > 0`, function() {
    
    var this_module = window['js/side_menu/side_menu_module.js'];


    $('body').off('click', '.we_lang_btn');
    $('body').on('click', '.we_lang_btn', function() {
      
      $('.we_lang_btn').removeClass('cit_active');
      $(this).addClass('cit_active');
      
      window.we_lang = this.id.replace('lang_', '');
      window.localStorage.setItem('window_we_lang', window.we_lang);
      
      window.run_we_trans();
      
      save_user_lang_in_db();

    });

    
    $('#add_reg_plate_btn_menu').off('click');
    $('#add_reg_plate_btn_menu').on('click', function(e) {
      
      
      // ova varijabla određuje da li da napravim update na userovom profilu u bazi 
      // isto određuje da li da pošaljem svim raspberrijima novo stanje registracija registracije !!!! ( to radi automatski nakon update usera )
      window.update_car_plates_in_db = true;
      
      // ako čitam string iz input polja
      // ovaj regex znači ostavi samo alfanumeric ali ipak ostavi i hrvatske znakove !!!
      var car_plate_text = $('#user_reg_plate_menu').val().replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '');
      // moram dodatno obrisati underscore jer iznekog razloga on se zvanično tretira kao alphanumeric ????? !!!!!!!
      car_plate_text = car_plate_text.replace(/\_/g, '');
      car_plate_text = car_plate_text.toUpperCase();

      if ( car_plate_text.length < 5 ) return;
        
      check_jel_nova_rega(null, car_plate_text)
      .then(
        function(result) { if (result == true) global_add_car_plate('menu') },
        function(fail) { conole.error(fail) }
      )
      
    });    
    
    
    if ( window.we_lang ) {
      setTimeout( function() {
        $('#lang_' + window.we_lang ).click();
      }, 222);
      
    };
    

    $('body').off('click', '.we_menu_icon');
    $('body').on('click', '.we_menu_icon', function() {
      $('#we_side_nav').addClass('cit_visible');
    });

    setTimeout( function() {

      var win_width = $(window).width();
      var win_height = $(window).height();
      $('#screen_size_and_ver').text(`CSS screen: ${win_width} X ${win_height} ${window.current_we_app_ver}`);

    }, 5000);  
    
    // prijavi listener za draganje side menu-a
    $('#side_nav_content').touchDrag( this_module.drag_callback, null, 'x' );
    
    

  $('#we_side_nav').off('click');
  $('#we_side_nav').on('click', function(e) {
    
    // ako user klikne na neko prazno mjesto  u side meniju onda ga zatvori
    if (e.target.id == 'we_side_nav') {    
      
      this_module.is_finger_on_side_nav = false;
      this_module.side_nav_delta_x = 0;
      
      $(this).removeClass('cit_visible');
    };

  });
  
      
    
    document.getElementById('side_nav_content').addEventListener('touchstart', this_module.finger_down, false );
    document.getElementById('side_nav_content').addEventListener('touchend', this_module.finger_up, false );
  
    
 
  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 10*1000 );         

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // 'js/side_menu/side_menu_module.js' se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window['js/side_menu/side_menu_module.js']; 
  
  // var show_popup_modal = window['/main_area/cit_popup_modal/cit_popup_modal.js'].show_popup_modal;
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    
    this_module.cit_data = {
      
    };
    
  };

  
  this_module.is_finger_on_side_nav = false;
  this_module.side_nav_delta_x = 0;  
  
  
  function finger_down() {
    
    $('#side_nav_content').css({
      'transition': 'unset',
    });
    
    this_module.is_finger_on_side_nav = true;
    
     // console.log('--------finger_down--------');
  };
  this_module.finger_down = finger_down;
  
  function finger_up() {
    // console.log('--------finger_up--------');
    
    this_module.is_finger_on_side_nav = false;
    
    // postavi css transiciju tako da imam animaciju
    $('#side_nav_content').css({
      'transition': 'transform 0.3s ease-out',
    });
    
    // ako je user prešao više od 80 pixela u desno
    // ONDA ZATVORI MENU !!!!!!!
    if ( this_module.side_nav_delta_x > 80 ) {
      
      // console.log('--------side_nav_delta_x > 100--------');
      
      // pomakni menu izvan ekrana
      $('#side_nav_content').css({
        'transform': 'translateX(110%)',
      });
      
      setTimeout( function() { 
        // nakon animacije obriši transform css i left css
        $('#we_side_nav').removeClass('cit_visible'); 
        $('#side_nav_content').css('left', 'unset'); 
        $('#side_nav_content').css('transform', ''); 
      }, 300);
      
        
    } else {
      
      // AKO JE USER POMAKNUO MENU MANJE OD 80 pixela ONDA OTVORI MENU !!!!!!!
      
      $('#side_nav_content').css({
        'transform': 'translateX(0)',
        'left': 'unset',
      });
      
      setTimeout( function() { 
        $('#side_nav_content').css('left', 'unset'); 
        $('#side_nav_content').css('transform', ''); 
      }, 300);
            
      
    };
    
    this_module.side_nav_delta_x = 0;
    
    
  };
  this_module.finger_up = finger_up;
  
  
  function drag_callback( event, deltaX, deltaY, startLeft, startTop ){
   
    
    this_module.side_nav_delta_x = deltaX;
    
    if ( deltaX < 0 ) $('#side_nav_content').css('left', 'unset');
    
    /*
    console.log(event);
    console.log('----------------');
    console.log(deltaX);
    console.log(deltaY);
    console.log(startLeft);
    console.log(startTop);
    
    */
   
  };
  this_module.drag_callback = drag_callback;
  
  
  /* ---------------------- TODO KASNIJE PREBACITI U POSEBAN FILE SIDE MENU --------------- */
  /* ---------------------- TODO KASNIJE PREBACITI U POSEBAN FILE SIDE MENU --------------- */
  /* ---------------------- TODO KASNIJE PREBACITI U POSEBAN FILE SIDE MENU --------------- */
  
  
  this_module.cit_loaded = true;
 
}
  
  
};