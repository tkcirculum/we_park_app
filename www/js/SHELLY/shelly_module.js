var module_object = {
  
  create: function( data, parent_element, placement ) {
    
    var component_id = null;
    
    var component_html =
    `
<div id="shelly_component"></div>
    `;
    
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
  },
 
scripts: function () {

  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module
  var this_module = window[module_url]; 

  
  function move_shelly(pp_sifra, shelly_name, user_number, up_or_down) {
    
    var def = $.Deferred();
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: (window.remote_we_url + '/move_shelly/' + pp_sifra + '/' + shelly_name + '/' + parseInt(user_number) + '/' + up_or_down  ),
      dataType: "json",
      timeout: 1000*12
    })
    .done(function (response) {

      if ( response.success == true ) {
        
        console.log( 'shelly barijera se otvorila !!!!!!!' );
        console.log( response );
        
        // window.ramp_is_open = true;
        window.barrier_is_open = true;
        
        def.resolve(response);
        
      } else {
        
        console.error('SUCCESS FALSE  pri otvaranju SHELLY BARIJERE !!!!!!!');  
        
        // window.ramp_is_open = null;
        window.barrier_is_open = null;
        
        def.resolve(response);
        
        
      };

    })
    .fail(function (fail_obj) {
      
      console.error('FAIL pri otvaranju SHELLY BARIJERE !!!!!!!');
      
      // window.ramp_is_open = null;
      window.barrier_is_open = null;
      
      def.reject(fail_obj);
      

    });
    
    return def;
    
    
  };
  
  this_module.move_shelly = move_shelly;
  
  
  

  this_module.cit_loaded = true;  

} /* kraj scripts djela  */
  
  
}; // kraj cijelog module objekta
