var module_object = {
  create: function( data, parent_element, placement ) {
    
    
    var this_module = window['js/park_page/park_page_module.js']; 
    
    wait_for('true == true', function() {
    
     
      
      if (data) {
        var this_module = window['js/park_page/park_page_module.js']; 
        console.log('UBACUJEM DATA U CIT DATA PROPERTY ZA PARK PAGE -------------------');
        console.log(data);
        this_module.cit_data = data;
      };

      var component_id = null;

      var w = window;  
      var component_html =
`
<div class="big_page_header">

  <div id="history_back_on_park_page" class="we_menu_back_arrow"><i class="fas fa-arrow-left"></i></div>
  <div id="park_page_header_label" style="flex: 1;">${ data ? data.pp_name : '' }&nbsp;</div>
  <div id="menu_btn_on_park_page" class="we_menu_icon"><i class="fas fa-bars"></i></div>

</div>

<div id="park_place_body">

  <a href="tel:00385923133015" id="help_tel_num_on_top"><img src="img/phone_icon.png"/> 00385 92 3133 015</a>
  
  
  <div id="parking_timer_box" class="cit_hide">
    <div id="parking_timer_label">${w.we_trans( 'id__parking_timer_label', w.we_lang) }</div>
    <div id="parking_timer">
      05:20
    </div>
  </div>
  
  <div id="park_place_image_box">
    <img src="${window.remote_we_url}/img/${ ( data && data.pp_images ) ? data.pp_images[0] : 'no_image_park_place.png' }" />
    
    <div id="park_page_curve"></div>
  </div>
</div>

`;
      
      
  var this_module = window['js/park_page/park_page_module.js']; 
      // ovo je arbitrarni uvijet  - uzeo sam prvi label u formi kao uvijet kad se pojavi ....
  wait_for( `$('#park_place_body').length > 0`, function() {
    
    
    // $('.cit_tooltip').tooltip();
    
    $('#minus_book_duration_btn').off('click');
    $('#minus_book_duration_btn').on('click', this_module.set_booking_duration(-1) );

    
    $('#plus_book_duration_btn').off('click');
    $('#plus_book_duration_btn').on('click', this_module.set_booking_duration(1) );
    
    
    $('#book_pp_btn').off('click');
    $('#book_pp_btn').on('click', this_module.potvrda_bookinga );
    
    
    $('#cancel_booking_btn').off('click');
    $('#cancel_booking_btn').on('click', this_module.STOP_booking );
    
    
    $('#enter_pp_btn').off('click');
    $('#enter_pp_btn').on('click', function() {
      
      var this_module = window['js/park_page/park_page_module.js']; 
      var park_data = this_module.cit_data;

      if ( park_data.broken == true ) {
        popup_ramp_is_broken();
        return;
      };

      
      if ( park_data.maint == true ) {
        popup_ramp_is_maint();
        return;
      };
      
      
      
      window.openinig_barrier = false;
      
      var button_selector = '#enter_pp_btn';
      
      
      set_button_in_progress_state(true, button_selector );

      if ( !cit_user || !cit_user.user_number ) {
        popup_nedostaje_user();
        set_button_in_progress_state(false, button_selector );
        return;
      };

      var park_data = this_module.cit_data;
      // ovaj tag kaže jel user specijalan kao npr TELE2 ili je običan ( onda je tag == null )
      var current_tag = write_special_tag_avail_count( [park_data] );
      // ako je gornja funkcija vratila ovaj dugački string to znači da trebam prikazati popup
      // i pitati usera jel želi ući kao običan user !!!
      if ( current_tag !== null && current_tag.indexOf('zelis_biti_obican_user_umjesto') > - 1  ) {
        var only_tag = current_tag.split('-')[1]; // TO JE NPR TELE2 ili slično...
        popup_become_regular_user(this_module.cit_data, only_tag, '#enter_pp_btn' );
        return;
      };    
      
      this_module.check_parking_and_open(current_tag, park_data)
      .then(
      function(response) {
        
        if ( response.success == true ) {
          
          var park_data = this_module.cit_data;
          

          if ( 
            park_data.ramps && 
            $.isArray(park_data.ramps) && 
            park_data.ramps.length > 0 
          ) {

            if ( window.we_user_lat == null ) {
              // popup_GPS_is_OFF();
              // set_button_in_progress_state(false, button_selector );
              this_module.open_with_BLE_or_PI_and_enter_parking(response.query_obj);
            } else {
              // var km_dist = geo_distance(park_data.pp_latitude, park_data.pp_longitude).km;
              // ako je udaljeniji od 500 metara nemoj mu dopustiti otvaranje
              // igonriraj udaljenost ako je za desktop testiranje !!!!!
              /*
              if ( km_dist > 0.5 && window.is_desktop == false && ima_pi_rampu ) {
                popup_predaleko_od_rampe();
                set_button_in_progress_state(false, button_selector );
                return;
              };
              */
              this_module.open_with_BLE_or_PI_and_enter_parking(response.query_obj);
            };
            
            // kraj ifa jel ima ili nema RPI
          }
          else {
            this_module.open_with_BLE_or_PI_and_enter_parking(response.query_obj);
          }; 
        }
        else {

          if ( response.msg && typeof window[response.msg] == 'function' ) {
            window[response.msg]();
          } else {
            popup_greska_prilikom_parkinga();
          };

          console.error(response.err);
          set_button_in_progress_state(false, button_selector );

        }; // ako je success true or false
      },
      function(error) {
        
          popup_greska_prilikom_parkinga();
          console.error(error);
          set_button_in_progress_state(false, button_selector );
        
      }); // kraj check parking and open
      
    });
    
    $('#exit_pp_btn').off('click');
    $('#exit_pp_btn').on('click', function() {
      
      var this_module = window['js/park_page/park_page_module.js']; 
      var park_data = this_module.cit_data;

      if ( park_data.broken == true ) {
        popup_ramp_is_broken();
        return;
      };
      
      if ( park_data.maint == true ) {
        popup_ramp_is_maint();
        return;
      };      
      
      
      window.openinig_barrier = false;
      this_module.open_with_BLE_or_PI_and_EXIT_parking();
    });
    
    $('#reklama_close_btn').off('click');
    $('#reklama_close_btn').on('click', function() { 
    
      var koef_size_x = $('#open_barrier_btn').width() / $(window).width();
      var koef_size_y = $('#open_barrier_btn').height() / $(window).height();

      var btn_top = $('#open_barrier_btn').offset().top;
      var btn_left = $('#open_barrier_btn').offset().left;


        $('#popup_reklama_za_barijere').css({
          'transform': `translate(${btn_left}px, ${btn_top}px) scale(${koef_size_x}, ${koef_size_y})`,
          'opacity': '1',
          'pointer-events': 'none',
          'background-color': '#0f68b4',
          'border-radius': '160px'
        });
      
        setTimeout(function() { 
          $('#popup_reklama_za_barijere').css({
            'opacity': '0',
          });
        }, 400);
      
        setTimeout(function() {
          $('#popup_reklama_za_barijere').removeClass('cit_animate'); 
        }, 700);
      
    });
    
    
    $('#open_barrier_btn').off('click');
    $('#open_barrier_btn').on('click', function() {
      
      
      var ZERO_MODULE = window['js/ZERO/zero_bar.js'];

      if ( 
        !window.cit_user
        ||
        ( window.cit_user && window.cit_user.user_number !== 777 && window.cit_user.user_number !== 9 && window.cit_user.email.indexOf('t.ht.hr') == -1 ) 
      ) { 
        // ako nema usera i nije user ---> niti karlo niti toni
        this_module.show_barrier_popup();
        return;
      };
      
      // ako park ima barijere
      if (
        this_module.cit_data.barriers             &&
        $.isArray(this_module.cit_data.barriers)  &&
        this_module.cit_data.barriers.length > 0 
      ) {
        

      // postavi da oba gumba i otvaranje i zatvaranje budu u stanju rotiranja zupčanika
      // ( ionako se ne vidi jedan od tih gumba )
      set_button_in_progress_state(true, '#open_barrier_btn');
      set_button_in_progress_state(true, '#close_barrier_btn'); 
        
        
        window.openinig_barrier = true;
        
        var curr_pp_sifra = this_module.cit_data.pp_sifra;   
        
        get_db_barrier_state(curr_pp_sifra, window.cit_user.user_number)
        .then(
        function(get_bar) {
          
          if ( !get_bar || get_bar.success !== true ) {
            
            set_button_in_progress_state(false, '#open_barrier_btn');
            set_button_in_progress_state(false, '#close_barrier_btn'); 
            return;
          };

          if ( get_bar.name && get_bar.name.indexOf('zero') > -1 ) {
            
            // ------------- AKO JE OVO ZERO BARIJERA -------------------
            // ------------- AKO JE OVO ZERO BARIJERA -------------------
            // ------------- AKO JE OVO ZERO BARIJERA -------------------
            
            var park_data = this_module.cit_data;
                    
    
            if ( park_data.main_ramp == false ) {
              ZERO_MODULE.check_park_and_ZERO_toggle(get_bar, park_data);
            } else {
              ZERO_MODULE.ZERO_barrier_toggle(get_bar, park_data);
            };
          }
          else if ( get_bar.name && get_bar.name.indexOf('shelly') > -1 ) {

            // ------------- AKO JE OVO SHELLY BARIJERA -------------------
            // ------------- AKO JE OVO SHELLY BARIJERA -------------------
            // ------------- AKO JE OVO SHELLY BARIJERA -------------------

            this_module.SHELLY_barrier_toggle(get_bar);

          
          } 
          else {

            // ------------- AKO JE OVO BLE BARIJERA -------------------
            // ------------- AKO JE OVO BLE BARIJERA -------------------
            // ------------- AKO JE OVO BLE BARIJERA -------------------

            this_module.BLE_barrier_toggle(get_bar);
          };

        },
        function( bar_error ) {
          
          console.log('ERROR OD GET DB STATE BARIJERE ---- ');
          console.error(bar_error);
                
          set_button_in_progress_state(false, '#open_barrier_btn');
          set_button_in_progress_state(false, '#close_barrier_btn');
      
        }); // kraj then od get db barrier state
          
      } 
      else {
        // ako park nema barijere
        this_module.show_barrier_popup();
        
      }; // kraj ifa ako ovaj park nema barijeru
      
      
    }); // kraj klika na open barier gumb
    
    $('#close_barrier_btn').off('click');
    $('#close_barrier_btn').on('click', function() {
      
      
      var ZERO_MODULE = window['js/ZERO/zero_bar.js'];
      
      if ( 
        !window.cit_user
        ||
        ( window.cit_user && window.cit_user.user_number !== 777 && window.cit_user.user_number !== 9 && window.cit_user.email.indexOf('t.ht.hr') == -1 )
      ) { 
        // ako nema usera i nije user ---> niti karlo niti toni
        this_module.show_barrier_popup();
        return;
      };
      
      
      window.openinig_barrier = true;
      
      // postavi da oba gumba i otvaranje i zatvaranje budu u stanju rotiranja zupčanika
      // ( ionako se ne vidi jedan od tih gumba )
      set_button_in_progress_state(true, '#open_barrier_btn');
      set_button_in_progress_state(true, '#close_barrier_btn'); 
      
      var curr_pp_sifra = this_module.cit_data.pp_sifra;
      get_db_barrier_state(curr_pp_sifra, window.cit_user.user_number)
        .then(
        function(get_bar) {
          
        if ( !get_bar || get_bar.success !== true ) {
          
          set_button_in_progress_state(false, '#open_barrier_btn');
          set_button_in_progress_state(false, '#close_barrier_btn'); 
          return;
        };


        if ( get_bar.name && get_bar.name.indexOf('zero') > -1 ) {

          // ------------- AKO JE OVO ZERO BARIJERA -------------------
          // ------------- AKO JE OVO ZERO BARIJERA -------------------
          // ------------- AKO JE OVO ZERO BARIJERA -------------------

          var park_data = this_module.cit_data;
          ZERO_MODULE.ZERO_barrier_toggle(get_bar, park_data);
        }
        else if ( get_bar.name && get_bar.name.indexOf('shelly') > -1 ) {

          // ------------- AKO JE OVO SHELLY BARIJERA -------------------
          // ------------- AKO JE OVO SHELLY BARIJERA -------------------
          // ------------- AKO JE OVO SHELLY BARIJERA -------------------

          this_module.SHELLY_barrier_toggle(get_bar);

        }           
        else {
          
          // ------------- AKO JE OVO BLE BARIJERA -------------------
          // ------------- AKO JE OVO BLE BARIJERA -------------------
          // ------------- AKO JE OVO BLE BARIJERA -------------------
          

          if ( this_module.cit_data.pp_sifra == '15' ) {
            this_module.BLE_barrier_toggle(get_bar);
          } else {
            this_module.show_barrier_popup();  
          };
          
          
        }; // kraj eles-a ---> ovo je BLA barijera
          
        // kraj get bar success 
        },
        function( bar_error ) {
          
          set_button_in_progress_state(false, '#open_barrier_btn');
          set_button_in_progress_state(false, '#close_barrier_btn'); 
          
          console.log('ERROR OD GET DB STATE BARIJERE ---- ');
          console.error(bar_error);
        }); // kraj then od get db barrier state
            
          
          
    });
    
    $('#show_barrier_map').off('click');
    $('#show_barrier_map').on('click', function() {
      
      // za svaki slučaj sakrij tooltip ako je prisutan
      $('#barrier_map_btn_tooltip_triangle').removeClass('cit_show');
      // povećaj broj na tri u loca storage tako da se više ne prikazuje tooltip
      window.localStorage.setItem('barrier_map_tooltip', '3');
      
      $('#barrier_map').css( {
        'transform': 'scale(1)',
        'pointer-events': 'all',
        'opacity': '1'
      });
      
      
    });
    
    $('#bar_map_zoom_close').off('click');
    $('#bar_map_zoom_close').on('click', function() {
      
      $('#barrier_map').css( {
        'transform': 'scale(0.6)',
        'pointer-events': 'none',
        'opacity': '0'
      });
      
      
    });
    
    
    
    
    
    
    $('#history_back_on_park_page').off('click');
    $('#history_back_on_park_page').on('click', we_history_back );
    
    /*
    $('#ramp_not_open_btn').off('click');
    $('#ramp_not_open_btn').on('click', popup_ramp_not_open );
    */
      
  }, 5000000 );      

    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
      
  }, 500000 );         

  },
 
scripts: function () {
 
  // VAŽNO !!!!  
  // 'js/park_page/park_page_module.js' se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window['js/park_page/park_page_module.js']; 
  
  
  // ako ne postoji ovaj property onda to znači da je ovo prvi load
  if ( !this_module.cit_loaded ) {
    console.log('RESETIRAO SAM CIT DATA PROPERTY ZA PARK PAGE -------------------');
    this_module.cit_data = {};
  };
  
  function show_barrier_popup() {
    
    // ----------------- AKO OVAJ PARK NEMA BARRIJERU -------------------------
    // ----------------- AKO OVAJ PARK NEMA BARRIJERU --------- prikaži popup reklamu ----------------
    // ----------------- AKO OVAJ PARK NEMA BARRIJERU -------------------------

    var koef_size_x = $('#open_barrier_btn').width() / $(window).width();
    var koef_size_y = $('#open_barrier_btn').height() / $(window).height();

    var btn_top = $('#open_barrier_btn').offset().top;
    var btn_left = $('#open_barrier_btn').offset().left;

    $('#popup_reklama_za_barijere').css({
      'transform': `translate(${btn_left}px, ${btn_top}px) scale(${koef_size_x}, ${koef_size_y})`
    });


    setTimeout(function() {
      $('#popup_reklama_za_barijere').addClass('cit_animate'); 
    }, 50);


    setTimeout(function() { 

      $('#popup_reklama_za_barijere').css({
        'transform': `translate(0px,0px) scale(1,1)`,
        'opacity': '1',
        'pointer-events': 'all',
        'background-color': '#fff',
        'border-radius': '0px'
      });

    }, 100);

    
  };
  this_module.show_barrier_popup = show_barrier_popup;
  
  
  function BLE_barrier_toggle(get_bar) { 
    
    var up_or_down = null;
    var old_state = null;
    
    if ( !window.cit_user || !window.cit_user.user_number ) {
      popup_nedostaje_user();
      return;
    };

    var curr_pp_sifra = this_module.cit_data.pp_sifra;

    // PAZI  - up or down je obrnut od trenutnog stanja
    // jer sada radim toogle !!!!!

    if ( get_bar.state == null || get_bar.state == 'reserved' ) {
      up_or_down = 'down';
      old_state = 'up';
    } else if ( get_bar.state == 'parked' ) {
      up_or_down = 'up';
      old_state = 'down';
    };

  if ( Date.now() - window.barrier_process_start < 1000*20 ) {
    return;
  };

  window.barrier_process_start = Date.now();

  // postavi da oba gumba i otvaranje i zatvaranje budu u stanju rotiranja zupčanika
  // ( ionako se ne vidi jedan od tih gumba )
  set_button_in_progress_state(true, '#open_barrier_btn');
  set_button_in_progress_state(true, '#close_barrier_btn');  

  window.barrier_is_open = false;

  var ble_module = window['js/BLE/BLE_module.js'];
  ble_module.BLE_scan_connect_and_open()
  .then( function(is_ble_enabled) { if ( !is_ble_enabled )  console.error('BARRIER DID NOT OPEN !!!!!!!!!!!!!!!!!!!!');  });

  // unutar BLE modula pretvorim barrier is open u true kada sve napravim s BLE komunikacijom po PS
  // ovdje čekam da se ta var pretvori u true
  // BLE BARRIER TOGGLE
  window.we_ramp_listener_id = wait_for( `window.barrier_is_open == true`,
  function(){ 

    window.ble_is_off = false;
    set_button_in_progress_state(false, '#open_barrier_btn');
    set_button_in_progress_state(false, '#close_barrier_btn');

    var curr_pp_sifra = this_module.cit_data.pp_sifra;
    update_db_barrier_state( curr_pp_sifra, get_bar.name, window.cit_user.user_number, up_or_down )
    .then(
      function(update_bar) { 

        if ( update_bar && update_bar.success == true ) {

          console.log(update_bar); 


          set_barrier_html( update_bar.name, up_or_down );
          setTimeout( function() { window.barrier_process_start = 0; }, 5000);

        } 
        else {

          if ( update_bar.msg && typeof window[update_bar.msg] == 'function' ) {
            window[update_bar.msg]();
          } else {
            popup_barrier_not_open();
          };

          set_barrier_html( update_bar.name, old_state );
          setTimeout( function() { window.barrier_process_start = 0; }, 300);

        };

        // kraj update bar response
      },
      function(error) {

        console.log(error);
        popup_barrier_not_open();

        set_barrier_html( get_bar.name, old_state );
        setTimeout( function() { window.barrier_process_start = 0; }, 300);

        // kraj update bar error
      });

    // kraj wait for success
  },
  1000*5,
  function() { 
    set_button_in_progress_state(false, '#open_barrier_btn');
    set_button_in_progress_state(false, '#close_barrier_btn');

    popup_barrier_not_open();
    set_barrier_html( get_bar.name, old_state );
    setTimeout( function() { window.barrier_process_start = 0; }, 300);

    //kraj wait for timeout
  });


    
    
  };
  this_module.BLE_barrier_toggle = BLE_barrier_toggle;
  
  function SHELLY_barrier_toggle(get_bar) {
    
    
    var up_or_down = null;
    var old_state = null;
    
    if ( !window.cit_user || !window.cit_user.user_number ) {
      popup_nedostaje_user();
      return;
    };

    if ( Date.now() - window.barrier_process_start < 1000*20 ) {
      return;
    };
    
    window.barrier_process_start = Date.now();

    window.barrier_is_open = false;

    var curr_pp_sifra = this_module.cit_data.pp_sifra;

    // OVO JE TOGGLE TJ MORAM SUPROTNO OD TRENUTNOG STANJA
    // ako je do sada null ili reserved onda ju otvori tj spusti
    if ( get_bar.state == null || get_bar.state == 'reserved' ) {
      up_or_down = 'down';
      old_state = 'up';
    } else if ( get_bar.state == 'parked' ) {
      up_or_down = 'up';
      old_state = 'down';
    };

    var curr_pp_sifra = this_module.cit_data.pp_sifra;
    var pi_module = window['js/SHELLY/shelly_module.js'];
    pi_module.move_shelly(curr_pp_sifra, get_bar.name, window.cit_user.user_number, up_or_down )
    .then(
      function(response) { 

        if ( response.success == true ) {
          console.log('barijera je reagirala uspješno !!!!!!!!!!!!!!!!'); 
          setTimeout( function() { window.barrier_process_start = 0; }, 5000);

        } else {
          if ( response.msg && typeof window[response.msg] == 'function' ) window[response.msg]();
        };

      },
      function(fail_obj) { 
        console.log(fail_obj); 

        if ( fail_obj.msg && typeof window[fail_obj.msg] == 'function' ) {
          window[fail_obj.msg](); 
        };
        /* popup_greska_prilikom_parkinga(); */ 
      });

    // SHELLY BARRIER TOGGLE

    window.we_ramp_listener_id = wait_for( `window.barrier_is_open == true`,
    function() { 

      window.shelly_listener_id = null;
      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');


      var curr_pp_sifra = this_module.cit_data.pp_sifra;
      update_db_barrier_state( curr_pp_sifra, get_bar.name, window.cit_user.user_number, up_or_down )
      .then(
        function(update_bar) { 

          if ( update_bar.success == true ) {

            console.log(update_bar); 

            set_barrier_html( update_bar.name, up_or_down );
            setTimeout( function() { window.barrier_process_start = 0; }, 5000);

          } else {

            if ( update_bar.msg && typeof window[update_bar.msg] == 'function' ) {
              window[update_bar.msg]();
            } else {
              popup_barrier_not_open();
            };

            set_barrier_html( update_bar.name, old_state );
            setTimeout( function() { window.barrier_process_start = 0; }, 300);

          };
          
          // kraj success-a za update bar
        },
        function(error) {

          console.log(error);
          popup_barrier_not_open();

          set_barrier_html( get_bar.name, old_state );
          setTimeout( function() { window.barrier_process_start = 0; }, 300);

        // kraj update bar;
        });

    // kraj wait for success  
    },
    1000*5,
    function() { 
      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');
      set_barrier_html( get_bar.name, old_state );
      popup_barrier_not_open();
    }
    ); // kraj wait for

  };
  this_module.SHELLY_barrier_toggle = SHELLY_barrier_toggle;
  
  // kraj run booking
  function book_park_place() {
    
    
    console.log(' ------------------- kliknuo na book park place button !!!! ')
    // samo za testiranj GUI-ja
    // set_booking_state( true, this_module.cit_data);
    // return;
    set_button_in_progress_state(true, '#book_pp_btn');
    
    if ( !cit_user || !cit_user.user_number ) {
      popup_nedostaje_user();
      $('#book_pp_btn').css('pointer-events', 'all' );
      return;
    };
    
    var park_data = this_module.cit_data;
        
    var time_stamp = Date.now();
    
    
    var book_duration_number = Number( $('#book_duration_num').text() );

    
    var current_tag = write_special_tag_avail_count( [park_data] );
    
    if ( current_tag !== null && current_tag.indexOf('zelis_biti_obican_user_umjesto') > - 1  ) {
      
      var only_tag = current_tag.split('-')[1];
      popup_become_regular_user(park_data, only_tag, '#book_pp_btn' );
      return;
    };
    
    var action_object = { 

      //* postojeci fieldovi  /  
      sifra : null,
      from_time: null,
      to_time: null,
      paid: null,  
      pp_sifra : String(this_module.cit_data.pp_sifra),
      user_number: cit_user.user_number,
      was_reserved_id: null,
      mobile_from_time: null,

      //* moji fieldovi  /  
      status: 'booked',
      user_set_book_duration: book_duration_number,

      reservation_from_time: time_stamp,
      reservation_to_time: null,
      reservation_expired: null,
      reservation_canceled: null,
      reservation_paid: null,
      
      
      current_price: this_module.cit_data.pp_current_price,

      pack_type: null,
      duration: null,
      revenue: null,
      owner_revenue: null,
      currency: 'kn',
      
      indoor: false,
      
      current_tag: current_tag,
      
      user_status: get_user_status(park_data)


    }; 

    console.log('prije ajaxa book park place');  
    console.log(this_module.cit_data);
      
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "POST",
      url: window.remote_we_url + "/new_booking",
      data: JSON.stringify(action_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.success == true ) {
        
        set_count_number(-1);
        
        this_module.current_park_action = response.action;
        
        set_button_in_progress_state(false, '#book_pp_btn');
        
        set_booking_state( true, this_module.cit_data, response.action );
        
        
      } 
      else {
        
        // unutar msg properija sam stavio ime funkcije koja poziva popup
        // pošto sam sve popup funkcije stavio u globalni window space onda ih zovem kao window propertije
        
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( window[response.msg] ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_bookinga();
        };
        set_button_in_progress_state(false, '#book_pp_btn');
      };
    })
    .fail(function(error) {
      
      console.log(error);
      
      if ( window[error.msg] ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_bookinga();
      };
      set_button_in_progress_state(false, '#book_pp_btn');
    });    

    
  };
  this_module.book_park_place = book_park_place;
    
  
  function potvrda_bookinga() {
    
    
    var this_module = window['js/park_page/park_page_module.js']; 
    var park_data = this_module.cit_data;

    if ( park_data.broken == true ) {
      popup_ramp_is_broken_booking();
      return;
    };

    
    
    var curr_pp_sifra = this_module.cit_data.pp_sifra;
    var current_price = 6;
    
    var user_ima_pretplatu_za_ovaj_pp = false;
    
      get_user_products_state().then(
      function(response) {

        if ( window.all_user_products && window.all_user_products.monthly_arr.length > 0 ) {

          $.each( window.all_user_products.monthly_arr, function(pretplata_index, pretplata ) {
            // ako pronađe da ima pretplatu za park u kojem se trenutno nalazi
            if ( Number(pretplata.pp_sifra) == Number(curr_pp_sifra) ) {
              // onda postavi da maximum sati za booking bude 3
              user_ima_pretplatu_za_ovaj_pp = true;
            };
          });
        };

        if ( user_ima_pretplatu_za_ovaj_pp == false ) {
          // ako user nema pretplatu za ovaj parking
          // onda troši PARE pa ga treba upozoriti
          get_park_list().then(
          function() {

            $.each( window.global_park_list, function(pp_index, park ) {
              if ( curr_pp_sifra == park.pp_sifra ) {
                current_price = park.pp_current_price;
              };
            }); // kraj loopa po svim parkovima 

            if ( current_price !== 0 ) popup_potvrda_bookinga(current_price, this_module.book_park_place);
            if ( current_price == 0 ) this_module.book_park_place();

          });
        } else {
          // ako user IMA pretplatu za ovaj parking !!!
          // onda NE TROŠI PARE i nije bitna potvrda !!!!
          this_module.book_park_place();

        };

    },
      function(error) { 
        console.error('greška kod dobivanja user producta iz baze prilikom popup-a za BOOKING');
        console.log(error)
      }
    );

    
  };
  this_module.potvrda_bookinga = potvrda_bookinga;
  
  
  function STOP_booking() {


  // samo za testiranj GUI-ja
  // set_booking_state( false, this_module.cit_data);
  // return;

    set_button_in_progress_state(true, '#cancel_booking_btn');
      

    if ( !cit_user || !cit_user.user_number ) {
      popup_nedostaje_user();
      set_button_in_progress_state(false, '#cancel_booking_btn');
      return;
    };

    var park_data = this_module.cit_data;

    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( window.remote_we_url + "/cancel_booking/" + park_data.pp_sifra + '/' + cit_user.user_number ),
      dataType: "json",
      })
    .done(function(response) {

      console.log(response);

      if ( response.success == true ) {

        this_module.current_park_action = response.action;
        
        
        set_count_number(1);

        set_booking_state( false, park_data);
        
        set_button_in_progress_state(false, '#cancel_booking_btn');  
        
        $('#park_place_info .book_indicator').remove();
        $('#park_place_info .parking_indicator').remove();
        
        // pokaži koliko je love user potrošio na booking
        var time_string = get_time_as_hh_mm_ss(response.action.book_duration);
        popup_show_paid_time(
          time_string, 
          zaokruziIformatirajIznos( response.action.reservation_paid), 
          response.action.currency,
          park_data.pp_sifra,
          response.action
        );
        
      } 
      else {
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( window[response.msg] ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_CANCEL_bookinga();
        };
        set_button_in_progress_state(false, '#cancel_booking_btn');
      };
    })
    .fail(function(error) {
      console.log(error);
      // ako postoji popup naslov u response msg onda ga pokreni !!
      if ( window[error.msg] ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_CANCEL_bookinga();
      };
      set_button_in_progress_state(false, '#cancel_booking_btn');
    });
        
    
  
  };
  this_module.STOP_booking = STOP_booking;
  
  
  
  function open_with_BLE_or_PI_and_enter_parking(query_obj) {


    var this_module = window['js/park_page/park_page_module.js']; 
    var park_data = this_module.cit_data;

    
    var last_open_ramp_time_stamp = {pp_sifra: this_module.cit_data.pp_sifra, time: Date.now() };
    
    window.localStorage.setItem('last_open_ramp_time_stamp',  JSON.stringify(last_open_ramp_time_stamp));
    
    // postavi ovu global varijablu na false i čekaj da se pretvori u true
    // to će se dogoditi u BLE modulu unutar funkcije OPEN barrier
    window.ramp_is_open = false;
    window.error_msg_from_RPI = false;
    window.last_open_ramp_instance_time = Date.now();
    
    window.ble_is_off = false;
    window.ramp_open_timeout_sec = 5;
    
    if ( window.is_desktop == false ) {
        
      // ako parking ima propery ramps i ako je to array i ako array nije prazan !!!
      // /////////////////////////// ako je PI RAMPA ///////////////////////////
      if ( 
        park_data.ramps && 
        $.isArray(park_data.ramps) && 
        park_data.ramps.length > 0 
      ) {

        // stavio sam da bude isto kao i za BLE, ali možda zbog problema s internetom treba biti veći timeout
        window.ramp_open_timeout_sec = 10;
        var pi_module = window['js/PI/pi_module.js'];
        pi_module.pi_otvori_rampu(park_data.ramps[0], cit_user.user_number)
        .then(
          function(response) { 
            if ( response.success == true )  {
              
              console.log('rampa se otvorila'); 
              
            } else {
              window.ramp_is_open = false;
              if ( 
                response.msg                                      && 
                typeof window[response.msg] == 'function'         &&
                response.msg !== 'popup_greska_prilikom_parkinga'
              ) {
                window.error_msg_from_RPI = true;
                
                set_button_in_progress_state(false, '#exit_pp_btn');
                set_button_in_progress_state(false, '#enter_pp_btn');
                // prikaži popup
                window[response.msg]();
              };
            };

          },
          function(fail_obj) { 
            console.log(fail_obj); 
            window.ramp_is_open = false;
              if ( 
                response.msg                                      && 
                typeof window[response.msg] == 'function'         &&
                response.msg !== 'popup_greska_prilikom_parkinga'
              ) {
              
              window.error_msg_from_RPI = true;
              set_button_in_progress_state(false, '#exit_pp_btn');
              set_button_in_progress_state(false, '#enter_pp_btn');
              
              window[fail_obj.msg](); 
            };
            /* popup_greska_prilikom_parkinga(); */ 
          });
      } 
      else {
        // /////////////////////////// ako je BLE RAMPA ///////////////////////////
        var ble_module = window['js/BLE/BLE_module.js'];
        ble_module.BLE_scan_connect_and_open()
        .then( function(is_ble_enabled) { if ( !is_ble_enabled )  console.error('BLE DID NOT OPEN !!!!!!!!!!!!!!!!!!!!');  });

      };

        
      window.we_ramp_listener_id = wait_for( `window.ramp_is_open == true`,
      function() { 

        var this_module = window['js/park_page/park_page_module.js']; 
        var park_data = this_module.cit_data;
        
        window.ble_is_off = false;
        window.we_ramp_listener_id = null;
        this_module.enter_pp(query_obj.current_tag, park_data);

      },
      1000*window.ramp_open_timeout_sec, 
      /* ( recorded_time, exit_pp_function, park_data, enter_pp_function, curr_action_tag ) { */
        wrapper_for_ramp_not_open(
          window.last_open_ramp_instance_time,
          null, 
          park_data,
          this_module.enter_pp,
          query_obj.current_tag 
        )
      
      ); // kraj wait for     
      
      // kraj ako nije desktop
    } 
    else {
      
      // kraj ako JE desktop
      window.ramp_is_open = true;
      this_module.enter_pp(query_obj.current_tag, park_data);
    };
    
  };
  this_module.open_with_BLE_or_PI_and_enter_parking = open_with_BLE_or_PI_and_enter_parking;
  
  
  
  function check_parking_and_open(current_tag, park_data) {
    
    
    return new Promise(function (resolve, reject) {

      var query_obj = {
        current_tag: current_tag,
        user_status: get_user_status(park_data),
        user_number: cit_user.user_number,
        indoor: false,
        pp_sifra: park_data.pp_sifra,
      };


      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "POST",
        url: window.remote_we_url + "/request_check_park",
        data: JSON.stringify(query_obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        })
      .done(function(response) {
        
        console.log(response);
        
        response.query_obj = query_obj;
        resolve(response);
      })
      .fail(function(error) {
        error.query_obj = query_obj;
        reject(error);
      });    
      
    }); // kraj promisa  
    
  };
  this_module.check_parking_and_open = check_parking_and_open;
  
  
  function enter_pp(current_tag, park_data) {
    
    var button_selector = '#enter_pp_btn';
    if ( window.openinig_barrier == true ) button_selector = '#open_barrier_btn';
    
    
    // var park_data = this_module.cit_data;
 
    var time_stamp = Date.now();
    
    
    var action_object = { 

      //* postojeci fieldovi  /  
      sifra : null,
      from_time: time_stamp,
      to_time: null,
      paid: null,  
      pp_sifra : String(this_module.cit_data.pp_sifra),
      user_number: cit_user.user_number,
      was_reserved_id: null,
      mobile_from_time: null,

      //* moji fieldovi  /
      
      status: 'parked',
      user_set_book_duration: null,

      reservation_from_time: null,
      reservation_to_time: null,
      reservation_expired: null,
      reservation_canceled: null,
      reservation_paid: null,
      
      
      current_price: this_module.cit_data.pp_current_price,

      pack_type: null,
      duration: null,
      revenue: null,
      owner_revenue: null,
      currency: 'kn',
      
      indoor: false,
      
      current_tag: current_tag,
      
      user_status: get_user_status(park_data)

    }; 

    console.log('prije ajaxa book park place');  
    console.log(this_module.cit_data);
      
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "POST",
      url: window.remote_we_url + "/enter_park",
      data: JSON.stringify(action_object),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      })
    .done(function(response) {
      
      console.log(response);
      
      if ( response.success == true ) {
        
        var park_data = this_module.cit_data;
        
        this_module.current_park_action = response.action;
        
        // umanji avail broj u krugu samo ako je ovo direkt parking , a nije parking nastao iz bookinga
        // I AKO NIJE GUMB ZA BARIJERU
        if ( response.action.reservation_from_time == null && !window.openinig_barrier ) set_count_number(-1);
        
        // ALI AKO PARK NEMA MAIN RAM ONDA UMANJI BROJ I AKO KILKNE NA BARIJERU
        if ( park_data.main_ramp == false && window.openinig_barrier && response.action.reservation_from_time == null ) set_count_number(-1);
        
        set_button_in_progress_state(false, button_selector );
        
        set_parking_state( true, this_module.cit_data, response.action, false);
        
      } 
      else {
        
        // unutar msg properija sam stavio ime funkcije koja poziva popup
        // pošto sam sve popup funkcije stavio u globalni window space onda ih zovam kao window propertije
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( window[response.msg] ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_parkinga();
        };
        set_button_in_progress_state(false, button_selector );
      };
    })
    .fail(function(error) {
      console.log(error);
      popup_greska_prilikom_parkinga();
      set_button_in_progress_state(false, button_selector );
    });    
  
    
    
  };
  this_module.enter_pp = enter_pp; 
  
  
  function open_with_BLE_or_PI_and_EXIT_parking() {
    
    var button_selector = '#exit_pp_btn';
    if ( window.openinig_barrier == true ) button_selector = '#close_barrier_btn';
    
    
    var this_module = window['js/park_page/park_page_module.js']; 
    var park_data = this_module.cit_data;
    
    var last_open_ramp_time_stamp = window.localStorage.getItem('last_open_ramp_time_stamp');
    
    if ( last_open_ramp_time_stamp ) {
      
      last_open_ramp_time_stamp = JSON.parse( last_open_ramp_time_stamp );
    
    
      if ( 
        last_open_ramp_time_stamp.pp_sifra == this_module.cit_data.pp_sifra &&
        Date.now() - last_open_ramp_time_stamp.time < ( 1000*60 )
      ) {
        
        popup_wait_1_min_to_exit();
        return;
      };
    };
    
    
    set_button_in_progress_state(true, button_selector );
    
    if ( !cit_user || !cit_user.user_number ) {
      popup_nedostaje_user();
      set_button_in_progress_state(false, button_selector );
      return;
    };
    
    
    // postavi ovu global varijablu na false i čekaj da se pretvori u true
    // to će se dogoditi u BLE modulu unutar funkcije OPEN barrier
    window.ramp_is_open = false;
    window.error_msg_from_RPI = false;
    window.last_open_ramp_instance_time = Date.now();
    
    window.ble_is_off = false;
    
    window.ramp_open_timeout_sec = 5;
    // mora biti cordova da bi pokrenuo BLE
    if ( window.is_desktop == false ) {
      
      // ali ako je ALGEBRA PARKING TJ AKO JE PP ŠIFRA == 5
      // onda isto preskoči BLE !!!!
      if ( park_data.pp_sifra == '5' ) {
        
        window.ramp_is_open = true;
        this_module.exit_pp(park_data);
        return;
      };
      
      // /////////////////////////// ako je PI RAMPA ///////////////////////////
      if ( 
        park_data.ramps &&
        $.isArray(park_data.ramps) &&
        park_data.ramps.length > 0 
      ) {
        // stavio sam da bude isto kao i za BLE, ali možda zbog problema s internetom treba biti veći timeout
        window.ramp_open_timeout_sec = 10;
        var pi_module = window['js/PI/pi_module.js'];
        pi_module.pi_otvori_rampu(park_data.ramps[0], cit_user.user_number)
        .then(
          function(response) { 
            if ( response.success == true ) {
              
              console.log('rampa se otvorila'); 
              window.ramp_is_open = true;
            } else {

              window.ramp_is_open = false;
              if ( 
                response.msg                                      && 
                typeof window[response.msg] == 'function'         &&
                response.msg !== 'popup_greska_prilikom_parkinga'
              ) {
                
                window[response.msg]();
                
               
                if ( response.msg == 'popup_fixed_open_state' && window.current_park_is_parked ) {
                  window.ramp_is_open = true;
                } else {
                
                  window.error_msg_from_RPI = true;
                  set_button_in_progress_state(false, '#exit_pp_btn');
                  set_button_in_progress_state(false, '#enter_pp_btn');
                  
                };
                
              };
              
            };

          },
          function(fail_obj) { 
            window.ramp_is_open = false;
            console.log(fail_obj);
            
             if ( fail_obj.msg && typeof window[fail_obj.msg] == 'function' ) {
              
              window.error_msg_from_RPI = true;
              set_button_in_progress_state(false, '#exit_pp_btn');
              set_button_in_progress_state(false, '#enter_pp_btn');
              
              window[fail_obj.msg](); 
              
            };
            
            /* popup_greska_prilikom_parkinga();*/ 
          });

      } 
      else {
        // /////////////////////////// ako je BLE RAMPA ///////////////////////////
        var ble_module = window['js/BLE/BLE_module.js'];
        ble_module.BLE_scan_connect_and_open()
        .then( function(is_ble_enabled) {  if ( !is_ble_enabled )  console.error('BLE DID NOT OPEN !!!!!!!!!!!!!!!!!!!!'); });

      };
      
      window.we_ramp_listener_id = wait_for( `window.ramp_is_open == true`,
      function() { 

        var this_module = window['js/park_page/park_page_module.js'];
        var park_data = this_module.cit_data;

        window.ble_is_off = false;
        window.we_ramp_listener_id = null;
        this_module.exit_pp(park_data); 
      },
      1000*window.ramp_open_timeout_sec,
        /* ( recorded_time, exit_pp_function, park_data, enter_pp_function, curr_action_tag ) { */
        wrapper_for_ramp_not_open(
          window.last_open_ramp_instance_time,
          this_module.exit_pp,
          park_data 
        )
      ); // kraj wait for
      
    } 
    else {
      // ako je ovo desktop !!!!!
      window.ramp_is_open = true;
      this_module.exit_pp(park_data);
    };
  }
  this_module.open_with_BLE_or_PI_and_EXIT_parking = open_with_BLE_or_PI_and_EXIT_parking;
  
    
  function exit_pp(park_data) {


  // samo za testiranj GUI-ja
  // set_booking_state( false, this_module.cit_data);
  // return;
    
    
    var button_selector = '#exit_pp_btn';
    if ( window.openinig_barrier == true ) button_selector = '#close_barrier_btn';
    
    set_button_in_progress_state(true, button_selector );
    
    if ( !cit_user || !cit_user.user_number ) {
      popup_nedostaje_user();
      set_button_in_progress_state(false, button_selector );
      return;
    };

    
    // ako je user možda izabrao da bude običan user, iako može biti specijalni user 
    // na primjer može biti tele 2 user ali ako nema mjesta za tele2 onda sam user dao opciju da bude običan user
    // ako je user to izabrao - sada kad izlazi s parkinga brišem da je to izabrao i vraćam ga da bude tele 2 user
    set_obican_user_parking( park_data, false );
    
    
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: ( window.remote_we_url + "/exit_park/" + park_data.pp_sifra + '/' + cit_user.user_number ),
      dataType: "json",
      })
    .done(function(response) {

      console.log(response);

      var park_data = this_module.cit_data;
      
      if ( response.success == true ) {
        
        // I AKO NIJE GUMB ZA BARIJERU
        if ( !window.openinig_barrier ) set_count_number(1);
        
        // ALI AKO PARK NEMA MAIN RAM ONDA UMANJI BROJ I AKO KILKNE NA BARIJERU
        if ( park_data.main_ramp == false && window.openinig_barrier ) set_count_number(1);

        this_module.current_park_action = response.action;
        
        $('#select_book_duration').addClass('cit_show').removeClass('cit_hide');
        set_parking_state( false, park_data, null, null);

        set_button_in_progress_state(false, button_selector );
        
        $('#park_place_info .book_indicator').remove();
        $('#park_place_info .parking_indicator').remove();
        
        // pokaži koliko je love user potrošio na PARKING
        var time_string = get_time_as_hh_mm_ss(response.action.duration);
        
        
        // BITNO !!!!!!!!!!!!!!!!!!!
        // ako se pojavi neka greška prilikom izlaska
        // onda pričekaj da se prvo pojavi popup od te greške
        // zatim sam napravio wait for da čeka sve dok user ne ugasi taj popup greške
        
          
        wait_for(`$('#layer_under_cit_popup_modal').hasClass('cit_show') == false`,
        function() {

          // prikaži popup koliko je user potrošio novaca i koliko je bio na parkingu !!!
          popup_show_paid_time(
            time_string, 
            zaokruziIformatirajIznos( response.action.paid), 
            response.action.currency,
            park_data.pp_sifra,
            response.action
          );


        }, 
        1000*30000, /* čekaj 30 sekundi da user ugasi popup od neke greške */
        function() {

          // nakon 30 sekundi na silu ugasi bilo koji popup ako postoji
          var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
          show_popup_modal(false, '', null, 'ok' );

          // pričekaj 600 ms da se ugasi postojeći popup
          setTimeout( function() {

            // prikaži popup koliko je user potrošio novaca i koliko je bio na parkingu !!!
            popup_show_paid_time(
              time_string, 
              zaokruziIformatirajIznos( response.action.paid), 
              response.action.currency,
              park_data.pp_sifra,
              response.action
            );

          }, 600); // kraj timeout da se zatvori postojeci popup

        }); // kraj wait for
        
      } 
      else {
        // ako postoji popup naslov u response msg onda ga pokreni !!
        if ( window[response.msg] ) {
          window[response.msg]();
        } else {
          // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
          popup_greska_prilikom_EXIT_parkinga();
        };
        
        set_button_in_progress_state(false, button_selector );
      };
    })
    .fail(function(error) {
      console.log(error);
      // ako postoji popup naslov u response msg onda ga pokreni !!
      if ( window[error.msg] ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_EXIT_parkinga();
      };
      set_button_in_progress_state(false, button_selector );
    });
        
    
  
  };
  this_module.exit_pp = exit_pp;
  
  
  function set_booking_duration(book_diff) {
    
    
    return function() {

      var current_book_num =  Number( $('#book_duration_num').text() );
      var new_book_num = current_book_num;
      
      if ( book_diff < 0) {
        
        if ( current_book_num >= 2 ) new_book_num = current_book_num + book_diff;
      } else {
        
        var this_park = this_module.cit_data;
        // postavi neki veliki broj da bude maximalan broj sati za rezervaciju
        var max_reservation_sati = 999;
        // provjeri jel postoji user porducts i jel ima i jednu pretplatu 
        if ( window.all_user_products && window.all_user_products.monthly_arr.length > 0 ) {
          
          $.each(window.all_user_products.monthly_arr, function(pretplata_index, pretplata ) {
            // ako pronađe da ima pretplatu za park u kojem se trenutno nalazi
            if ( Number(pretplata.pp_sifra) == Number(this_park.pp_sifra) ) {
              // onda postavi da maximum sati za booking bude 3
              max_reservation_sati = 3;
            };
          });
        };
        // ako je current broj sati + dodatak manji ili jednak maximumum onda dodaj
        if ( current_book_num + book_diff <= max_reservation_sati ) new_book_num = current_book_num + book_diff;
      };
      
      $('#book_duration_num').text(new_book_num);
      
    };
    
  };
  this_module.set_booking_duration = set_booking_duration;
  
  
  this_module.cit_loaded = true;
 
}
  
  
};