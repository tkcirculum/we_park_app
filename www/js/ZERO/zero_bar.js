var module_object = {
  
  create: function( data, parent_element, placement ) {
    
    var component_id = null;
    
    var component_html =
    `
<div id="zero_component"></div>
    `;
    
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
  },
 
scripts: function () {

  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module
  var this_module = window[module_url]; 

    
  function open_zero(ime_rampe, user_number) {
    
    return new Promise(function(resolve, reject) {

      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      // resolve({ success: false, msg: 'popup_fixed_close_state' });
      // return;
      
      
      // resolve({ success: false, msg: 'popup_fixed_open_state' });
      // return;
      
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      
      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: (window.remote_we_url + '/open_rampa/' + ime_rampe + '/' + parseInt(user_number) ),
        dataType: "json",
        timeout: 1000*12
      })
      .done(function (response) {


        if ( response.success == true ) {

          console.log( `-----------zero OPEN bar je ${ ime_rampe } !!!!!!!` );
          console.log( response )

          window.zero_is_ok = true;
          resolve(response);

        } else {

          resolve(response);
          console.error('SUCCESS FALSE  pri OPEN ZERO !!!!!!!');  
        };

      })
      .fail(function (fail_obj) {

        reject(fail_obj);
        console.error('ajax FAIL pri OPEN ZERO !!!!!!!');

      });
    
    }); // kraj promisa
    
  };
  this_module.open_zero = open_zero;

  
  function close_zero(ime_rampe, user_number) {
    
    return new Promise(function(resolve, reject) {

      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      // resolve({ success: false, msg: 'popup_fixed_close_state' });
      // return;
      
      
      // resolve({ success: false, msg: 'popup_fixed_open_state' });
      // return;
      
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      
      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: (window.remote_we_url + '/close_zero/' + ime_rampe + '/' + parseInt(user_number) ),
        dataType: "json",
        timeout: 1000*12
      })
      .done(function (response) {


        if ( response.success == true ) {

          console.log( `-----------zero CLOSED !!!!!!!` );
          console.log( response )

          window.zero_is_ok = true;
          resolve(response);

        } else {

          resolve(response);
          console.error('SUCCESS FALSE  pri CLOSE ZERO !!!!!!!');
          
        };

      })
      .fail(function (fail_obj) {

        reject(fail_obj);
        console.error('ajax FAIL pri CLOSE ZERO !!!!!!!');

      });
    
    }); // kraj promisa
    
  };
  this_module.close_zero = close_zero;
  
  
  function ZERO_barrier_toggle(get_bar, park_data) {

    var up_or_down = null;
    var old_state = null;
    
    if ( !window.cit_user || !window.cit_user.user_number ) {
      popup_nedostaje_user();
      return;
    };

    if ( Date.now() - window.barrier_process_start < 1000*7 ) {
      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');
      popup_wait_10_sec();
      return;
    };
    
    window.barrier_process_start = Date.now();

    window.zero_is_ok = false;
    
    var curr_pp_sifra = park_data.pp_sifra;
    
    var action_for_zero = null;

    // OVO JE TOGGLE
    // ako je barijera reserved ili null onda je otvori tj spusti
    if ( get_bar.state == null || get_bar.state == 'reserved' ) {
      action_for_zero = 'open_zero';
      up_or_down = 'down';
      old_state = 'up';
    } else if ( get_bar.state == 'parked' ) {
      // ako je parked onda zatvori barijeru tj podigni ju !!!
      action_for_zero = 'close_zero';
      up_or_down = 'up';
      old_state = 'down';
    };


    this_module[action_for_zero](get_bar.name, window.cit_user.user_number)
    .then(
    function(response) { 
      if ( response.success == true )  {

        console.log('rampa se otvorila'); 

      } else {
        window.ramp_is_open = false;
        if ( response.msg && typeof window[response.msg] == 'function' ) {
          window.error_msg_from_RPI = true;

          set_button_in_progress_state(false, '#open_barrier_btn');
          set_button_in_progress_state(false, '#close_barrier_btn');
          window[response.msg]();
        };
      };

    },
    function(fail_obj) { 
      console.log(fail_obj); 
      window.ramp_is_open = false;
      if ( fail_obj.msg && typeof window[fail_obj.msg] == 'function' ) {

        window.error_msg_from_RPI = true;
        set_button_in_progress_state(false, '#open_barrier_btn');
        set_button_in_progress_state(false, '#close_barrier_btn');

        window[fail_obj.msg](); 
      };
      /* popup_greska_prilikom_parkinga(); */ 
    });

    
    window.we_ramp_listener_id = wait_for( `window.zero_is_ok == true`,
    function() { 

      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');


      var curr_pp_sifra = park_data.pp_sifra;
      update_db_barrier_state( curr_pp_sifra, get_bar.name, window.cit_user.user_number, up_or_down )
      .then(
        function(update_bar) { 

          if ( update_bar.success == true ) {

            console.log(update_bar); 

            set_barrier_html( update_bar.name, up_or_down );
            // setTimeout( function() { window.barrier_process_start = 0; }, 5000);
            
            
            var PARK_PAGE = window['js/park_page/park_page_module.js'];
            // ako nema glavnu rampu
            var current_tag = null;
            if ( park_data.main_ramp == false && action_for_zero == 'open_zero') PARK_PAGE.enter_pp(current_tag, park_data);
            if ( park_data.main_ramp == false && action_for_zero == 'close_zero') PARK_PAGE.exit_pp(park_data); 

          } else {

            if ( update_bar.msg && typeof window[update_bar.msg] == 'function' ) {
              window[update_bar.msg]();
            } else {
              popup_barrier_not_open();
            };

            set_barrier_html( update_bar.name, old_state );
            setTimeout( function() { window.barrier_process_start = 0; }, 300);

          };
          
          // kraj success-a za update bar
        },
        function(error) {

          console.log(error);
          popup_barrier_not_open();

          set_barrier_html( get_bar.name, old_state );
          setTimeout( function() { window.barrier_process_start = 0; }, 300);

        // kraj update bar;
        });

    // kraj wait for success  
    },
    1000*10,
    function() { 
      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');
      set_barrier_html( get_bar.name, old_state );
      popup_barrier_not_open();
    }
    ); // kraj wait for

    
    
    
    
  };
  this_module.ZERO_barrier_toggle = ZERO_barrier_toggle;
  
  
  
  
  function check_park_and_ZERO_toggle(get_bar, park_data) {
      
    var PARK_PAGE = window['js/park_page/park_page_module.js'];
    var current_tag = null;
    PARK_PAGE.check_parking_and_open(current_tag, park_data)
    .then(
    function(response) {

      if ( response.success == true ) {
        ZERO_barrier_toggle(get_bar, park_data)
      }
      else {

        if ( response.msg && typeof window[response.msg] == 'function' ) {
          window[response.msg]();
        } else {
          popup_greska_prilikom_parkinga();
        };

        console.error(response.err);

        set_button_in_progress_state(false, '#open_barrier_btn');
        set_button_in_progress_state(false, '#close_barrier_btn');

      }; // ako je success true or false


    },
    function(error) {

        popup_greska_prilikom_parkinga();
        console.error(error);
        set_button_in_progress_state(false, '#open_barrier_btn');
        set_button_in_progress_state(false, '#close_barrier_btn');

    }); // kraj check parking and open

    
  };
  this_module.check_park_and_ZERO_toggle = check_park_and_ZERO_toggle;

  this_module.cit_loaded = true;  

} /* kraj scripts djela  */
  
  
}; // kraj cijelog module objekta
