window.remote_we_url = 'https://wepark.circulum.it:9000';
// window.remote_we_url = 'http://192.168.43.40:8080';
window.do_code_push = true;
window.current_we_app_ver = 'v3.4.7';

//// OVO JE JAKO BITNO  JER NA OSNOVU JEL DESKTOP ILI NIJE POKREĆEM POZIVANJE BLUETOOTH-a
//// OVO JE JAKO BITNO  JER NA OSNOVU JEL DESKTOP ILI NIJE POKREĆEM POZIVANJE BLUETOOTH-a
// kada je desktop true onda prilikom otvaranja rampe ne pozivam BLE func nego samo odmah uđem
window.is_desktop = false;

window.ramp_is_open = false;
window.ble_is_off = false;
window.we_ramp_listener_id = null;

window.cordova_is_ready = false;

window.we_user_lat = null;
window.we_user_lon = null;

window.barrier_process_start = 0;

// ako je code push true to znači da je u produkciji
// i zato console log i error pretvori u prazne funkcije 
// koje će javascript JIT compiler samo preskočiti !!!!!!!
if ( window.do_code_push == true ) {
  window.console = {};
  window.console.log = function(){};
  window.console.error = function(){};
};

console.log( 'cordova is NOT ready' );

var cit_user = null;
window.all_user_products = { balance_sum: 0, monthly_arr: [] }; 

window.all_active_bookings = [];

window.all_shellys = {
  shelly_test: { up: 'E2d733', down: 'E2d706' }
};
