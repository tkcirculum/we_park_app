var module_object = {
create: function( data, parent_element, placement ) {


  var this_module = window[module_url]; 

  var component_id = null;


  var component_html =
`
`;


var this_module = window[module_url]; 

  if ( parent_element ) {
    cit_place_component(parent_element, component_html, placement);
  }; 

  return {
    html: component_html,
    id: component_id
  };




},

scripts: function () {
 
  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module  
  var this_module = window[module_url]; 
  
  
if ( window.is_desktop == false ) {

  wait_for(`window.cordova_is_ready == true`, function() {

    
    console.log(' ------------------ inside BLE MODULE ')
    
    function bytesToString(buffer) {
      return String.fromCharCode.apply(null, new Uint8Array(buffer));
    }

    function hex_to_unit_8_arr(str) {
      
      if (!str) {
        return new Uint8Array();
      };

      var a = [];
      for (var i = 0, len = str.length; i < len; i+=2) {
        a.push(parseInt(str.substr(i,2),16));
      }

      return new Uint8Array(a);
    };
    
    function unit_8_arr_to_hex(buffer) {
      
      var arr_8 = new Uint8Array(buffer);
      var arr_hex = [];
      
      arr_8.forEach(function(byte, arr_8_index) {
        
        arr_hex[arr_8_index] = (byte & 0xFF).toString(16).slice(-2);
        
        if ( arr_hex[arr_8_index].length > 2 ) arr_hex[arr_8_index] = arr_hex[arr_8_index].slice(-2);
        if ( arr_hex[arr_8_index].length < 2 ) arr_hex[arr_8_index] = '0' + arr_hex[arr_8_index];
        arr_hex[arr_8_index] = arr_hex[arr_8_index].toUpperCase();
      });
      
      return arr_hex.join("");  
      
      // return String.fromCharCode.apply(null, new Uint8Array(buffer));
    };
    
    function reverse_hex_string(str) {
      
      var reverse_sting = '';
      for (i = str.length; i >= 2; i-=2) {
        var res = str.substring(i-2, i);
        console.log(res);
        reverse_sting += res;
      };
      
      return reverse_sting;
    };
    
    function BLE_scan_connect_and_open() {
      
      var def = $.Deferred();
     
      var BLE_STATUS = null;
      
      ble.isEnabled(
          function() {
              BLE_STATUS = "enabled";
              console.log("Bluetooth is enabled");
            
              // reset ble id        
              window.localStorage.removeItem( 'BLE_ID' );
            
              
              ble.scan([], 2, function(device) {
                
                  console.log(device);
                 
                  if ( device.name == "we.park" ) {
                    
                    window.localStorage.setItem( 'BLE_ID', device.id );
                    
                    setTimeout(function() {
                      START_ble_connect();
                      
                      def.resolve(true);
                      
                    }, 100);
                    
                  };
                
              }, function(error) {  
                console.log('ERROR ON BLE SCAN !!!');
                console.log(error);
                popup_BLE_is_OFF();
                window.ble_is_off = true;
                def.resolve(false);
                set_button_in_progress_state(false, '#enter_pp_btn');
                if ( window.openinig_barrier == true ) set_button_in_progress_state(false, '#open_barrier_btn');
              });
            
          },
          function() {
            BLE_STATUS = "not_enabled";
            console.log("Bluetooth is NOT enabled");
            popup_BLE_is_OFF();
            window.ble_is_off = true;
            def.resolve(false);
            set_button_in_progress_state(false, '#enter_pp_btn');
            if ( window.openinig_barrier == true ) set_button_in_progress_state(false, '#open_barrier_btn');
          }
      );


      return def;  
      
    }; // end of start scan
    this_module.BLE_scan_connect_and_open = BLE_scan_connect_and_open;
    
    
    
    
    
    
    
    
    function START_ble_connect() {
      
      var auth_response_count = 0;
      
      var CHALLENGE = null;
      var BLE_MAC = null;
      var BLE_SECRET = null;
      var BLE_STATUS = null;
      
      function connect_to_ble(BLE_ID) {
        
        // resetiraj prvo id od prijasnjeg spojenog ble ako postoji
        window.localStorage.removeItem( 'BLE_CONNECTED_ID' );
        
        ble.connect(
          BLE_ID, 
          function(ble_object) { 

            // kada se spojim na ble uspješno onda postavim njegov id u local storage
            window.localStorage.setItem( 'BLE_CONNECTED_ID', BLE_ID );

            console.log("THIS IS BLE CONNECTION INFO >>>>>>>>>>>>>>>>>");
            console.log(ble_object);
            
            read_advertising_data_for_mac_and_secret(ble_object);

            start_ble_listener(BLE_ID);

            request_a_challenge(BLE_ID);

          },
          function(connect_error) { 
            console.log(connect_error);
            // za svaki slučaj removam connected id iz local soragea  ... ako postoji ....
            window.localStorage.removeItem( 'BLE_CONNECTED_ID' );    
          }
        );        
        
      };
    
      function read_advertising_data_for_mac_and_secret(ble_object) {
        
        if ( ble_object.advertising ) {

          var secret_from_mac = {

              "E732433065BC": "F21EC38CBA40076A",
              "C46EBE2CF5D7": "815FBB38D826FCF0",
              "D4C755BC6DAE": "2A12B559ED6D28E6",
              "D119B50653EE": "A8070A8212A8A3C3",
              "D6AE0AF30143": "09E66961DF00A5F0",
              "E252003108FE": "EA261333BD3C8C88",
              "C8891A349D82": "AAAB361E883F9A25",
              "DD45867B4F82": "05AD108B21AE1FB2",
              "CFB7F648C921": "5AB269859250132E",
              "E0C5DE2591E9": "F03E15FE14C65CFE",
              "CB64EC9036AB": "8A42FF2580AD130A",
              "F0D98F858900": "69C7AF4DEB9B3105",
              "DB2A6299CA91": "EEEE83689B974A96",
              "CADD82C56EFF": "44B378A242E3544C",
              "E6C45C3C3155": "08FE0B3D89E494F2",
              "D33692ABD7F4": "E2447B4CBEF6CE56",
            
              /*

              ISPOD JE NOVA TURA KOJE MI JE POSALO KARLO 2020-01-14
              ali u njima ove već imam !!!
                E6C45C3C3155:  08FE0B3D89E494F2
                D119B50653EE: A8070A8212A8A3C3
                D33692ABD7F4: E2447B4CBEF6CE56

              */

              "FA7DFD9DB020": "D93F2B88779B3EBE",
              "DC6FF875C928": "162B28987615BCE9",
              "C97811591353": "33D37EC702BFC50A",
              "FA6614F194FA": "68FA6A9556B8374F",
              "DAA49C5ABDD7": "DAB351A87FE46678",
              "FB58D6F8C3FA": "1C681B9D6B95FB4A",
              "C85AD46F527A": "1896E5352E268D71",
            
            };                      

          
          var adv = null;

          // provjeri jel iOS
          var is_ios = navigator.userAgent.indexOf('iPhone') > -1;
          
          if ( is_ios ) {
            adv = unit_8_arr_to_hex(ble_object.advertising.kCBAdvDataManufacturerData);  
            BLE_MAC = adv.substring(4, 4+12);
          } else {
            adv = unit_8_arr_to_hex(ble_object.advertising); 
            BLE_MAC = adv.substring(14, 14+12);
          };
          
          console.log(" THIS IS ADVERTISING ---> data " + adv );
          console.log( adv );

          console.log(" THIS IS BLE_MAC ---> data " + BLE_MAC );
          console.log( BLE_MAC );


          BLE_SECRET = secret_from_mac[ BLE_MAC ];
          console.log(" THIS IS BLE_SECRET ---> data " + BLE_SECRET );
          console.log( BLE_SECRET );

        };
      };
      
      function start_ble_listener(BLE_ID) {

        ble.startNotification(
        BLE_ID,
        '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
        '6e400003-b5a3-f393-e0a9-e50e24dcca9e', 
          function(data){     
            
            /*
            if ( CHALLENGE == null ) {
              create_HMAC_answer(BLE_ID, data);
            } else {
            */
            
            
              get_auth_response(BLE_ID, data);
            
            // };
            
          },
          function(failure) {
            if ( CHALLENGE == null ) {
              console.log("Failed CHALLENGE NOTIFICATION START on characterstic 6e400003-....");
            } else {
              console.log("Failed AUTH RESPONSE -....");
            };
          }
        ); 
        
      };
      
      function request_a_challenge(BLE_ID) {
        
        var BLE_CHALLENGE_REQUEST = new Uint8Array(1);
        BLE_CHALLENGE_REQUEST[0] = 0;

        ble.write(
          BLE_ID,
          '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
          '6e400002-b5a3-f393-e0a9-e50e24dcca9e', 
          BLE_CHALLENGE_REQUEST.buffer,
            function(data){            
              console.log(" SUCCSESS AFTER WRITE CH REQ 6e400002 data " + unit_8_arr_to_hex(data) );
              console.log( unit_8_arr_to_hex(data) );
            },
            function(failure){
                console.log("Failed 6e400002-....");
            }
          ); 

      }; 
      
      function create_HMAC_answer(BLE_ID, data) {

        console.log(" THIS IS CHALLENGE ---> data " + unit_8_arr_to_hex(data) );
        console.log( unit_8_arr_to_hex(data) );

        // odreži packet type ( koji je za challenge uvijek 01)
        CHALLENGE = unit_8_arr_to_hex(data).substring(2);
        
        console.log(" THIS IS CHALLENGE ODREZAN PACAGE TYPE ---> data " + CHALLENGE);
        console.log( CHALLENGE );

        if ( !BLE_SECRET ) {
          console.error('BLE DEVICE IS NOT IN WE PARK DATABASE !!!!!');
        } else {

          ////////////////////////////////////////////////////////////////////////

          var shaObj = new jsSHA("SHA-1", "HEX");
          shaObj.setHMACKey(BLE_SECRET, "HEX");
          shaObj.update(CHALLENGE, "HEX");
          var HMAC = shaObj.getHMAC("HEX");          
          
          var HMAC = HMAC.substring(0, HMAC.length-2);

          console.log(" THIS IS GENARATED hmac s odrana ZADNJA SVA stringa  ---> data " + HMAC);
          console.log( HMAC );

          // odrezanom hmacu s prednje strane dodaj 02 paket
          HMAC = '02' + HMAC;

          WRITE_HMAC_answer( 
            BLE_ID,
            '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
            '6e400002-b5a3-f393-e0a9-e50e24dcca9e',
            HMAC 
          );  
        };

      };
      
      function WRITE_HMAC_answer( BLE_ID, service_id, char_id, challenge_response_hex ) {
        
        var ch_response_bytes = hex_to_unit_8_arr(challenge_response_hex);
        
        console.log("  HEX CH RES ............................... ");
        console.log(challenge_response_hex);
        
        console.log("  UNIT 8 ARR CH RES ............................... ");
        console.log(ch_response_bytes);
        
        console.log(" .............. WRITING CH RES ............................... ");
        
        ble.write(
        BLE_ID,
        service_id,
        char_id, 
        ch_response_bytes.buffer,
          function(data) {        
            console.log('WRITE CH RESPONSE SUCCESSFUL!')
          },
          function(failure){
              console.log('WRITE CH RESPONSE FAILED !!!!!!!!!!')
              console.log(failure);
          }
        );
        
      };
      
      function OPEN_barrier( BLE_ID, service_id, char_id, open_barrier_command_hex ) {
        
        var open_barrier_command_bytes = hex_to_unit_8_arr(open_barrier_command_hex);
        
        
        console.log(" .............. OOOOOOOPENING BARRIER  ............................... ");
        
        ble.write(
        BLE_ID,
        service_id,
        char_id, 
        open_barrier_command_bytes.buffer,
          function(data) {
            
            // ovaj globalni flag je bitan je na njega postavim wait_for listener i čekam da se pretvori u true !!!!!!
            
            window.ramp_is_open = true;
            if ( window.openinig_barrier == true ) window.barrier_is_open = true;
            console.log('OOOOOOOOOOPEN CALLBACK RAMP / BARRIER SUCCESSFUL!');
          },
          function(failure){
              console.log('OOOOOOOOOOPEN BARRIER FAILED !!!!!!!!!!')
              console.log(failure);
          }
        );
        
      };
      
      function get_auth_response(BLE_ID, data) {
        
        auth_response_count += 1;
        
        
        console.log(" THIS IS AUTH RESPONSE "  + auth_response_count + " ---> data " + unit_8_arr_to_hex(data) );
        console.log( unit_8_arr_to_hex(data) );
        
        if ( data )  {
          
          //
          var ble_response = unit_8_arr_to_hex(data);
          
          // ako je BLE poslao challenge  data type je 02
          if ( ble_response.substring(0, 2) == '01' ) {
            create_HMAC_answer(BLE_ID, data);
          };
          
          // ako je autorizavija ok - tj ako je hash koji sam poslao ok
          // rampa će odgovoriti sa 0300
          if ( ble_response == '0300' ) {
            
            OPEN_barrier( 
              BLE_ID,
              '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
              '6e400002-b5a3-f393-e0a9-e50e24dcca9e',
              '100100' 
            );

            setTimeout( function() { 
              STOP_ble_listener(BLE_ID); 
            }, 5000);

          };
          
          
          // napisi kod kada je odgovor error tj ocinje sa F0
          
          // ako je 0301 to znači da je došlo do neke greške ... PROBATI PONOVO OTVORITI RAMPU
          if ( ble_response == '0301' ) {
            
            popup_app_nije_pronasao_BLE();
          
          };

          
        } else {
          
          console.log( ' NO DATA IN AUTH RESPONSE !!!!!!!' );
          
        };
        
      };

      function STOP_ble_listener(BLE_ID) {
        ble.stopNotification(
        BLE_ID,
        '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
        '6e400003-b5a3-f393-e0a9-e50e24dcca9e', 
          function(data) {            
            console.log("STOPED CHALLENGE NOTIFICATION STOP on characterstic 6e400003-....");
          },
          function(failure){
            console.log("Failed CHALLENGE NOTIFICATION STOP on characterstic 6e400003-....");
          }
        ); 
        
      };

      var BLE_ID = window.localStorage.getItem( 'BLE_ID');
      
      if ( BLE_ID ) {
        connect_to_ble(BLE_ID);
      } else {
        popup_app_nije_pronasao_BLE();
      };

    };

        
    $('#connect_to_BLE').off('click');
    $('#connect_to_BLE').on('click', BLE_scan_connect_and_open );
    

  }, 1000*60*5);
  
};

  
  
  this_module.cit_loaded = true;
 
}
  
  
};




