var module_object = {
  
  create: function( data, parent_element, placement ) {
    
    var component_id = null;
    
    var component_html =
    `
<div id="pi_component"></div>
    `;
    
    if ( parent_element ) {
      cit_place_component(parent_element, component_html, placement);
    }; 
    
    return {
      html: component_html,
      id: component_id
    };
      
  },
 
scripts: function () {

  // VAŽNO !!!!  
  // module_url se definira na početku svakog injetktiranja modula u html
  // to se nalazi u main_script.js unutra funkcije get_module
  var this_module = window[module_url]; 

  
  function pi_otvori_rampu(ime_rampe, user_number) {
    
    return new Promise(function(resolve, reject) {

      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      // resolve({ success: false, msg: 'popup_fixed_close_state' });
      // return;
      
      
      // resolve({ success: false, msg: 'popup_fixed_open_state' });
      // return;
      
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      // za testiranje !!!!!!!!
      
      
      $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: (window.remote_we_url + '/open_rampa/' + ime_rampe + '/' + parseInt(user_number) ),
        dataType: "json",
        timeout: 1000*12
      })
      .done(function (response) {


        if ( response.success == true ) {

          console.log( 'pi rampa se otvorila !!!!!!!' );
          console.log( response )

          window.ramp_is_open = true;
          resolve(response);

        } else {

          window.ramp_is_open = false;
          resolve(response);
          console.error('SUCCESS FALSE  pri raspberry otvaranju rampe !!!!!!!');  
        };

      })
      .fail(function (fail_obj) {

        window.ramp_is_open = null;
        reject(fail_obj);
        console.error('FAIL pri raspberry otvaranju rampe !!!!!!!');

      });
    
    }); // kraj promisa
    
  };
  this_module.pi_otvori_rampu = pi_otvori_rampu;

  this_module.cit_loaded = true;  

} /* kraj scripts djela  */
  
  
}; // kraj cijelog module objekta