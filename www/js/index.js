function init_we_park_app() {
  
/*  
dostupno na deviceu true ili false
  
waze: false
moovit: false
lyft: false
baidu: false
uber: true
cabify: false
maps_me: false
taxis_99: false
gaode: false
citymapper: false
google_maps: true
yandex: false
sygic: false
here_maps: false
ee.mtakso.client: true  
  
  
  
var APP = {
USER_SELECT: "user_select"
APPLE_MAPS: "apple_maps"
GOOGLE_MAPS: "google_maps"
WAZE: "waze"
CITYMAPPER: "citymapper"
NAVIGON: "navigon"
TRANSIT_APP: "transit_app"
YANDEX: "yandex"
UBER: "uber"
TOMTOM: "tomtom"
BING_MAPS: "bing_maps"
SYGIC: "sygic"
HERE_MAPS: "here_maps"
MOOVIT: "moovit"
LYFT: "lyft"
MAPS_ME: "maps_me"
CABIFY: "cabify"
BAIDU: "baidu"
TAXIS_99: "taxis_99"
GAODE: "gaode"
GEO: "geo"
BOLT: "ee.mtakso.client"
  
}


APPS_BY_PLATFORM:
android: Array(17)
0: "user_select"
1: "geo"
2: "google_maps"
3: "citymapper"
4: "uber"
5: "waze"
6: "yandex"
7: "sygic"
8: "here_maps"
9: "moovit"
10: "lyft"
11: "maps_me"
12: "cabify"
13: "baidu"
14: "taxis_99"
15: "gaode"
16: "ee.mtakso.client"
length: 17
__proto__: Array(0)
ios: Array(19)
0: "user_select"
1: "apple_maps"
2: "google_maps"
3: "waze"
4: "citymapper"
5: "navigon"
6: "transit_app"
7: "yandex"
8: "uber"
9: "tomtom"
10: "sygic"
11: "here_maps"
12: "moovit"
13: "lyft"
14: "maps_me"
15: "cabify"
16: "baidu"
17: "taxis_99"
18: "gaode"

  
  
APP_NAMES:
user_select: "[User select]"
apple_maps: "Apple Maps"
google_maps: "Google Maps"
waze: "Waze"
citymapper: "Citymapper"
navigon: "Navigon"
transit_app: "Transit App"
yandex: "Yandex Navigator"
uber: "Uber"
tomtom: "Tomtom"
bing_maps: "Bing Maps"
sygic: "Sygic"
here_maps: "HERE Maps"
moovit: "Moovit"
lyft: "Lyft"
maps_me: "MAPS.ME"
cabify: "Cabify"
baidu: "Baidu Maps"
taxis_99: "99 Taxi"
gaode: "Gaode Maps (Amap)"
geo: "[Geo intent chooser]"
ee.mtakso.client: "Bolt"
  */
  
  
  // alert('ovo je ios test ver 2.0.1');
  
  var isCordovaApp = (window.cordova && window.cordova.platformId !== 'browser');
  
  
  if ( window.we_geo_watch_ID ) {
    // obriši prošli watch
    navigator.geolocation.clearWatch(window.we_geo_watch_ID);
  };

  
  setTimeout( function() {    
    // napravi novi watch !!!!!
    watch_user_position();
  }, 777 );
  
  
  setTimeout( function() { 
    
    wait_for( `window.cit_user !== null && typeof window.cit_user !== 'undefined'`, function() {
      
      if ( !window.car_plate_menu_list_in_progress) {
        window.car_plate_menu_list_in_progress = true;
        generate_car_plate_list('menu');
      };
      
    }, 20*1000);

    if ( window.we_user_lat == null ) get_user_position();
  }, 888);
  
  
  console.log( window.cordova ? window.cordova.platformId : 'NEMA CORDOVE' );
  
  window.we_device_platform = device.platform.toLowerCase();
  if (window.we_device_platform.indexOf('andr') > -1 ) window.we_device_platform = 'ANDROID';
  if (window.we_device_platform.indexOf('ios') > -1 ) window.we_device_platform = 'IOS';
  
  
  window.we_navigator = null;
  
  
  if ( isCordovaApp ) {
    launchnavigator.availableApps(function(results) {

      for (var app in results) {
        if ( window.we_navigator == null && results.google_maps == true ) {
          window.we_navigator = launchnavigator.APP.GOOGLE_MAPS;
        };

        if ( window.we_navigator == null && results.apple_maps == true ) {
          window.we_navigator = launchnavigator.APP.APPLE_MAPS;
        };
      };

    });
  };
  
  window.screen.orientation.lock('portrait');
  
  MobileAccessibility.usePreferredTextZoom(false);
  
  setTimeout( function () {
    var turn_on_ble_gps_tooltip = window.localStorage.getItem('turn_on_ble_gps_tooltip');
    if ( turn_on_ble_gps_tooltip == null ) {
      window.localStorage.setItem('turn_on_ble_gps_tooltip', 'done');
      popup_BLE_is_OFF();
    };
  }, 2000);
  
  
  
  /* 
  
  ---------- OVO JE PORUKA ZA BESPLATNI PARKING COVID 19
  *************** ALI MOGU TO KORISTITI ZA BILO KOJU PORUKU !!!!!
  setTimeout( function () {
    
    var covid_msg = window.localStorage.getItem('covid_msg');
    
    if ( covid_msg == null ) {
      window.localStorage.setItem('covid_msg', 'done');
      popup_covid_19_free_park();
    };
    
  }, 3000);
  
  */
  
  
  clear_all_pp_markers();
  
  window.all_we_map_markers = [];
  
  window.google_maps_loaded = false;
  window.cordova_is_ready = true;
  console.log( 'cordova is ready' );
  
  
  
  // provjeravam jel iphone sa onim jeb... safe zone 
  // ova čudna provjera je zbog buga prilikom bootanja iphona
  // jer inicijalno iphone prijavljuje dimenzije ekrana kao da nema safe zone
  // to su ovi u Apple-u skrpali tako
  
  
  /*
  ----------------------------------------------------------------------
  */
  
  
  
  
  if ( navigator.userAgent.indexOf('iPhone') > -1) {

    var win_width = $(window).width();
    var win_height = $(window).height();

    const is_iPhoneX = (Math.min(win_width, win_height) == 375 && Math.max(win_width, win_height) > 720);
    const is_iPhoneXR = (Math.min(win_width, win_height) == 414 && Math.max(win_width, win_height) > 810);

    if (is_iPhoneX || is_iPhoneXR) {

      $('body').addClass('iPhone_safe_zone');

      if (is_iPhoneX) $('body').attr('id', 'iPhoneX');
      if (is_iPhoneXR) $('body').attr('id', 'iPhoneXR');

    } else {

      $('body').attr('id', 'iPhone');

    };

  };

  
  
  // ------------------------OVO JE ZA BUG KADA SE ZATVORI TIPKOVNICA NA IOS ONDA HTML WINDOW OSTANE VISITI IZNAD --  
  
var move_window_timeout;
function blurEvent(e) {
	move_window_timeout = setTimeout (() => {
		window.scrollTo(0, 0);
	}, 50);
}
		
function focusEvent(e) {
	clearTimeout(move_window_timeout);
};

window.addEventListener('scroll', function(e) {
	let inputfields = document.querySelectorAll('input');
	for (let inp of inputfields) {
		inp.removeEventListener('blur', blurEvent);
		inp.addEventListener('blur', blurEvent);
				
		inp.removeEventListener('focus', focusEvent);
		inp.addEventListener('focus', focusEvent);
	}
});
  
  
  // ----------------------------------------------------------------------
    
  
  function code_push_update() {
    
    
// codePush.sync(syncCallback?, syncOptions?, downloadProgress?, syncErrback?);
    
/*

OVO SU STATUSI koje vraća syncCallback svaki put kad se promjeni stanje !!!! 
-----------------------------------------------------------------------------

UP_TO_DATE: The app is fully up-to-date with the configured deployment.

UPDATE_INSTALLED: An available update has been installed and will be run either immediately after the callback function returns or the next time the app resumes/restarts, depending on the InstallMode specified in SyncOptions.

UPDATE_IGNORED: The app has an optional update, which the end user chose to ignore. (This is only applicable when the updateDialog is used)

ERROR: An error occurred during the sync operation. This might be an error while communicating with the server, downloading or unzipping the update. The console logs should contain more information about what happened. No update has been applied in this case.

IN_PROGRESS: Another sync is already running, so this attempt to sync has been aborted.

CHECKING_FOR_UPDATE: The CodePush server is being queried for an update.

AWAITING_USER_ACTION: An update is available, and a confirmation dialog was shown to the end user. (This is only applicable when the updateDialog is used)

DOWNLOADING_PACKAGE: An available update is being downloaded from the CodePush server.

INSTALLING_UPDATE: An available update was downloaded and is about to be installed.
*/

    function syncStatus(status) {
      
      switch (status) {
        case SyncStatus.UP_TO_DATE:
          // nemoj ništa raditi
          break;
        case SyncStatus.UPDATE_INSTALLED:
          // resetiraj aplikaciju tako da se ažirira aplikacija
          
          show_we_global_loader(true, w.we_trans( 'resetting_app', w.we_lang), null, false);
          
          setTimeout( function() {
            show_we_global_loader(false,  w.we_trans( 'resetting_app', w.we_lang), null, false);
            codePush.restartApplication();
          }, 1500 );
          
          break;
        case SyncStatus.UPDATE_IGNORED:
          show_we_global_loader(false, w.we_trans( 'updating_data', w.we_lang), null, true);
          // nemoj ništa raditi
          break;    
        case SyncStatus.ERROR:
          show_we_global_loader(false, w.we_trans( 'updating_data', w.we_lang), null, true);
          // pokaži useru popup da uasi i upali aplikaciju i provjeri internet konekciju !!!
          // TODO -----> popup_code_push_error();
          break;  
        case SyncStatus.IN_PROGRESS:
          // nemoj ništa raditi
          break;  
        case SyncStatus.CHECKING_FOR_UPDATE:
          // nemoj ništa raditi
          break;    
        case SyncStatus.AWAITING_USER_ACTION:
          show_we_global_loader(false, w.we_trans( 'updating_data', w.we_lang), null, true);
          break;
        case SyncStatus.DOWNLOADING_PACKAGE:
          show_we_global_loader(true, w.we_trans( 'updating_data', w.we_lang), null, true);
          break;
        case SyncStatus.INSTALLING_UPDATE:
          show_we_global_loader(true, w.we_trans( 'saving_new_data', w.we_lang), null, false);
          break;
      }
    }

    function downloadProgress(downloadProgress) {
      
      if (downloadProgress) {
        // Update "downloading" modal with current download %
        var download_percent = (downloadProgress.receivedBytes / downloadProgress.totalBytes) * 100;
        download_percent = Math.ceil(download_percent);
        $('#we_progress_prec').css('width', download_percent + '%');
        //console.log("Downloading " + downloadProgress.receivedBytes + " of " + downloadProgress.totalBytes);
      };
      
    };
    
    // Silently check for the update, but
    // display a custom downloading UI
    // via the SyncStatus and DownloadProgress callbacks
    
    var isCordovaApp = (window.cordova && window.cordova.platformId !== 'browser');
    
    if ( isCordovaApp ) codePush.sync(syncStatus, null, downloadProgress);    

  };
  
  if ( window.do_code_push ) code_push_update();
  

  document.addEventListener("backbutton", onBackKeyDown, false);

  
  function onBackKeyDown() {
      // alert('kiliknuo na back button');
    
      we_history_back();
  };



  function register_pause_online_and_resume() {
      document.addEventListener("online", on_cordova_online, false);
      document.addEventListener("resume", on_cordova_resume, false);
      document.addEventListener("pause", on_cordova_pause, false);
      loadMapsApi();
  };
  
  register_pause_online_and_resume();

  function on_cordova_online () {
    loadMapsApi();
  };
  
  

  function on_cordova_resume () {
    
    console.log('------------------RESUME EVENT ---------------------');
    
    

    

    if ( window.we_geo_watch_ID ) {
      // obriši prošli watch
      navigator.geolocation.clearWatch(window.we_geo_watch_ID);
    };


    setTimeout( function() {    
      // napravi novi watch !!!!!
      watch_user_position();
    }, 555);
    
    
    setTimeout( function() { 

      wait_for( `window.cit_user !== null && typeof window.cit_user !== 'undefined'`, function() {
        
        if ( !window.car_plate_menu_list_in_progress) {
          window.car_plate_menu_list_in_progress = true;
          generate_car_plate_list('menu');
        };
        
      }, 20*1000);

      
      if ( window.we_user_lat == null ) get_user_position();
      
    }, 666);

    
    window.app_is_active = true;
    
    
    // ako je u toku bilo plaćanje na WSPAY webu onda NEMOJ na resume kliknuti na listu parkinga
    // ova win varijabla postane neki url u trenutku kada user ode na WSPAY
    // ili na pay success ili pay error ili pay cancel page 
    if ( !window.we_current_in_app_browser_url ) {
      
      // ali ako ova window varijavla ne postoji ili je null onda klikni na listu tako da to dočeka usera
      $('#park_list_button').click();
      
    } else {
      // ako je ovo resume dok je bilo u toku plaćanje onda nakon dvije sekunde resetiraj varijablu na null
      // tako da stanje bude svježe za sljedeći put !!!!
      setTimeout(function() { window.we_current_in_app_browser_url = null; }, 555);
      
    };
    
    
    setTimeout( function() {
      var account_module = window['js/account/account_module.js'];
      if ( account_module ) account_module.update_account_podatke();
      // alert('ovo je code push update verzija 003');
      
    }, 777);
    
    loadMapsApi();
    
    if ( window.do_code_push ) code_push_update();
    
  };
  
  function on_cordova_pause() {
    
    console.log('------------------ PAUSE EVENT ---------------------');

    // stopiranje geo watch
    if ( window.we_geo_watch_ID ) navigator.geolocation.clearWatch(window.we_geo_watch_ID);

    window.app_is_active = false;
    
    var active_id = null;
    
    if ( $('.big_page.cit_active').length > 0 ) active_id = $('.big_page.cit_active')[0].id;
    
    
    if ( active_id !== null && active_id == 'card_iframe_box' ) {
    
      $('#card_iframe_box')
        .removeClass('cit_active')
        .addClass('cit_hide cit_bottom');

      setTimeout( function() { 
        $('#account_page')
          .addClass('cit_active')
          .removeClass('cit_hide cit_left cit_right cit_top'); 
      }, 50 );  
    };
    
  };
  

  function loadMapsApi() {
    
    
    if ( navigator.connection.type === Connection.NONE ) {
      
      // ne radi ništa
      
    } else {

      if ( !window.google && !window.google_maps_loaded ) {
        
        clearTimeout( window.gmap_timeout_id );
        window.gmap_timeout_id = setTimeout( function() {
          $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyB_AEDsZauvAijKBjQdVB-MOVpHKWroLuM&sensor=true&language=hr&region=HR');
        }, 1500);
      
      };
      
    };
  };

  

  wait_for( `typeof window.google !== "undefined" && window.google !== null && !window.google_maps_loaded`, function() {
    window.google_maps_loaded = true;
    init_google_map();
  }, 500000);
  
  
  function remove_park_place_page() {
    
    if ( $('#park_place_page').hasClass('cit_hide') ) return; 
    
    // prvo sakrij park page stranicu - to je stranica sa headerom i slikom parkirališta ( nema ništa drugo )
    $('#park_place_page')
      .removeClass('cit_active')
      .addClass('cit_hide cit_top'); 


    // get dom element od info diva u kojeg sam prije stavio clonirani red iz liste parkinga
    var park_place_info = $('#park_place_info');
    
 
    // pomakni info traku ispod vidljivog ekrana
    park_place_info.css('transform', `translate(0, 120vh)`);
    
    
    // pričekaj 300 ms da se završi animacija spuštanja
    setTimeout( function() {
      
      // oduzmi klasu za animiranje ( ako nema te klase onda nema css transition  tj u biti animacija je disabled)
      park_place_info.removeClass('cit_animate');
      
      // rotiraj div sa gumbima u 3d prostoru tako za bude okomito na ekran
      // to u biti izgleda kao da je cijeli div nestao !!!!!
      $('#park_place_buttons_box').css({
        'transform': 'rotateX(90deg)'
      });

      
      // VAŽNO !!!!!
      // VAŽNO !!!!!  -----> kada user uđe na neki park preko list stranice 
      // VAŽNO !!!!!  -----> list stranice se još uvijek tretira kao aktivna ( nisam joj skinuo klasu cit_active )
      
      // ako je trenutni page park list
      // ovo moram napraviti jer inače ako user klikne na park list gumb u bottom meniju
      // ta se stranice neće prikazati jer sam stavio uvijet da se animira samo ako je trenutno na nekoj drugoj stranici
      if ( $('.big_page.cit_active')[0].id == 'park_list_page' ) {
        
        // vrati taj page nazad u vidljivi ekran tako da mu dodaš css klasu active i obrišeš ostale css klase
        $('#park_list_page')
            .addClass('cit_active')
            .removeClass('cit_hide cit_left cit_right cit_top'); 
        
      };
      
    }, 300 );
    
  }
  
  
  function register_main_bottom_and_sidenav_menu_events() {

    
    $('#show_directions_panel_btn').off('click');
    $('#show_directions_panel_btn').on('click', function() {
      
      if ( $('#we_directions_panel_wrapper').hasClass('cit_visible') ) {
        
        var map_height = $('body').height() - 50;
        $('#map_page').css('height', map_height + 'px');
        
        setTimeout( function() {
          $('#we_directions_panel_wrapper').removeClass('cit_visible');
        }, 350);
        
      } else {
        
        $('#we_directions_panel_wrapper').addClass('cit_visible');
        
        setTimeout( function() {
          var map_height = $('body').height() - 50;
          var directions_panel_height = $('#we_directions_panel_wrapper').height();
          $('#map_page').css('height', ( map_height - directions_panel_height ) + 'px');
        }, 350);
      }
    });
    
    

    $('#park_list_button').off('click');
    $('#park_list_button').on('click', function() {
      
      
      // po defaultu omogući klikanje na plaćanje karticom
      $('#pay_with_token_btn').css({ 'opacity': '1', 'pointer-events': 'all' });
      $('#open_ws_pay_IAB').css({ 'opacity': '1', 'pointer-events': 'all' });
      
      
      $('#show_barrier_map').removeClass('cit_show');
      
      var map_height = $('body').height() - 50;
      $('#map_page').css('height', map_height + 'px');
        
      setTimeout( function() {
        $('#we_directions_panel_wrapper').removeClass('cit_visible');
      }, 100);
      
      
      window.current_we_page = 'park_list_button';
      console.log(window.current_we_page);
      
      $('#we_directions_panel_wrapper').removeClass('cit_visible');
      $('#we_directions_panel_wrapper').css({
        'opacity': '0',
        'pointer-events': 'none'
      });
      
      
      add_we_history('park_list_button');

      // ovo je varijabla koju poništavam kada user odlazi sa park page na park list
      // služi mi da znam jel user otišao ili još uvijek čeka da se ovori rampa
      
      window.last_open_ramp_instance_time = null;
      
      window.global_park_list = null;
      window.all_user_products = null;

      
      
      // window.we_user_lat = null;
      
      
      var park_list = null;
      
      
      // VAŽNO !!!!1
      // ------------------------------------------------------------------------
      // svaki put kad kliknem na gumb za park list uvijek iznova kreiram listu
      // ------------------------------------------------------------------------
        
      
      get_park_list()
      .then(
      function(all_parks) {
      

        var park_list_module = window['js/park_list/park_list_module.js']
        park_list_module.create( window.global_park_list, $('#park_list_page'));

        setTimeout(function() {

          var park_list_module = window['js/park_list/park_list_module.js'];
          park_list_module.write_distance_in_all_park_rows();

        }, 50);
          
      
      })
      .catch(
      function(error) {
        console.error('ERROR ON CLICK LIST PAGE !!!!');
        console.error(error);
      })
      
      wait_for(`window.global_park_list !== null`, function() {
        
        
        
        remove_park_place_page();
        
        if ( $('.big_page.cit_active')[0].id !== 'park_list_page' ) {

          var active_id = $('.big_page.cit_active')[0].id;

          if ( active_id == 'account_page' || active_id == 'map_page' || active_id == 'card_iframe_box' ) {
            
            $('.big_page.cit_active')
              .not('#card_iframe_box')
              .not('#park_place_page')
              .addClass('cit_hide cit_right')
              .removeClass('cit_active');
            
            $('#card_iframe_box')
              .removeClass('cit_active')
              .addClass('cit_hide cit_bottom');
          };

          setTimeout( function() { 

            $('#park_list_page')
              .addClass('cit_active')
              .removeClass('cit_hide cit_left cit_right cit_top'); 

          }, 100 );

        };
        
      }, 50000);  
      
    });
    
   
    $('#account_button').off('click');
    $('#account_button').on('click', function(e) {
      
      $('#show_barrier_map').removeClass('cit_show');
      
      var login_module = window['js/login/we_login_module.js'];
    
      // login_module.check_user();
    
      
      var map_height = $('body').height() - 50;
      $('#map_page').css('height', map_height + 'px');
        
      setTimeout( function() {
        $('#we_directions_panel_wrapper').removeClass('cit_visible');
      }, 100);
      
      
      
      window.current_we_page = 'account_button';
      console.log(window.current_we_page);
      
      $('#we_directions_panel_wrapper').removeClass('cit_visible');
      $('#we_directions_panel_wrapper').css({
        'opacity': '0',
        'pointer-events': 'none'
      });
      
      
      add_we_history('account_button');
      
      remove_park_place_page();
      
      
      // ovo je varijabla koju poništavam kada user odlazi sa park page na neki drugi page
      // služi mi da znam jel user otišao ili još uvijek čeka da se ovori rampa
      window.last_open_ramp_instance_time = null;
      
      window.all_user_products = null;

      if ( $('.big_page.cit_active')[0].id !== 'account_page' ) {

        
        var active_id = $('.big_page.cit_active')[0].id;
        
        if ( active_id == 'map_page' || active_id == 'card_iframe_box' ) {
          
          $('.big_page.cit_active')
            .not('#card_iframe_box')
            .not('#park_place_page')
            .addClass('cit_hide cit_right')
            .removeClass('cit_active');
          
          $('#card_iframe_box')
            .removeClass('cit_active')
            .addClass('cit_hide cit_bottom');
        };

        if ( active_id == 'park_list_page' || active_id == 'card_iframe_box' ) {
          
          $('.big_page.cit_active')
            .not('#card_iframe_box')
            .not('#park_place_page')
            .addClass('cit_hide cit_left')
            .removeClass('cit_active');
          
          $('#card_iframe_box')
            .removeClass('cit_active')
            .addClass('cit_hide cit_bottom');
        };
        
        

        setTimeout( function() { 

          $('#account_page')
            .addClass('cit_active')
            .removeClass('cit_hide cit_left cit_right cit_top'); 

          
            
          // ako je jako mali ekran onda malo smanji box za plaćanje karticom i voucherom
          // ... također u taj box ide i veliki balance broj
          var win_width = $(window).width();
          var win_height = $(window).height();    

          var balance_box_transform = '';

          if ( win_height > 568 ) {
            balance_box_transform = 'scale(1)';

          } else {
            balance_box_transform = 'scale(0.9)';
          };


          $('#account_balance_box').css({
            'transform': balance_box_transform,
            'transform-origin': '50% 0'
          });


        }, 100 );

      };
      
      
      if ( cit_user && cit_user.user_number ) {
      
        get_user_products_state().then(
          function(response) {
            // svaki put kad kliknem na gumb za park list  uvijek iznova kreiram listu
            var account_module = window['js/account/account_module.js'];
            account_module.create_monthly_list(response);
            account_module.update_user_balance(response);
            account_module.create_card_num_list(response);
            
            setTimeout(function() {account_module.initial_setup_of_customer_credit_card_list();}, 300);
            
            account_module.initial_setup_of_choose_offer_list();
          },
          function(error) { console.log(error)}
        );
        
      };
      
      
    }); 
    
    
        
    $('#map_button').off('click');
    $('#map_button').on('click', function() {
      
      $('#show_barrier_map').removeClass('cit_show');
      
      var map_height = $('body').height() - 50;
      $('#map_page').css('height', map_height + 'px');
        
      setTimeout( function() {
        $('#we_directions_panel_wrapper').removeClass('cit_visible');
      }, 100);
      
      
      window.current_we_page = 'map_button';
      console.log(window.current_we_page);
      
      reset_we_google_map();
      
      // ovo je varijabla koju poništavam kada user odlazi sa park page na neki drugi page
      // služi mi da znam jel user otišao ili još uvijek čeka da se ovori rampa
      window.last_open_ramp_instance_time = null;
      
      
      $('#we_directions_panel_wrapper').removeClass('cit_visible');
      $('#we_directions_panel_wrapper').css({
        'opacity': '0',
        'pointer-events': 'none'
      });
      
      
      
      add_we_history('map_button');
      
      remove_park_place_page();
      
      
      if (  $('.big_page.cit_active').length == 0 || $('.big_page.cit_active')[0].id !== 'map_page' ) {
        
        
        var active_id = null;
        if ( $('.big_page.cit_active').length > 0 ) active_id = $('.big_page.cit_active')[0].id;
        
             
        // ako se user nalazi na listi pakinga ili na acount page
        if ( active_id == null || active_id == 'park_list_page' || active_id == 'account_page' || active_id == 'card_iframe_box' ) {
          
          // onda pomakni taj page udesno
          $('.big_page.cit_active')
            .not('#card_iframe_box')
            .not('#park_place_page')
            .addClass('cit_hide cit_left')
            .removeClass('cit_active');
          
          $('#card_iframe_box')
            .removeClass('cit_active')
            .addClass('cit_hide cit_bottom');
        };
        // prikaži map page
        setTimeout( function() { 
          $('#map_page').addClass('cit_active').removeClass('cit_hide cit_left cit_right'); 
          
        }, 100 );
      };
    });
    
    
    
  }; // kraj func registracije evenata

  wait_for( `$('#map_button').length > 0`, function() {
    
    window.app_is_active = true;
    register_main_bottom_and_sidenav_menu_events();
    
    
    
    // napravio sam tipfeler i umjesto first sam napisao frist :)
    // sad cu obrisati key sa tipfelerom i napisati novi key kako treba
    var we_frist_time_login = window.localStorage.getItem('we_frist_time_login');
    
    if ( we_frist_time_login !== null ) {
      window.localStorage.removeItem('we_frist_time_login');
      window.localStorage.setItem('we_1_time_login', 'true');
    };
    
    var we_1_time_login = window.localStorage.getItem('we_1_time_login');
    
    if ( we_1_time_login == null ) $('#account_button').click();
    
  }, 500000);

  
$(document).ready(function () {
  
  toggle_global_progress_bar(true); 
  
  var loaded_count = 0;
  var loaded_comp_array = [];
    
  var all_cit_comps = [
    'js/park_page_buttons/park_page_buttons_module.js',
    'js/login/we_login_module.js',
    'js/side_menu/side_menu_module.js',
    'js/park_list/park_list_module.js',
    'js/account/account_module.js',
    'js/park_page/park_page_module.js',
    'js/popup_modal/cit_popup_modal.js',
    'js/BLE/BLE_module.js',
    'js/PI/pi_module.js',
    'js/SHELLY/shelly_module.js',
    'js/ZERO/zero_bar.js',
  ];  
    
  // LOAD ALL MODULES FOR GENERAL COMPONENTS
  $.each(all_cit_comps, function (index, module_url) {
    get_cit_module(module_url, 'load_css');  
  });
  
  function all_components_loaded() {
    
    var all_comps_loaded = false;
    
    loaded_count = 0;
    loaded_comp_array = [];
    
    // loop listu za loadanje i provjeri jel svaka od njih loadana
    $.each(all_cit_comps, function(index, url) {
      if ( window[url] && window[url].cit_loaded ) {
        loaded_count += 1;
        loaded_comp_array.push(url);
      };
    });
    
    // dodatni uvijet je da sam dobio response od requesta za listu svih parkinga
    if (
      all_cit_comps.length == loaded_count &&
      window.global_park_list !== null
    ) {
      all_comps_loaded = true;
    };
    return all_comps_loaded ;
  };
  
  
  
  window.global_park_list = null;
  window.all_user_products = null;

  /// OVO JE PRVO INICIJALNO GETANJE PARK LISTE :-)
  get_park_list();
  
  
  wait_for( 
    all_components_loaded, 
    function() {

      toggle_global_progress_bar(false); 

      console.log('ALL CIT COMPONENTS SUCCESSFULLY LOADED HOORAY !!!!'); 

      
      var park_page_buttons_module = window['js/park_page_buttons/park_page_buttons_module.js'];
      
      // važno !!!!! - ubaci na kraju tj append !!!!
      park_page_buttons_module.create('proba', $('#park_place_info'), 'append');  
      
      var side_menu_module = window['js/side_menu/side_menu_module.js']
      side_menu_module.create('proba', $('#we_side_nav'));  
      
      // loadaj module za popup-e SAMO JEDNOM !!!!
      // kasnije za svaki popup promijenim html ovisno o tome što mi treba
      var popup_modal = window['js/popup_modal/cit_popup_modal.js'];
      popup_modal.create(null, $('body'), 'prepend');  

      var account_module = window['js/account/account_module.js']
      account_module.create('proba', $('#account_page'));  
      

      setTimeout( function() {
        var park_list_module = window['js/park_list/park_list_module.js'];
        park_list_module.create(window.global_park_list, $('#park_list_page'));
      }, 150);

      setTimeout( function() {
        var park_page = window['js/park_page/park_page_module.js']
        park_page.create(null, $('#park_place_page'));      
      }, 200);  

    },
    ( 1000 * 60 * 5  ), // timeout is 2 min
    function() { 
      alert('Some modules or components failed to load !!!!');
      toggle_global_progress_bar(false); 
      var missing_modules = [];

      $.each(all_cit_comps, function(index, url) {
        var url_missing = true;
        $.each(loaded_comp_array, function(index, loaded_url) {
          if ( url == loaded_url ) url_missing = false;
        });
        if ( url_missing == false ) missing_modules.push(url);
      });

      console.log(missing_modules);
    }
  );    


}); // END OF $(document).ready ....
  

  /*
  ---------------------------------------------------------------------------------------------------------------  
    
  OVO JE STARI SISTE PUSH PORUKA KOJI JE DEPRICATED ----> prebacujem se na AZURE push notifikacije !!!!  
  OVO JE STARI SISTE PUSH PORUKA KOJI JE DEPRICATED ----> prebacujem se na AZURE push notifikacije !!!!  
  OVO JE STARI SISTE PUSH PORUKA KOJI JE DEPRICATED ----> prebacujem se na AZURE push notifikacije !!!!  
    
  function primi_notifikaciju(pushNotification) {
    
    var message = pushNotification.message;
    var title = pushNotification.title;

    if (message === null || message === undefined) {
      // Android messages received in the background don't include a message. On Android, that fact can be used to
      // check if the message was received in the background or foreground. For iOS the message is always present.
      title = 'Android background';
      message = '<empty>';
    };

    // Custom name/value pairs set in the App Center web portal are in customProperties
    if (pushNotification.customProperties && Object.keys(pushNotification.customProperties).length > 0) {
      message += '\nCustom properties:\n' + JSON.stringify(pushNotification.customProperties);
    };

    console.log(title, message);
  };

  AppCenter.Push.addEventListener('notificationReceived', primi_notifikaciju); 

  ---------------------------------------------------------------------------------------------------------------
  */
  
}; // kraj init we park



window.app_is_active = false;
document.addEventListener('deviceready', function() {
  
  // wait_for(`window.kreni_sa_radom == true`, function() {
    init_we_park_app();
  // }, 5000000000);
  
}, false);
// init_we_park_app();

