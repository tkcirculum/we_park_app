
var w = window;


function show_popup_and_register_ok_button(type, popup_text) {

  
  var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
  show_popup_modal(type, popup_text, null, 'ok');
  
  
};



function check_pp_monthly( user_products, pp_sifra, action_obj ) {
  
  var mjesecna_za_ovaj_pp = false;

  var sve_mjesecne = user_products.monthly_arr;

  // loop kroz sve mjesečne koje ima korisnik i provjeri jel ima mjesečnu za ovaj parking !!!
  // NAPOMENA ovaj array ima samo mjesecne koje su aktive tj koje su u toku !!!!
  if ( sve_mjesecne && sve_mjesecne.length > 0 ) {
    
    /*
    obavezno mora biti tag od producta jednak current tagu userove akcije
    npr ako user ima pretplatu od svoje firme onda će mu u productu pisati da je to TELE2 tag
    ako se dogodi da nema mjesta u TELE2 dijelu parkinga onda user se može prebaciti na OBIČNOG USERA
    u tom slučaju current tag mu neće biti TELE2 već će biti null
    i onda se njegov current tag neće poklapati za tagom od objekta pretplate
    */

    $.each(sve_mjesecne, function( mjesecna_index, mjesecna ) {
       
      if ( Number(mjesecna.pp_sifra) == Number(pp_sifra) ) { 
        
        mjesecna_za_ovaj_pp = true; 
        
        if ( mjesecna.tag ) {
          // ako ima special tag ali taj tag nije isti kao action tag 
          // to znači da je ušao kao neki drugi user na primjer usao je kao obični user ne jedno parkiralište
          // ali ima specijalno mjesto za neko drugo parkiralište !!!!
          if ( mjesecna.tag !== action_obj.current_tag ) { mjesecna_za_ovaj_pp = false; };
        };
        
      };                                                                      
      
    });
    
  };
  
  return mjesecna_za_ovaj_pp;
};




function popup_show_paid_time(time_string, reservation_paid, currency, pp_sifra, action_obj) {
      
  if ( cit_user && cit_user.user_number ) {

    get_user_products_state().then(
      function(response) {

        
var total_cost_html = 
`
<p style="margin: 0;" id="popup_text_total_cost">
${w.we_trans( 'popup_text_total_cost', w.we_lang) }<br>
</p>
<h5 style="margin-top: 0; margin-bottom: 7px; font-size: 30px; font-weight: 300;">${reservation_paid} ${currency}</h5>
`;
        // ako ima mjesečnu onda nemoj prikazivati total cost !!!
        var ima_mjesecnu = check_pp_monthly( response, pp_sifra, action_obj );

        if ( ima_mjesecnu ) total_cost_html = '';

        // ili ako je user VIP
        if ( cit_user.roles.pp_vip && $.isArray(cit_user.roles.pp_vip) && cit_user.roles.pp_vip.length > 0 ) {
          
          if ( $.inArray( pp_sifra, cit_user.roles.pp_vip ) > -1 ) total_cost_html = '';
          
        };
  
var popup_text = 
`
<p style="margin: 0;" id="popup_text_total_time">
${w.we_trans( 'popup_text_total_time', w.we_lang) }
</p>
<h5 style="margin-top: 0; margin-bottom: 7px; font-size: 30px; font-weight: 300;">${time_string}</h5>
${total_cost_html}

`;

        show_popup_and_register_ok_button(true, popup_text);       
      
            
      },
        function(error) { 
          console.error('greška kod dobivanja user producta iz baze prilikom popup-a za time passed i cost');
          console.log(error)
        }
      );

  }; // ako postoji cit user i number             
            
};


function popup_nedostaje_user() {
  
  var popup_text = 
`
${w.we_trans( 'popup_nedostaje_user', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);      
  
};




function popup_sve_pretplate_kupljene(data) {
  
  var popup_text = 
`
${w.we_trans( 'popup_sve_pretplate_kupljene', w.we_lang, data) }
`;

  show_popup_and_register_ok_button('warning', popup_text);      
  
};





function popup_session_expired() {
  
  var popup_text = 
`
${w.we_trans( 'session_expired', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);      
    
  
};


function popup_no_cards() {
  
  var popup_text = 
`
${w.we_trans( 'popup_no_cards', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);     
  
};


function popup_no_voucher_code() {
  
  var popup_text = 
`
${w.we_trans( 'popup_no_voucher_code', w.we_lang) }
`;

  show_popup_and_register_ok_button('pay_warning', popup_text);    
  
};


function popup_closest_open_time(park) {


  park.pp_prices.sort( window.by('pp_price_time_from', false, Number ) );

  var time_stamp = Date.now();


  var next_day = 0;
  var add_days = 0;
  var opens_in_time = null;
  var price_already_found = false;

  // loop za sljedećih 100 dana ... valjda će se parkiralište otvoriti za to vrijeme :)
  for ( next_day = 0; next_day <= 100; next_day++ ) {

      var zero_time_object = create_ms_at_zero_and_week_day(next_day);
      var ms_at_zero = zero_time_object.ms_at_zero;
      var week_day = zero_time_object.week_day;

      if ( price_already_found == false ) {

        // prvo tražim intervale sa specifičnim danom
        // intervali sa specifičnim danom IMAJU PRIORITET !!!!!
        $.each( park.pp_prices, function( pp_price_index, price) {
          if ( 
            time_stamp < (ms_at_zero + price.pp_price_time_from) 
              && 
            week_day == Number(price.pp_price_day)
          ) {
            price_already_found = true;
            add_days = next_day;
            opens_in_time = (ms_at_zero + price.pp_price_time_from) - time_stamp;
          };
        });

        // onda tražim intervale koji nemaju specifični dan

        $.each( park.pp_prices, function( pp_price_index, price) {
          if ( time_stamp < (ms_at_zero + price.pp_price_time_from) ) {
            price_already_found = true;
            add_days = next_day;
            opens_in_time = (ms_at_zero + price.pp_price_time_from) - time_stamp;
          };
        });
        
      };

  }; // kraj loopa za dodavanje novog dana

  var popup_text = 
`
<h5 id="closest_open_time_title">${w.we_trans( 'closest_open_time_title', w.we_lang) }</h5>
<p>
<span id="closest_open_time_text">${w.we_trans( 'closest_open_time_text', w.we_lang) }</span><br>
${ add_days > 0 ? ( add_days + 'd & ') : ''}
${ opens_in_time !== null ? get_time_as_hh_mm_ss(opens_in_time) : '???' }
<br>
</p>
`;

  show_popup_and_register_ok_button('warning', popup_text);   

};


function popup_become_regular_user(park, tag, click_button_id) {

  wait_for( `$('#layer_under_cit_popup_modal').hasClass('cit_show') == false`, function(){

    var trans_data = {
      park: park,
      tag: tag,
      click_button_id: click_button_id
    }

    // pazi ovo je funkcija koja returna html !!!!!!!!
    var popup_text = 
  `
  ${ w.we_trans( 'popup_become_regular_user', w.we_lang, trans_data) }
  `;

    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

    show_popup_modal(true, popup_text, null, 'yes/no');


    $('#cit_popup_modal_OK').off('click');


    $('#cit_popup_modal_YES').off('click');
    $('#cit_popup_modal_YES').on('click', function() {

      show_popup_modal(false, popup_text, null, 'yes/no');

      set_obican_user_parking( park, true );


      // ------------- OVO JE KRUG SA COUNTEROM NA LISTI --------------------
      // obriši ako je do sada bila druga boja u slučaju nula slobodnih mjesta
      $(`#park_list_row_${park._id} .park_available_count`).css('background-color', '');
      // ubaci u krug sa brojem slobodnih mjesta broj OBIČNIH MJESTA
      $(`#park_list_row_${park._id} .park_available_count`).html(park.pp_available_out_num);


      // ------------- OVO JE KRUG SA COUNTEROM NA PARK PAGE --------------------
      // obriši ako je do sada bila druga boja u slučaju nula slobodnih mjesta
      $(`#park_place_row_data .park_available_count`).css('background-color', '');
      // ubaci u krug sa brojem slobodnih mjesta broj OBIČNIH MJESTA
      $(`#park_place_row_data .park_available_count`).html(park.pp_available_out_num);

      // ako je ovo poziv u trenutku parkinga ili u trenutku bookinga onda samo klikni ponovo na taj gumb
      if ( click_button_id ) $(click_button_id).click();

    }); 

    $('#cit_popup_modal_NO').off('click');
    $('#cit_popup_modal_NO').on('click', function() {
      show_popup_modal(false, popup_text, null, 'yes/no');
      set_obican_user_parking( park, false );
    });
    
  
  }, 5000*1000);
  
  
}; // kraj zelis li postati obican user


function popup_nemate_lovu_ili_pretplatu() {
  
  var popup_text = 
`
${w.we_trans( 'popup_nemate_lovu_ili_pretplatu', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);   
  
  
};


function popup_amount_nije_ispravan() {
  var popup_text = 
`
${w.we_trans( 'popup_amount_nije_ispravan', w.we_lang) }
`;

  show_popup_and_register_ok_button('pay_warning', popup_text);   
  
  
};

    
function popup_greska_prilikom_bookinga() {
      
  var popup_text = 
`
${w.we_trans( 'popup_greska_prilikom_bookinga', w.we_lang) }
`;  

  show_popup_and_register_ok_button('error', popup_text);   
};

function popup_error_jel_nova_rega() {
      
  var popup_text = 
`
${w.we_trans( 'popup_error_jel_nova_rega', w.we_lang) }
`;  

  show_popup_and_register_ok_button('error', popup_text);   
};


function popup_vec_postoji_reg() {
      
  var popup_text = 
`
${w.we_trans( 'popup_vec_postoji_reg', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);   
};


function popup_obavezna_barem_jedna_rega() {
  
  var popup_text = 
`
${w.we_trans( 'obavezna_barem_jedna_rega', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
  
  
};



function popup_dosadno_1_rega() {
  
  var popup_text = 
`
${w.we_trans( 'popup_dosadno_1_rega', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
  
  
};




    
function popup_greska_prilikom_parkinga() {
      
  var popup_text = 
`
${w.we_trans( 'popup_greska_prilikom_parkinga', w.we_lang) }
`; 

  show_popup_and_register_ok_button('error', popup_text);   
      
};

    
function popup_ramp_not_open(park_data) {
      
  var popup_text = 
`
${w.we_trans( 'popup_ramp_not_open', w.we_lang ) }
`;  

  
  show_popup_and_register_ok_button('error', popup_text);
  
  // ako sam ovom popapu dao argument park data
  // to znači da je nastao problem prilikom izlaska iz parkinga !!!!!
  setTimeout(function() {
      
  var park_list_module = window['js/park_list/park_list_module.js'];
  if ( park_data ) park_list_module.check_book_i_park_i_open_park_place(park_data);
    
  }, 600);
      
};



function popup_ramp_in_process() {
      
  var popup_text = 
`
${w.we_trans( 'ramp_in_process', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
      
};


function popup_infra_red() {
      
  var popup_text = 
`
${w.we_trans( 'popup_infra_red', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
      
};





function popup_nema_vise_mjesta() {
      
  var popup_text = 
`
${w.we_trans( 'popup_nema_vise_mjesta', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
      
};



function popup_sve_barijere_zauzete() {
      
  var popup_text = 
`
${w.we_trans( 'popup_sve_barijere_zauzete', w.we_lang) }
`;  

  show_popup_and_register_ok_button('warning', popup_text);     
      
};




function popup_wait_1_min_to_exit() {
      
  var popup_text = 
`
${w.we_trans( 'popup_wait_1_min_to_exit', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);   
      
};


function popup_wait_10_sec() {
      
  var popup_text = 
`
${w.we_trans( 'popup_wait_10_sec', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);   
      
};




function postoji_vise_bookinga() {
      
  var popup_text = 
`
${w.we_trans( 'postoji_vise_bookinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);     
      
};


function vec_postoji_booking() {
      
  var popup_text = 
`
${w.we_trans( 'vec_postoji_booking', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);     
      
};


function nema_bookinga() {
      
  var popup_text = 
`
${w.we_trans( 'nema_bookinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);        
           
      
};


function nema_parkinga() {
      
  var popup_text = 
`
${w.we_trans( 'nema_parkinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);         
           
};


function popup_postoji_vise_parkinga() {
      
  var popup_text = 
`
${w.we_trans( 'popup_postoji_vise_parkinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);             
      
};


function popup_napon_too_low() {
      
  var popup_text = 
`
${w.we_trans( 'popup_napon_too_low', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);             
      
};



function popup_fixed_close_state() {
      
  var popup_text = 
`
${w.we_trans( 'popup_fixed_close_state', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);             
      
};


function popup_fixed_open_state() {
      
  var popup_text = 
`
${w.we_trans( 'popup_fixed_open_state', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);             
      
};




function popup_update_products_error() {
      
  var popup_text = 
`
${w.we_trans( 'popup_update_products_error', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);          
      
};


function popup_greska_prilikom_CANCEL_bookinga() {

  var popup_text = 
`
${w.we_trans( 'popup_greska_prilikom_CANCEL_bookinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);        
      
};


function popup_greska_prilikom_EXIT_parkinga() {
      
  var popup_text = 
`
${w.we_trans( 'popup_greska_prilikom_EXIT_parkinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);            
      
};


function popup_app_nije_pronasao_BLE() {
      
  var popup_text = 
`
${w.we_trans( 'popup_greska_prilikom_CANCEL_bookinga', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);      
      
};


function popup_BLE_is_OFF() {
      
var popup_text = 
`
${w.we_trans( 'popup_BLE_is_OFF', w.we_lang) }
`;

show_popup_and_register_ok_button(true, popup_text);        
      
};



function popup_GPS_is_OFF() {
      
var popup_text = 
`
${w.we_trans( 'popup_GPS_is_OFF', w.we_lang) }
`;

show_popup_and_register_ok_button(true, popup_text);        
      
};


function popup_predaleko_od_rampe() {
      
var popup_text = 
`
${w.we_trans( 'popup_predaleko_od_rampe', w.we_lang) }
`;

show_popup_and_register_ok_button('warning', popup_text);        
      
};





function popup_nemate_google_ili_apple_maps( park ) {
      
var popup_text = 
`
${w.we_trans( 'popup_nemate_google_ili_apple_maps', w.we_lang) }
`;

var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

show_popup_modal('warning', popup_text, null, 'ok', 'skip_registering_ok_btn' );

$('#cit_popup_modal_OK').off('click');
$('#cit_popup_modal_OK').on('click', function() {

  show_popup_modal(false, popup_text, null, 'ok');

  var park_list_module = window['js/park_list/park_list_module.js']
  park_list_module.open_regular_google_maps(park);


});      
        
      
};


function popup_ERROR_on_BLE_scan() {
      
var popup_text = 
`
${w.we_trans( 'popup_ERROR_on_BLE_scan', w.we_lang) }
`;

show_popup_and_register_ok_button('error', popup_text);        
   
  
};



function popup_no_amount_for_card_pay() {
  
var popup_text = 
`
${w.we_trans( 'popup_no_amount_for_card_pay', w.we_lang) }
`;

show_popup_and_register_ok_button('warning', popup_text);        
    
  
  
};


function popup_pay_with_token_success() {
  
 
  var popup_text = 
`
${w.we_trans( 'popup_pay_with_token_success', w.we_lang) }
`;
  
  show_popup_and_register_ok_button('pay_success', popup_text); 
  
};
  

function popup_pay_with_token_error() {


var popup_text = 
`
${w.we_trans( 'popup_pay_with_token_error', w.we_lang) }
`;

  show_popup_and_register_ok_button('pay_error', popup_text); 


};



function popup_pay_with_voucher_success(msg) {
  
 
  var popup_text = 
`
${w.we_trans( 'popup_pay_with_voucher_success', w.we_lang, msg)}
`;

show_popup_and_register_ok_button('pay_success', popup_text); 
  
};
  


function popup_pay_with_voucher_error(popup_msg) {


var popup_text = 
`
${w.we_trans( 'popup_pay_with_voucher_error', w.we_lang, popup_msg)}
`;

show_popup_and_register_ok_button('pay_error', popup_text); 



};



function popup_sva_polje_za_reg_pass_min_6() {


var popup_text = 
`
${w.we_trans( 'popup_sva_polje_za_reg_pass_min_6', w.we_lang) }
`;

show_popup_and_register_ok_button('pay_error', popup_text); 


};



function popup_email_nije_dobar() {


var popup_text = 
`
${w.we_trans( 'popup_email_nije_dobar', w.we_lang) }
`;

show_popup_and_register_ok_button('pay_error', popup_text);  


};



function popup_lozinke_nisu_iste() {

var popup_text = 
`
${w.we_trans( 'popup_lozinke_nisu_iste', w.we_lang) }
`;

show_popup_and_register_ok_button('pay_error', popup_text);  

};



function popup_error_reset_pass() {

var popup_text = 
`
${w.we_trans( 'popup_error_reset_pass', w.we_lang) }
`;

show_popup_and_register_ok_button('pay_error', popup_text); 


};




function popup_enter_pass_for_token() {
  
 
  var popup_text = 
`
${w.we_trans( 'popup_enter_pass_for_token', w.we_lang) }
`;

  var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

  show_popup_modal('pay_warning', popup_text, null, 'ok', 'skip_registering_ok_btn');
  
  $('#cit_popup_modal_OK').off('click');
  $('#cit_popup_modal_OK').on('click', function() {
    
    var pass = $('#pass_for_token').val();
    
    show_popup_modal(false, popup_text, null, 'ok');
    
    if ( pass == '' ) return;
    
    var pass_data = {
      pass: pass,
      user: window.cit_user.name
    };

    $('#pass_for_token').val('');
    
    $.ajax({
      type: 'POST',
      url: window.remote_we_url + '/pass_for_token',
      data: JSON.stringify(pass_data),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function(response) {

      if ( response.success == true ) {
        
        var is_token_payment = true;
        popup_ask_for_r1(is_token_payment);
        
        
      } else {
        
        popup_pass_for_token_error(response.msg);
      };

    })
    .fail(function(error) {

      popup_pass_for_token_error(error.msg);
      console.log(error);
    });    
    
      
    
  });
  
};
  


function popup_pass_for_token_error(msg) {


var popup_text = 
`
${w.we_trans( 'popup_pass_for_token_error', w.we_lang, msg)}
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  


};


function popup_no_user_by_email() {



var popup_text = 
`
${w.we_trans( 'popup_no_user_by_email', w.we_lang) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  



};


function popup_reset_pass_check_email() {
  
var popup_text = 
`
${w.we_trans( 'popup_reset_pass_check_email', w.we_lang) }
`;
  
show_popup_and_register_ok_button('pay_success', popup_text);  
 
  
};
  

function popup_discount_code_error() {

var popup_text = 
`
${w.we_trans( 'popup_discount_code_error', w.we_lang) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  



};

function popup_discount_code_not_found() {

var popup_text = 
`
${w.we_trans( 'popup_discount_code_not_found', w.we_lang) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  

};


function popup_discount_used() {


var popup_text = 
`
${w.we_trans( 'popup_discount_used', w.we_lang) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  


};


function popup_discount_code_success(discount) {
  
 
var popup_text = 
`
${w.we_trans( 'popup_discount_code_success', w.we_lang, discount)}
`;
  
show_popup_and_register_ok_button('pay_success', popup_text);  
  
  
};
  

function popup_discount_nedovoljan_iznos(iznos_uplate, prava_cijena) {

  
var trans_data = {
  iznos_uplate: iznos_uplate,
  prava_cijena: prava_cijena
}; 
  
var popup_text = 
`
${w.we_trans( 'popup_discount_nedovoljan_iznos', w.we_lang, trans_data ) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  

};


function popup_business_podaci_error() {
  
  
  console.log(arguments);
  
  
var popup_text = 
`
${w.we_trans( 'popup_business_podaci_error', w.we_lang ) }
`;
  
show_popup_and_register_ok_button('pay_error', popup_text);  
  
  
};



function update_user_business_data(business_data, is_token_payment) {
  
  var account_module = window['js/account/account_module.js'];
  
  // napravi update usera samo ako je r1 property == true
  if ( business_data.r1 == true ) {
    
    $.ajax({
      type: 'POST',
      url: window.remote_we_url + '/r1_racun_user_podaci',
      data: JSON.stringify(business_data),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
    .done(function(response) {

      if ( response.success == true ) {
        
        if ( is_token_payment ) {
          
          // sad više ne koristim čisti ajax za token plaćanje već isto formu
          // po novom pravilniku ------> izmjenjno 10.11.2020.
          // account_module.pay_with_token();
          account_module.before_in_app_browser_check_discount(is_token_payment);
        
        } else {
          
          account_module.before_in_app_browser_check_discount(is_token_payment);
        };
        
      } else {
        // pozovi popup prozor koji sam dobio kao response
        window[response.msg]();
        
      };

    })
    .fail(function(error) {

      window[error.msg]();
      console.log(error);
    });    
  
  } 
  else {
    
    // ako NE ŽELI R1 NE MORAM NAPRAVITI UPDATE NA USERU ZA NJEGOV BUSINESS DATA
    if ( is_token_payment ) {
      // account_module.pay_with_token();
      account_module.before_in_app_browser_check_discount(is_token_payment);
    } else {
      account_module.before_in_app_browser_check_discount(is_token_payment);
    };

  };
  
  
}; // kraj update user business data




function popup_ask_for_r1(is_token_payment) {
  
  wait_for( `$('#layer_under_cit_popup_modal').hasClass('cit_show') == false`, function() {
  
  
    if (!cit_user) {
      popup_nedostaje_user();
      return;
    };
  

var popup_text = 
`
${w.we_trans( 'ask_for_r1', w.we_lang, cit_user) }
`;


    wait_for( `$('#business_mjesto_input').length > 0`, function() {

      if ( cit_user.oib ) $('#business_oib_input').val(cit_user.oib);
      if ( cit_user.business_name ) $('#business_name_input').val(cit_user.business_name);


      if ( cit_user.business_address ) {

        var adresa = cit_user.business_address.split(',')[0] ? cit_user.business_address.split(',')[0] : '';
        var pp_i_mjesto = cit_user.business_address.split(',')[1] ? cit_user.business_address.split(',')[1].trim() : '';

        $('#business_address_input').val(adresa);
        $('#business_mjesto_input').val(pp_i_mjesto);

      };

      // dodaj custom boju i style ....

      $('#cit_popup_modal_message').css({
        'background-color': '#033966',
        'padding': '10px',
        'color': '#fff',
        'border-radius': '10px'
      });


    }, 50000 );


    
    
    $('#cit_popup_modal_message').html('');

    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

    show_popup_modal(true, popup_text, null, 'yes/no');


    $('#cit_popup_modal_OK').off('click');


    $('#cit_popup_modal_YES').off('click');
    $('#cit_popup_modal_YES').on('click', function() {


      var oib = $('#business_oib_input').val();
      if ( !$.isNumeric(oib) || oib.length !== 11 ) {
        alert( 'Pogrešan OIB!\n\nMora biti brojčana vrijednost sa 11 znamenki!' );
        return;
      };

      var naziv_firme = $('#business_name_input').val();
      if ( naziv_firme == '' ) {
        alert( 'Obavezno ime tvrtke!' );
        return;
      };


      var adresa_firme = $('#business_address_input').val();
      if ( adresa_firme == '' ) {
        alert( 'Obavezna adresa tvrtke!' );
        return;
      };

      var pp_i_mjesto_firme = $('#business_mjesto_input').val();
      if ( pp_i_mjesto_firme == '' ) {
        alert( 'Obavezan je poštanski broj i mjesto tvrtke!' );
        return;
      };    


      show_popup_modal(false, popup_text, null, 'yes/no');

      $('#cit_popup_modal_message').removeAttr('style');

      var business_data = {
        user_num: cit_user.user_number,
        naziv_firme: naziv_firme,
        oib: oib,
        adresa_firme: adresa_firme,
        pp_i_mjesto_firme: pp_i_mjesto_firme,
        
        r1: true
      };

      update_user_business_data(business_data, is_token_payment);

    }); 

    $('#cit_popup_modal_NO').off('click');
    $('#cit_popup_modal_NO').on('click', function() {


      show_popup_modal(false, popup_text, null, 'yes/no');

      $('#cit_popup_modal_message').removeAttr('style');

      var business_data = {
        user_num: cit_user.user_number,

        naziv_firme: null,
        oib: null,
        adresa_firme: null,
        pp_i_mjesto_firme: null,


        r1: false
      };


      update_user_business_data(business_data, is_token_payment);

    });
  
  }, 5000*1000); // kraj wait for 

  
}; // kraj zelis li postati obican user


function popup_ime_usera_zauzeto() {
  
  var popup_text = 
`
${w.we_trans( 'ime_usera_zauzeto', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);   
  
};



function popup_email_usera_zauzet() {
  
  var popup_text = 
`
${w.we_trans( 'email_usera_zauzet', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);   
  
};


function popup_registration_error(msg) {
      
  var popup_text = 
`
${w.we_trans( 'popup_registration_error', w.we_lang, (msg || '') ) }
`;  

  show_popup_and_register_ok_button('error', popup_text);   
};




function popup_welcome_new_user() {
      
var popup_text = 
`
${w.we_trans( 'welcome_new_user', w.we_lang ) }
`;  

show_popup_and_register_ok_button(true, popup_text);
  
};



function popup_covid_19_free_park() {

var popup_text = 
`
${w.we_trans( 'covid_19_free_park', w.we_lang ) }
`;  

show_popup_and_register_ok_button(true, popup_text);

  
};


function popup_pass_pp_iako_ramp_not_open(exit_pp_func, park_data, enter_pp_func, current_action_tag ) {
  
  
  wait_for( `$('#layer_under_cit_popup_modal').hasClass('cit_show') == false`, function(){
  
var popup_text = 
`
${w.we_trans( 'pass_pp_iako_ramp_not_open', w.we_lang ) }
`;  

    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

    show_popup_modal(true, popup_text, null, 'yes/no');


        
    $('#cit_popup_modal_message').css({
      'background-color': '#e8412f',
      'padding': '10px',
      'color': '#fff',
      'border-radius': '10px'
    });


    $('#cit_popup_modal_OK').off('click');


    $('#cit_popup_modal_YES').off('click');
    $('#cit_popup_modal_YES').on('click', function() {



      $('#cit_popup_modal_message').removeAttr('style');

      show_popup_modal(false, popup_text, null, 'yes/no');

      if ( exit_pp_func ) exit_pp_func(park_data);

      if ( enter_pp_func ) enter_pp_func(current_action_tag, park_data);

    }); 

    $('#cit_popup_modal_NO').off('click');
    $('#cit_popup_modal_NO').on('click', function() {

      show_popup_modal(false, popup_text, null, 'yes/no');

      
      setTimeout( function() {
        
        var park_list_module = window['js/park_list/park_list_module.js'];
        if ( park_data ) park_list_module.check_book_i_park_i_open_park_place(park_data);
        
        // popup_ramp_not_open(park_data);
      }, 600);
      


    });

  }, 5000*1000); // kraj wait for 
    
};


function popup_barrier_not_open() {
      
  var popup_text = 
`
${w.we_trans( 'barrier_not_open', w.we_lang) }
`;

  show_popup_and_register_ok_button('error', popup_text);         
           
};




function popup_ramp_is_broken() {
      
  var popup_text = 
`
${w.we_trans( 'popup_ramp_is_broken', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);         
           
};


function popup_ramp_is_maint() {
      
  var popup_text = 
`
${w.we_trans( 'popup_ramp_is_maint', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);         
           
};




function popup_ramp_is_broken_booking() {
      
  var popup_text = 
`
${w.we_trans( 'popup_ramp_is_broken_booking', w.we_lang) }
`;

  show_popup_and_register_ok_button('warning', popup_text);         

};



function popup_potvrda_bookinga(book_price, booking_func) {
  
  
  wait_for( `$('#layer_under_cit_popup_modal').hasClass('cit_show') == false`, function(){
  
  
var popup_text = 
`
${w.we_trans( 'potvrda_bookinga', w.we_lang, book_price ) }
`;  

    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;

    show_popup_modal(true, popup_text, null, 'yes/no');



      $('#cit_popup_modal_message').css({
        'background-color': '#e8412f',
        'padding': '10px',
        'color': '#fff',
        'border-radius': '10px'
      });


    $('#cit_popup_modal_OK').off('click');


    $('#cit_popup_modal_YES').off('click');
    $('#cit_popup_modal_YES').on('click', function() {



      $('#cit_popup_modal_message').removeAttr('style');

      show_popup_modal(false, popup_text, null, 'yes/no');

      setTimeout(function() {
        booking_func();
      }, 300);

    }); 

    $('#cit_popup_modal_NO').off('click');
    $('#cit_popup_modal_NO').on('click', function() {

      show_popup_modal(false, popup_text, null, 'yes/no');


    });
  
  }, 5000*1000); // kraj wait for
  
};



