
function show_we_global_loader(state, text, stop_function, download_progress) {
  
  if ( state ) {
    $('#we_global_loader_overlay').addClass('cit_show');
  } else {
    $('#we_global_loader_overlay').removeClass('cit_show');
    return;
  };
  
  if ( text ) {
    $('#loader_we_global_text').text(text);
  } else {
    $('#loader_we_global_text').text('...SAMO TRENUTAK...');
  };
  
  $('#stop_we_global_loader_btn').off('click');
  
  if ( stop_function ) {
    $('#stop_we_global_loader_btn').css('display', 'flex');
    $('#stop_we_global_loader_btn').on('click', stop_function);
    
  } else {
    $('#stop_we_global_loader_btn').css('display', 'none');
  };
  
  
  if ( download_progress ) {
    $('#we_global_loader_progress').css('display', 'block');
  } else {
    $('#we_global_loader_progress').css('display', 'none');
  };  
  
  
};

function reset_we_google_map() {
  
  window.we_directionsRenderer.setMap(null);

  setTimeout( function() {
    
    show_pp_markers();
    window.we_map.setZoom(12);
    var pt = new google.maps.LatLng(45.79500308999068, 15.973694855983695);
    window.we_map.setCenter(pt);
    
  }, 300);  
  
};


window.get_all_parks_timestamp = 0;
window.get_all_parks_cached = null;

function get_park_list() {
  
  return new Promise( function( resolve, reject ) { 

      // ako sam već napravio ovaj request u manje od 1 sec !!!
    if ( Date.now() - window.get_all_parks_timestamp < 1*1000 ) {
      // uzmi iz cache-a
      window.global_park_list = window.get_all_parks_cached;
      console.log(' -------------------- uzimam cache od global_park_list -------------------- ')

      resolve(window.global_park_list);
      return; 
    };


    $.ajax({
        type: 'GET',
        url: window.remote_we_url + '/get_all_park_places',
        dataType: "json"
    })
    .done(function(response) {

      if ( response.success == true ) {

        window.global_park_list = response.park_places;

        window.get_all_parks_cached = window.global_park_list;
        window.get_all_parks_timestamp = Date.now();

        console.log(window.global_park_list);

        resolve(response.park_places);

      } else {

        console.error('NO PARK LIST ---- array is empty !!!!!');
        reject('NO PARK LIST ---- array is empty !!!!!');

      };

    })
    .fail(function(error) {
      
      console.error("Error on get park list !!!");
      console.error(error);
      reject('Error on get park list !!!');
    });

    
  }); // kraj promisa
  
};

window.all_user_products = null;

function get_user_products_state() {
  
  return new Promise( function( resolve, reject ) { 
  
  if ( !cit_user || !cit_user.user_number ) {
    resolve(null);
    return;
  };
    
  
  var zajednicki_user_num = cit_user.user_number;
  
  if ( 
    cit_user.user_number == 182 ||
    cit_user.user_number == 184 ||
    cit_user.user_number == 185 ||
    cit_user.user_number == 410
    ) {
    
    zajednicki_user_num = 333;
  };
  
  
    $.ajax({
      headers: {
          'X-Auth-Token': cit_user.token
      },
      type: "GET",
      url: window.remote_we_url + '/get_user_products_state/' + zajednicki_user_num,
      dataType: "json"
    })
    .done(function (response) {

      if ( response.success == true ) {
        
        console.log( ' ovo je moj product objekt ' );
        console.log( response )
        
        window.all_user_products = response;
        resolve(response);
        
      } else {
        
        def.resolve(null);
        console.error('greška pri preuzimanju producta customera');  
      };

    })
    .fail(function (err) {
      resolve(err);
      console.error('greška pri preuzimanju producta customera');
    });

}); // kraj promisa
  

  
};



// za funkciju window.by (..ili samo by)  je ovo BITNO:
// drugi argument je true što znači reverse tj silazno sortiranje
// sljedeći window.by ima drugi argument false  ŠTO OPET ZNAČI SILAZNO (dakle mora biti obrnuto od prvog sorta ) !!!!
// jer iz nekog razloga autor funkcije je napravio tako 
// ----> vjerojatno je BUG ili reverse doslovno znači obrnuto od prijašnjeg ?????
// BLA_PRIMJER.sort( window.by('sirina', true, Number, window.by('visina', false, Number) ) );


// window.by koristim zato što se  ponekad izgubi scope tj kada se ova funkcija poziva unutar nekog loopa 

// ADDITIONAL OPTIONS FOR NATIVE JAVASCRIPT SORT ------- from http://jsfiddle.net/gfullam/sq9U7/  
function by(path, reverse, primer, then) {
  var get = function (obj, path) {
      if (path) {
        path = path.split('.');
        for (var i = 0, len = path.length - 1; i < len; i++) {
          obj = obj[path[i]];
        };
        return obj[path[len]];
      }
      return obj;
    },
    prime = function (obj) {
      return primer ? primer(get(obj, path)) : get(obj, path);
    };

  return function (a, b) {
    var A = prime(a),
      B = prime(b);

    return (
      (A < B) ? -1 :
      (A > B) ? 1 :
      (typeof then === 'function') ? then(a, b) : 0
    ) * [1, -1][+!!reverse];
  };
};


function toggle_global_progress_bar(show) {
  
  if (show) $('#cit_global_progress_bar').addClass('cit_show');
  if (!show) $('#cit_global_progress_bar').removeClass('cit_show');
};


///////////////////////////// ovo je u formatu yyyy-mm-dd hh:mm:ss /////////////////////////////

function getMeADateAndTime(number) {
  var today = null;
  
  if ( typeof number !== 'undefined') {
    today = new Date( number );  
  } else {
    today = new Date();  
  };
  
  var sek = today.getSeconds();
  var min = today.getMinutes();
  var hr = today.getHours();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  // ako je samo jedna znamenka
  if (dd < 10) {
    dd = '0' + dd;
  };
  // ako je samo jedna znamenka
  if (mm < 10) {
    mm = '0' + mm;
  };

  // ako je samo jedna znamenka
  if (sek < 10) {
    sek = '0' + sek;
  };


  // ako je samo jedna znamenka
  if (min < 10) {
    min = '0' + min;
  };

  // ako je samo jedna znamenka
  if (hr < 10) {
    hr = '0' + hr;
  };

  today = yyyy + '-' + mm + '-' + dd;
  time = hr + ':' + min + ':' + sek;

  // EXPERIMENTAL !!!!!!:
  // stavio sam mogućnost da mi upiše datum / vrijeme na svim elementima koji imaju 
  // istu klasu kao ove - tj ako u html-u stoji element sa ovim klasama i ako na onload pokrenem ovu funckiju
  // sadržaj svakog elementa će dobiti datum / vrijeme ovisno o klasi koju ima

  // $(".getMeADate").html(today);
  // $(".getMeATime").html(time);

  return {
    datum: today,
    vrijeme: time

  }

};

function get_time_as_hh_mm_ss(arg_time, from_or_to) {
  
  var time = arg_time;
  // pošto svako početno vrijeme povećavam za 200 milisekundi 
  // sada kada vraćam vrijeme nazad u string moram to dodati nazad !!!!!
  if (from_or_to == 'to') time = time + 200;
  
  var sat = parseInt( time/(60*60*1000) );
  var modulus_sat = time/(60*60*1000) - sat;
  
  var min = parseInt( modulus_sat*60 );
  var modulus_min = modulus_sat*60 - min;
  
  var sek_string = zaokruziIformatirajIznos(modulus_min*60, 0);
  var sek = parseInt(sek_string) || 0;
  if ( sek == 60 ) sek = 59;
  
  if ( String(sat).length < 2 ) sat = '0' + sat;
  if ( String(min).length < 2 ) min = '0' + min;
  if ( String(sek).length < 2 ) sek = '0' + sek;
  
  
   
  var time_string = sat + ':' + min + ':' + sek;
  
  
  
  return time_string;
  
};




/* /////////////////////////   SAMO ZAOKRUŽI BROJ   ///////////////////////////////////// */
function zaokruzi(broj, brojZnamenki) {

 var broj = parseFloat(broj);

 if ( typeof brojZnamenki == 'undefined' ) {
   brojZnamenki = 0;
 };
 var multiplicator = Math.pow(10, brojZnamenki);
 broj = parseFloat((broj * multiplicator).toFixed(11));
 var test =(Math.round(broj) / multiplicator);
 // + znak ispred je samo js trik da automatski pretvori string u broj
 return +(test.toFixed(brojZnamenki));
};




/* /////////////////////////    ZAOKRUŽI I FORMATIRAJ BROJ NA HRVATSKI SISTEM ///////////////////////////////////// */
// --> decimalni zarez i točka za 1000  


function zaokruziIformatirajIznos(num, zaokruzi_na) {
  // primjer : var num = 123456789.56789;

  var brojZnamenki = 2;
  if ( typeof zaokruzi_na !== 'undefined' )  brojZnamenki = parseInt( zaokruzi_na );
  

  var zaokruzeniBroj = zaokruzi(num, brojZnamenki);
  

  var stringBroja = String(zaokruzeniBroj);
  // 123456789
  var lijeviDio = stringBroja.split('.')[0] + "";
  var tockaPostoji = stringBroja.indexOf(".");
  var desniDio = "00";
  if (tockaPostoji > -1) {
    desniDio = stringBroja.split('.')[1] + "";
  };

  /*
  if (Number(desniDio) < 10) {
    desniDio = '0' + Number(desniDio);
  };
  */

  
  var broj_je_pozitivan = true;
  
  // ako je broj negativan 
  if ( lijeviDio.substring(0,1) == '-' ) {
    lijeviDio = lijeviDio.substring(1);
    broj_je_pozitivan = false;
  };
  
  var stringLength = lijeviDio.length;
  
  // ne zanimaju me brojevi s tri znamenke i manje jer oni nemaju točke
  if (stringLength >= 4) {
    // pretvaram string u array
    lijeviDio = lijeviDio.split('');
    // gledam koliko stane grupa po 3 znamaneke
    // i onda uzmem ostatak - to je jednostavna modulus operacija
    var dotPosition = stringLength % 3;
    // zanima me koliko treba biti točaka u broju
    // to dobijem tako da podijelim broj znamenki sa 3 i gledam samo cijeli broj
    var dotCount = parseInt(stringLength / 3)

    // postavim prvu točku
    // NAPOMENA u slučajevima kada je modulus = 0 ova funkcije postavi točku čak i na počatek broja
    // tu točku kasnije obrišem
    // svaka sljedeća točka je UVIJEK 4 pozicije udesno !!!!
    // napomena - logično bi bilo da je svake 3 pozicije udesno, 
    // ali moram brojati i točku koju sam stavio i ona povećava length od stringa
    for (i = 1; i <= dotCount; i++) {
      // stavi točku
      lijeviDio.splice(dotPosition, 0, ".");
      // id na poziciju current + 4 
      dotPosition = dotPosition + 4;
    };
    
    
    // spoji nazad sve znakove u jedan string !!!
    lijeviDio = lijeviDio.join('');

    // kad se dogodi da je modulus === 0 onda postavim točku na nultu poziciju
    // i sad je moram obrisati !!!!!!!
    if (lijeviDio.charAt(0) === ".") {
      // uzimam sve osim prvog znaka tj točke
      lijeviDio = lijeviDio.substring(1);
    };
    
  };

  // ponovi nulu onoliko puta koliko treba da decimale imaju onoliko stringova 
  // koliko je definirano sa argumentom [zaokruzi_na] 
  // ( ... od kojeg gore dobijem varijablu brojZnamenki )
  if (desniDio.length < brojZnamenki) {
    desniDio = desniDio + '0'.repeat(brojZnamenki - desniDio.length);
  };

  
  var formatiraniBroj = lijeviDio + "," + desniDio;

  if ( desniDio == "00" &&  brojZnamenki == 0 )  formatiraniBroj = lijeviDio;
  
  if ( broj_je_pozitivan == false ) formatiraniBroj = '-' + formatiraniBroj;
  
  return formatiraniBroj;
  
};


window.last_geo_timestamp = 0;


function get_user_position(for_ramp) {
  
  window.last_geo_timestamp = null;
  
  return new Promise( function( geo_resolve, geo_reject ) {
    
    var onSuccess = function(position) {

      /*
      ovako izgleda position objekt koji vrati
      {
        Latitude: position.coords.latitude,
        Longitude: position.coords.longitude,
        Altitude: position.coords.altitude,
        Accuracy: position.coords.accuracy,
        Altitude_Accuracy: position.coords.altitudeAccuracy,
        Heading: position.coords.heading,
        Speed: position.coords.speed,
        Timestamp: position.timestamp
      }
      */
      
      window.we_user_lat =  position.coords.latitude;
      window.we_user_lon = position.coords.longitude;
      
      window.last_geo_timestamp = Date.now();
      
      geo_resolve(position);

    };

    var onError = function(error) {
      
      // window.we_user_lat = null;
      // window.we_user_lon = null;
      
      window.last_geo_timestamp = Date.now();
        
      geo_reject({
        code: error.code,
        message: error.message
      });

    };
    
    var we_geo_options = { maximumAge: 5*1000, enableHighAccuracy: true };
    
    navigator.geolocation.getCurrentPosition(onSuccess, onError, we_geo_options);
    
    
    // JAKO BITNO !!!!!!!  -------> AKO ZA 3 SEC NE DOBIJE ODGOVOR OD GPS-a 
    // NAPRAVI REJECT
    setTimeout( function() {
      
      if ( for_ramp && Date.now() - window.last_geo_timestamp > 3*1000 && window.we_user_lat == null ) {
        
        geo_reject({
          code: 'MOJ TIMEOUT',
          message: 'PROŠLO JE 3 SEC DOK SAM ČEKAO GEO LOKACIJU'
        });
        
      };
      
    }, 20*1000);
    
  }); // kraj Promisa
  
  
}; // kraj get user position func




function watch_user_position() {
  
    
  var onSuccess = function(position) {

    /*
    ovako izgleda position objekt koji vrati
    {
      Latitude: position.coords.latitude,
      Longitude: position.coords.longitude,
    }
    */
    
    console.log('WATCH GEO ------------- NAŠAO KOORDINATE ------------- !!!!!!!!!!!!!!!!');

    window.we_user_lat =  position.coords.latitude;
    window.we_user_lon = position.coords.longitude;

    window.last_geo_timestamp = Date.now();
    
    
    var park_list_module = window['js/park_list/park_list_module.js'];
    park_list_module.write_distance_in_all_park_rows();
    

  };

  var onError = function(error) {
    console.log('WATCH GEO POS ERROR ------------- !!!!!!!!!!!!!!!!');
    console.log(error);
  };

  var we_geo_options = { maximumAge: 30*1000, timeout: 20*1000, enableHighAccuracy: true };

  window.we_geo_watch_ID = navigator.geolocation.watchPosition(onSuccess, onError, we_geo_options);

}; // kraj watch user position func





function geo_distance( lat2, lon2 ) {
    
  
  if ( !window.we_user_lat ) return;
  
    var lat1 = window.we_user_lat;
    var lon1 = window.we_user_lon;
  
  
    var dist_object = {
        mil: 0,
        km: 0,
        nautic: 0
      };

    if ((lat1 == lat2) && (lon1 == lon2)) {
      dist_object = {
        mil: 0,
        km: 0,
        nautic: 0
      };  
    } else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;

      var km = null;
      var nautic = null;


      km = dist * 1.609344;
      nautic = dist * 0.8684;

      dist_object = {
        mil: dist,
        km: km,
        nautic: nautic
      };

    };
  
  return dist_object;
   
};


function clear_all_pp_markers() {

  if ( !window.all_we_map_markers) return;

  
  $.each(window.all_we_map_markers, function(marker_index, marker) {
    // kada ste map od markera bude null onda zapravo obrišem marker sa mape !!!!
    window.all_we_map_markers[marker_index].setMap(null);
  });

};



function show_pp_markers() {

//    map: window.we_map,

  wait_for(`window.global_park_list !== null`, function() {


    if ( window.all_we_map_markers.length == 0 ) {

      $.each(window.global_park_list, function(pp_index, park) {

        var pp_Latlng = { lat: park.pp_latitude, lng: park.pp_longitude };

        var marker = new google.maps.Marker({
          position: pp_Latlng,
          custom_we_data: park,
          icon: 'img/google_we_icon.png'
        });

        marker.addListener('click', function(e) {
          // console.log(e);
          // console.log(marker);
          var park_data = marker.custom_we_data;
          var park_list_module = window['js/park_list/park_list_module.js'];
          park_list_module.check_book_i_park_i_open_park_place(park_data);
          
          
          var map_height = $('body').height() - 50;
          $('#map_page').css('height', map_height + 'px');

          setTimeout( function() {
            $('#we_directions_panel_wrapper').removeClass('cit_visible');
          }, 100);

          /*
          window.we_map.setZoom(16);
          window.we_map.setCenter(window.we_user_marker.getPosition());
          */
        });        

        window.all_we_map_markers.push(marker);

      });

    };


    $.each(window.all_we_map_markers, function(marker_index, marker) {
      window.all_we_map_markers[marker_index].setMap(window.we_map);
    });  

  }, 50000);  

};



function init_google_map() {

    window.we_directionsService = new google.maps.DirectionsService();
    window.we_directionsRenderer = new google.maps.DirectionsRenderer();

    var map_Latlng = {lat: 45.79500308999068, lng: 15.973694855983695};

    window.we_map = new google.maps.Map(document.getElementById('map_page'), {
      zoom: 14,
      center: map_Latlng,
      gestureHandling: 'greedy'
    });

    window.we_map.setOptions({draggableCursor:'crosshair'});

    window.we_directionsRenderer.setMap(window.we_map);
  
    window.we_directionsRenderer.setPanel(document.getElementById('we_directions_panel'));
    
  
  
    show_pp_markers();
  
    /*
    window.we_map.addListener('center_changed', function() {
      // 3 seconds after the center of the map has changed, pan back to the
      // marker.
      window.setTimeout(function() {
        map.panTo(marker.getPosition());
      }, 3000);
    });
    */

    /*
    window.we_map.addListener('click', function( event ) {

      if ( this_module.edit_mode == true )  {
        $('#pp_latitude').val( event.latLng.lat() );
        $('#pp_longitude').val( event.latLng.lng() );
      };

    });
    */


  };




function wait_for(condition, callbackFunction, period, expired_callback) {

  var new_wait_for_object = {
      timeStamp: Date.now(),
      condition: condition
  };

  if ( !window.wait_interval_array ) window.wait_interval_array = [];
  window.wait_interval_array.push(new_wait_for_object);
  var interval_id = setInterval(function() {
      check_condition(interval_id, condition, callbackFunction, period, expired_callback);
  }, 30);

  window.wait_interval_array[window.wait_interval_array.length - 1].id = interval_id;

  // if (period == 'inf') {
  return interval_id;
  //};
}; // end of wait_for_component function


function check_condition(interval_id, condition, callbackFunction, period, expired_callback) {
    var waiting_time = 50000;
    if (typeof period !== 'undefined' && period !== 'inf') {
        waiting_time = Number(period);
    };

    var resolved_condition = null;


    /*
    function Date(n){
      return ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"][n%7 || 0];
    }
    function runCodeWithDateFunction(obj){
        return Function('"use strict";return ( function(Date){ return Date(5) } )')()(Date);
    }
    console.log( runCodeWithDateFunction("function(Date){ return Date(5) }") )
    */


    if ($.type(condition) == "function") resolved_condition = condition();
    if ($.type(condition) == "string") resolved_condition = eval(condition);

    if (resolved_condition !== true) {

        /* -------------- if period is 'inf' then dont do this check at all --------------  */
        // effectively this means 
        if (period == 'inf') {

            /* 
            ------------- 
            DONT DO ANY CHECKS IS TIME EXPIRED ---> BECAUSE LISTENER IS INFINITE
            ----------------
            */
        } else {
            // console.log('waiting for condition:    ' + condition + '    to be true.....');
            // just a pointers to delete later
            var remove_this_indexes = [];
            // iterate array of interval object ids
            $.each(window.wait_interval_array, function(index, interval_object) {
                // when I find right id
                if (parseInt(interval_object.id) === parseInt(interval_id)) {
                    // check timestamp to see is expired
                    if (Date.now() - Number(interval_object.timeStamp) > waiting_time) {
                        // if difference is greater then defined period/waiting time
                        // remove this interval
                        clearInterval(interval_id);
                        // save this index for later delete
                        remove_this_indexes.push(index);

                        // console.log('condition  ' + condition + ' DID NOT fulfil IN ' + waiting_time / 1000 + 'sec .. aborting .....');

                        if (typeof expired_callback !== 'undefined') {
                            expired_callback();
                        };
                    };
                };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                window.wait_interval_array.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // END OF if period is NOT 'inf' and it is NOT undefined

    } else {

        // IF CONDITION IS TRUE !!!!!!!!!1



        if (period == 'inf') {

            // if period is 'inf' just run callback function 
            // BUT DONT STOP THE LISTENER !!!!!!!!

            callbackFunction();
        } else {

            var remove_this_indexes = [];

            $.each(window.wait_interval_array, function(index, interval_object) {
                if (parseInt(interval_object.id) === parseInt(interval_id)) {
                    clearInterval(interval_id);

                    // save this index to later remove
                    remove_this_indexes.push(index);

                    callbackFunction();
                };
            });

            $.each(remove_this_indexes, function(ind, index_number) {
                window.wait_interval_array.splice(index_number, 1);
            });

            remove_this_indexes = [];

        }; // end if period is NOT 'inf'

    }; // END IF IF CONDITION IS TRUE

}; 
// end of check_is_loaded function

function delete_wait_for(interval_id) {
  
  if ( !window.wait_interval_array ) return;
  
  
  var remove_this_index = null;
  $.each(window.wait_interval_array, function(index, interval_object) {
    // when I find right id
    if (parseInt(interval_object.id) === parseInt(interval_id)) {
      // remove this interval
      clearInterval(interval_id);
      // save this index for later delete
      remove_this_index = index;
    };
  });
  if ( remove_this_index !== null ) window.wait_interval_array.splice(remove_this_index, 1);
  
};



function cit_place_component(parent_element, component_html, placement) {

    if ( !placement ) parent_element.html(component_html);
    if ( placement == 'prepend' ) parent_element.prepend(component_html);
    if ( placement == 'append' ) parent_element.append(component_html);
    if ( placement == 'after' ) parent_element.after(component_html);
    if ( placement == 'before' ) parent_element.before(component_html);

};
  

function get_cit_module(url, load_css) {

  var prom = $.Deferred();
  
  var url_module_id = url.replace(/\//g, '_');
  url_module_id = url_module_id.replace(/\./g, '_');
  
  var css_module_id = url_module_id.replace(/_js/gi, '_css');
  var css_url = url.replace('.js', '.css');

  
  
  if ( load_css == 'load_css' && $('#' + css_module_id).length == 0 ) {
    var css_link =
    `<link rel="stylesheet" id="${css_module_id}" href="${css_url}">`;
    $("head").append(css_link);
  };
  
  
  if ($('#' + url_module_id).length == 0) {

    $.ajax({
        type: "GET",
        url: url,
        dataType: "script",
        cache: true,
      })
      .done(function (module) {

        var module_code =
`
<script id="${url_module_id}" type="text/javascript">

(function () {
window['${url}'] = {};

var module_url = '${url}';

${module}

$.each(module_object, function(key, item) {
  window['${url}'][key] = item;
});

}());

//# sourceURL=${url}
</script>
`;

        $('body').append(module_code);
        
        // prilikom prvog loadanja pokreni sve funkcije unutar scripts propertija
        wait_for( `typeof  window["${url}"] !== "undefined"`, function() {
          if ( window[url].scripts ) window[url].scripts();
          prom.resolve(window[url]);
        }, 50000);

      })
      .fail(function (err) {
        alert("GREŠKA PRI UČITAVANJU MAIN MODULA !!!!!");
        prom.reject(err);
      });

  }
  else {
    if ( window[url].scripts ) window[url].scripts();
    prom.resolve(window[url]);
  };
    
  return prom;
  
};


function get_we_history() {

  window.we_history = window.localStorage.getItem('we_history');
  
  if ( window.we_history == null ) {
    window.we_history = { states: [ 'park_list_button' ] };
  } else {
    window.we_history = JSON.parse(window.we_history);
  };
    
  return window.we_history;
  
};


function add_we_history(state) {
  
  window.we_history = get_we_history();
  
  var history_length = window.we_history.states.length;
  
  var zadnji_history = window.we_history.states[history_length-1];
  
  if ( window.we_history.states.length >= 10 ) window.we_history.states.shift();
  
  if ( zadnji_history !== state ) window.we_history.states.push(state);
  
  window.localStorage.setItem('we_history', JSON.stringify(window.we_history) );
  
  
};


function we_history_back() {
  
  window.we_history = get_we_history();
  
  if ( window.we_history.states.length > 1 ) {
    
    // prvo odbijem zadnje tj ovo trenuto stanje
    window.we_history.states.pop();
    // onda uzmem id od predzadnjeg stanja ...koje je sada zapravo zadnje
    var last_btn_id = window.we_history.states.pop();
    
    // kliknem na taj id
    $('#' + last_btn_id).click();
    
    window.localStorage.setItem('we_history', JSON.stringify(window.we_history) );
    
  };
  
};

  

function get_obican_user_parking(park) {
  
  
  var zeli_biti_obican_user = false;
  
  var obican_user_parking_array = window.localStorage.getItem('obican_user_parking_array');
  
  if ( obican_user_parking_array !== null ) {
    obican_user_parking_array = JSON.parse(obican_user_parking_array);
    
    $.each(obican_user_parking_array, function(index, pp_sifra) {
      if ( pp_sifra == park.pp_sifra ) zeli_biti_obican_user = true;
    });
    
  };
  
  return zeli_biti_obican_user;
  
};


function set_obican_user_parking(park, state) {
  
  
  var obican_user_parking_array = window.localStorage.getItem('obican_user_parking_array');
  if ( obican_user_parking_array !== null ) {
    obican_user_parking_array = JSON.parse(obican_user_parking_array);
  } else {
    obican_user_parking_array = [];
  };
  

  // AKO VEĆ POSTOJI OVA PP SIFRA ONDA JU OBRIŠI --------------------------
  // to radim uvijek bez obzira jel argumnet state true ili false !!!!! --------------------------
  
  var obrisi_ovaj_index = null;
  $.each(obican_user_parking_array, function(index, pp_sifra) {
    if ( pp_sifra == park.pp_sifra ) obrisi_ovaj_index = index;
  });
  if ( obrisi_ovaj_index !== null ) obican_user_parking_array.splice(obrisi_ovaj_index, 1);
  
  
  // samo ako je state true onda dodaj sifru od parkinga
  if ( state == true ) obican_user_parking_array.push(park.pp_sifra);

  // spremi nove izmjene nazad u local storage
  window.localStorage.setItem('obican_user_parking_array', JSON.stringify(obican_user_parking_array) );
  
  
};



function write_special_tag_avail_count(park_place_arr) {

  var is_specific_park = false;
  var found_customer_tag = null; 
  
  if ( !park_place_arr ) {
    // ako NISAM dao array kao argument onda je array svi parkinzi
    park_place_arr = window.global_park_list;
  } else {
    
    // ako sam explicitno dao array
    // onda prebacim varijablu u true samo da znam ispod dolje da je ovo za specifični parking
    // INAČE ČAK I KAD se radi o jednom specifičnom parkingu 
    // predat cu ga kao item u array-u !!!!!!!!!!!!!!!!!!!!!!!!!!!
    // samo zato da ne mijenjam code ispod koji je loop po arrayu :)
    
    is_specific_park = true;
  };


  $.each( park_place_arr, function( park_index, park ) {  
    
    var vec_parkiran = $('#park_list_row_' + park._id).find('.parking_indicator').length;
    var vec_bookiran = $('#park_list_row_' + park._id).find('.book_indicator').length;
    var is_closed = $('#park_list_row_' + park._id).find('.closed_indicator').length;
    
    var avail_circle_elem = $('#park_list_row_' + park._id).find('.park_available_count'); 
    


    if ( 
      window.cit_user                             &&  
      window.cit_user.customer_tags               &&
      $.isArray(window.cit_user.customer_tags)    &&
      window.cit_user.customer_tags.length > 0    &&
      park.special_places                         &&
      Object.keys(park.special_places).length > 0 
    ) {

      // resetiraj tag match za svaki novi red parkinga u listi
      var tag_match = null;

      // loop po svim special places ( KOJI JE OBJEKT ) unutar jednog parka
      $.each( park.special_places, function( place_tag, spec_place_obj ) {

        // loop da provjerim svaki specijal place sa svakim customer tagom ( KOJI JE ARRAY )
        $.each( cit_user.customer_tags, function( tag_index, tag_obj ) {

          // loop sve dok ne nađem match
          
          
          if ( tag_match == null && place_tag == tag_obj.sifra ) {
            

            // ako jesam našao match onda ne traži dalje !!!!!!
            tag_match = place_tag;
            
            
            
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            
            // spec_place_obj.avail_out = 0;

            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            // JAKO BITNO  !!!!!!! SADA SAM HARDCODIRAO DA NIKAD NEMA MJESTA ZA TELE 2 
            
            
            var avail_count = 
              `
              ${spec_place_obj.avail_out}
              <div class="special_tag">${tag_obj.naziv}</div>
              `;
            
  

            if ( is_specific_park ) {
              // ako sam tražio podatak za specifični park place onda samo returnaj customer tag
              // koji se poklapa sa special place od parkinga
              found_customer_tag = tag_obj.sifra;
              
              
              // ako je npr tele2 parking avail = nula
              // i ako ima mjesta na običnim parkinzima
              if (
                spec_place_obj.avail_out == 0 &&
                park.pp_available_out_num > 0
              ) {
                
                // onda postavi ovaj customer tag ako user nije izabrao da bude običan user
                if ( 
                  get_obican_user_parking(park) == false &&
                  
                  !vec_bookiran && 
                  !vec_parkiran && 
                  !is_closed
                   ) {
                  found_customer_tag = 'zelis_biti_obican_user_umjesto-' + place_tag;
                };
                
                // ako je je user izabrao da bude običan user
                if ( get_obican_user_parking(park) == true ) found_customer_tag = null;
                
              } 
              else if ( spec_place_obj.avail_out > 0 ) {
                
                // ako ima tele2 mjesta onda vrati nazad stanje usera da bude specijalni tele2
                // to radim tako da jednostavno obrišem iz local storage park na kojem je tražio da bude običan user
                
                set_obican_user_parking( park, false );
                
              };
              
            } 
            else {
              
              // ako ovo nije sa jedan specifični parking onda to znači da je ova funkcija pozvana za sve parkinge u listi
              // tada to znači da trebam updateati html  za available count u svakom redu tj za svaki parking
              
              var this_park_row_count_circle = $(`#park_list_row_${park._id} .park_available_count`);
              var already_has_vip = this_park_row_count_circle.find('.vip_user_tag').length;
              
              // ali dodaj tag za grupu samo ako nije VIP 
              // ako je vip onda već ima VIP circle iznad broja
              if ( already_has_vip == 0 ) {
                this_park_row_count_circle.html(avail_count);
              };

              
              // ako je npr tele2 parking avail = nula
              // i ako ima mjesta na običnim parkinzima
              if (
                spec_place_obj.avail_out == 0 &&
                park.pp_available_out_num > 0
              ) {
                
              if ( 
                  get_obican_user_parking(park) == false &&
                  
                  !vec_bookiran && 
                  !vec_parkiran && 
                  !is_closed
                
                ) {
                  popup_become_regular_user(park, tag_obj.sifra); 
                };
              
                if ( get_obican_user_parking(park) == true ) {
                  // obriši crvenu boju ako je do sada bila druga boja u slučaju nula slobodnih mjesta
                  $(`#park_list_row_${park._id} .park_available_count`).css('background-color', '');
                  // ubaci u krug sa brojem slobodnih mjesta broj OBIČNIH MJESTA
                  $(`#park_list_row_${park._id} .park_available_count`).html(park.pp_available_out_num);
                };
                
              } 
              else if ( spec_place_obj.avail_out > 0 ) {
                // ako ima mjesta od tele2
                // obriši ovaj parking iz liste u kojoj user želi biti običan
                set_obican_user_parking( park, false );
                
              };
              
            }; // kraj jel jedan specifični park ili je cijela lista

          }; // kraj jel nasao match u tagovima od usera i u special places u parkovima
        }); // loop po customer tags
      }); // loop po park special places

    }; // provjera jel user ima tags i jel park ima special places

  }); // loop po svim parkinzima
  
  return found_customer_tag;


};


function get_data_of_clicked_park_row(clicked_pp_id) {

  if ( !window.global_park_list ) {
    console.error( "park list global array is missing !!!!!!!! " );
    return;
  };

  var clicked_pp_data = null;
  $.each(window.global_park_list, function(park_place_index, pp_object) {
    if ( pp_object._id == clicked_pp_id ) clicked_pp_data = pp_object;
  });

  return clicked_pp_data
};

function color_all_avail_circles_for_zero() {
  
    // ovo su krugovi u kojima je broj slobodnih mjesta
  $('.park_available_count').each( function() {
    var clone = $(this).clone();
    // ovo je div sa specijal tagom kao npr tele2 i sl ... 
    // kojeg moram obrisati ako postoji tako da uzmem samo broj unutar kruga
    clone.find('div').remove();
    var count = Number( clone.text().replace(/\r?\n|\r/g, '').replace(/\s/g,'' ) );
    console.log(count);
    
    if ( count == 0 ) {
      $(this).css('background-color', '#e8412f');
    } else {
      $(this).css('background-color', '#06b7ff');
    };

  }); 
  
};


function setup_park_page_GUI(response, park) {
  
  var is_booking = null;
  

  if ( response.booking.length > 0 ) { 
    var booking_obj = response.booking[0];
    set_booking_state( true, park, booking_obj); 
    is_booking = true;
  } 
  else { 
    set_booking_state( false, park );
    is_booking = false; 
  };

  if ( response.parking.length > 0 ) { 
    var parking_obj = response.parking[0];
    set_parking_state( true, park, parking_obj, is_booking); 
  } 
  else {
    set_parking_state( false, park, parking_obj, is_booking);
  };
  
  color_all_avail_circles_for_zero();
  
          
  setTimeout( function() {
    if ( window.global_park_list ) {
      $.each( window.global_park_list, function( park_index, park ) {
        get_user_status(park, 'create_html');
      });
    };
  }, 300);
  

};


function setup_park_list_GUI(response) {
  
  var we_booking_in_progress = [];
  var we_park_in_progress = [];
  
  if ( !window.global_park_list ) return;
  
  $.each( window.global_park_list, function( park_index, park ) {
    
    $.each( response.booking, function( book_index, booking_obj ) {
      if ( booking_obj.pp_sifra == park.pp_sifra ) we_booking_in_progress.push(park.pp_sifra);
    });
    
    $.each( response.parking, function( book_index, parking_obj ) {
      if ( parking_obj.pp_sifra == park.pp_sifra ) we_park_in_progress.push(park.pp_sifra);
    });
    
  }); 
  
  $.each( window.global_park_list, function( park_index, park ) {
    
    // u ovoj funkciji pozivam i get user status i u njemu KREIRAM VIP TAGOVE !!!!!
    // u ovoj funkciji pozivam i get user status i u njemu KREIRAM VIP TAGOVE !!!!!
    setup_pp_state_indicator(we_booking_in_progress, we_park_in_progress, park);
  }); 
  
  // zatim kreiram sve tagove za pojedine grupe npr STED, ZCZZ itd
  write_special_tag_avail_count();
  
  
  color_all_avail_circles_for_zero();

};


function setup_pp_state_indicator(we_booking_in_progress, we_park_in_progress, park) {
  
  
  var pp_state_indicator = '';

  /* ---------------START---------------- OVO JE DIO ZA BOOK INDIKATORE  ------------------------------- */ 
  var reservation_indicator = '';
  if ( $.inArray( park.pp_sifra, we_booking_in_progress) > -1 ) {
    reservation_indicator = `<div class="book_indicator">RESERVED <i class="fas fa-hourglass-half"></i></div>`;
  };
  /* ----------------END--------------- OVO JE DIO ZA BOOK INDIKATORE  ------------------------------- */ 


  /* ---------------START---------------- OVO JE DIO ZA PARK IN PROGRESS INDIKATORE  ------------------------------- */ 
  var parking_indicator = '';
  if ( $.inArray( park.pp_sifra, we_park_in_progress) > -1 ) {
    parking_indicator = `<div class="parking_indicator">PARKED <i class="fas fa-parking"></i></div>`;
  };
  /* ----------------END--------------- OVO JE DIO ZA PARK IN PROGRESS INDIKATORE  ------------------------------- */ 


  
  /* ---------------START---------------- OVO JE DIO ZA PARK CLOSED INDIKATORE  ------------------------------- */ 
  var closed_indicator = '';
  
  var user_status = get_user_status(park, 'create_html');
  
  if ( park.pp_current_price == null && user_status !== 'owner' ) {
    closed_indicator = `<div class="closed_indicator">CLOSED <i class="fas fa-ban"></i></div>`;
  };

  /* ----------------END--------------- OVO JE DIO ZA PARK CLOSED INDIKATORE  ------------------------------- */ 


  if ( !closed_indicator ) {
    // ako nije closed
    if ( parking_indicator ) {
      // ako je parkiran na tome parkiralištu
      pp_state_indicator = parking_indicator;
    } else {
      // ako nije parkiran ----> provjeri jel rezerviran parking
      if ( reservation_indicator ) pp_state_indicator = reservation_indicator;
    };

  } else {
    pp_state_indicator = closed_indicator;
  };  
  
  
  var indicator_postavljen = false;
  
  if ( parking_indicator ) {
    pp_state_indicator = parking_indicator;
    indicator_postavljen = true;
  };
  
  
  if ( !indicator_postavljen && closed_indicator ) {
    pp_state_indicator = closed_indicator;
  };
  
  if ( reservation_indicator ) {
    pp_state_indicator = reservation_indicator;
  };
  
 
  var jel_vec_ima_book_indicator = $(`#park_list_row_${park._id}`).find('.book_indicator').length;
  var jel_vec_ima_park_indicator = $(`#park_list_row_${park._id}`).find('.parking_indicator').length;
  var jel_vec_ima_closed_indicator = $(`#park_list_row_${park._id}`).find('.closed_indicator').length;
  
  
  // ubaci book indicator samo ako već nije ubačen u html
  if ( !jel_vec_ima_book_indicator && pp_state_indicator == reservation_indicator ) {
    $(`#park_list_row_${park._id} .park_distance`).after(pp_state_indicator);
  };
  
  // ubaci PARK indicator samo ako već nije ubačen u html
  if ( !jel_vec_ima_park_indicator && pp_state_indicator == parking_indicator ) {
    $(`#park_list_row_${park._id} .park_distance`).after(pp_state_indicator);
  };
  
  // ubaci CLOSED indicator samo ako već nije ubačen u html
  if ( !jel_vec_ima_closed_indicator && pp_state_indicator == closed_indicator ) {
    $(`#park_list_row_${park._id} .park_distance`).after(pp_state_indicator);
  };  
  
  
  
};


function get_active_booking_or_parking( park, user_num ) {

  
  return new Promise( function( resolve, reject ) { 

    if ( !user_num ) {
      console.error('ERROR get active book an park ------> nema argumenta user num !!!!');
      reject(false);
      return;
    };

    $.ajax({
        type: 'GET',
        url: (window.remote_we_url + '/get_active_booking_or_parking/' + park.pp_sifra + '/' + user_num),
        dataType: "json"
    })
    .done(function(response) {

        console.log(response);

        if ( response.success == true ) {

          // nije potrebno da radim ovo odmah jer trebam sačekati animaciju tj da se uopće generira HTML !!!!!
          // if ( park.pp_sifra !== 'all' ) setup_park_page_GUI(response, park);

          if ( park.pp_sifra == 'all' ) setup_park_list_GUI(response);
          
          resolve(response);

        } else {

          reject(false);

          if ( response.msg && typeof window[response.msg] == 'function' ) {
            window[response.msg](); 
          }
          else {
            // ako msg nije nikakav popup koji sam prepoznao onda pokreni ovaj općeniti popup !!!!
            popup_greska_prilikom_parkinga();
          };

        };
    })
    .fail(function(error) {
      
      console.error("Error on get active book and park func !!!");

      if ( window[error.msg] ) {
        window[error.msg]();
      } else {
        // ako msg nije nikakav popup koji sam preposnao onda pokreni ovaj općeniti popup !!!!
        popup_greska_prilikom_parkinga();
      };
      
      reject(error);

    }); // kraj ajax

  }); // kraj promisa
  
};

 

function write_pp_state_in_local_storage(pp_type, state, park ) {
  
  window[pp_type] = window.localStorage.getItem(pp_type);
  
  if ( window[pp_type] == null ) {
    window[pp_type] = [];
  } else {
    window[pp_type] = JSON.parse(window[pp_type]);
  };
  
  var current_pp_state_index = $.inArray( park.pp_sifra, window[pp_type]); 
  
  if ( state === true ) {
    // ubaci id o trenutnog parkirališta u array za booking
    // samo ako već nije unutra !!!!!!
    if ( current_pp_state_index == -1 )  window[pp_type].push(park.pp_sifra);
  };
  
  if ( state === false ) {
    if ( current_pp_state_index > -1 ) window[pp_type].splice(current_pp_state_index, 1);
  };
  
    
  window.localStorage.setItem(pp_type, JSON.stringify( window[pp_type] ));
  
};


function get_user_status(park, create_html) {
  
  
  var user_status = '';
    
    if ( 
      cit_user                           && /* ako postoji user*/
      cit_user.roles                     && /* ako postoji roles u useru */
      cit_user.roles.pp_vip              && /* ako postoji prop pp owner */ 
      $.isArray( cit_user.roles.pp_vip ) && /* ako je pp owner array */
      cit_user.roles.pp_vip.length > 0 /* ako array nije prazan */
    ) {
      
      if ( $.inArray( park.pp_sifra, cit_user.roles.pp_vip) > -1 ) {
        
        user_status = 'vip';

        if ( create_html == 'create_html' ) {
          var this_park_row_count_circle = $('#park_list_row_' + park._id + ' .park_available_count');
          var already_has_vip = this_park_row_count_circle.find('.vip_user_tag').length;

          if ( already_has_vip == 0 ) {
            var vip_tag = `<div class="vip_user_tag">VIP</div>`;
            this_park_row_count_circle.prepend(vip_tag);
          };
        };
        
      };
      
    };
  
  
    
    if ( 
      cit_user                            &&
      cit_user.roles                      &&
      cit_user.roles.free_pp              &&
      $.isArray( cit_user.roles.free_pp ) &&
      cit_user.roles.free_pp.length > 0
    ) {
      
      if ( $.inArray( park.pp_sifra, cit_user.roles.free_pp) > -1 ) user_status = 'free';
      
    };  
  
  
  
    if ( 
      cit_user                             &&
      cit_user.roles                       &&
      cit_user.roles.super_admin
    ) {
      
      if ( cit_user.roles.super_admin == true ) user_status = 'super';
      
    };  
  
  
  return user_status;
  
};


function show_rampa_nije_otvorena_button() {
  
  $('#select_book_duration').addClass('cit_hide').removeClass('cit_show');
  $('#book_timer').addClass('cit_hide').removeClass('cit_show');
  $('#ramp_not_open_btn').addClass('cit_show').removeClass('cit_hide');
  
};


// wrepao sam ovo u funkciju koja vraća funkiciju 
// tako da zapečem neke varijable prilikom pozivanja
function wrapper_for_ramp_not_open ( 
  recorded_time,
  exit_pp_function, 
  park_data,
  enter_pp_function,
  curr_action_tag,
  barrier_name,
  barrier_state 
) {

    
    return function() { 

    // ako sam dobio neku poruku od RPI rampe onda nemoj prikazivati ovaj popup !!!!  
    if ( window.error_msg_from_RPI !== false ) return;
      
    // kada dođe vrijeme i ako uopće dođe do toga 
    // ova funkcija će usporediti zapečenu varijablu iz argumenta sa globalnom window varijablom

    // događalo se da se rampa ne otvori, user ode na listu i onda na neki posve drugi parking page i onda mu ja prikažem button
    // RAMPA SE NIJE OTVORILA ? jer se pokrenula fail callback funkcija 

    // AKO USER ODE SA PARK PAGE STRANICE NAZAD NA LISTU NAPRAVIO SAM DA SE GLOBALNA VARIABLA RESETIRA NA NULL
    // tako da onda sigurno ne može biti ista kao zapečena varijabla
    // NA TAJ NAČIN OSIGURAVAM DA SE BUTTON U SLUČAJU NEOTVARANJA RAMPE POJAVI SAMO ZA ODOVARAJUĆU INSTANCU KADA JE POZVAN
    // A NE KASNIJE ZA NEKO POSVE DRUGO PARKIRALIŠTE
    if ( 
      window.ble_is_off == false &&
      recorded_time == window.last_open_ramp_instance_time 
    ) {
      
      show_rampa_nije_otvorena_button(); 
      
      set_button_in_progress_state(false, '#exit_pp_btn');
      set_button_in_progress_state(false, '#enter_pp_btn');
      
      
      // u local storage down je true, a up je false
      if ( barrier_name && barrier_state == 'down' ) {
        
        
      };
      
      if ( exit_pp_function ) {
                                     /* (exit_pp_func, park_data, enter_pp_func, current_action_tag ) */
        popup_pass_pp_iako_ramp_not_open(exit_pp_function, park_data, null, null);
      };
      
      if ( enter_pp_function ) {
                                     /* (exit_pp_func, park_data, enter_pp_func, current_action_tag ) */
        popup_pass_pp_iako_ramp_not_open(null, park_data, enter_pp_function, curr_action_tag );
      };
      
      
    }; // kraj ifa ako radi ble i ako je isti timestamp tj ista instanca otvaranja
    

  }; // kraj return func

}; // kraj func wrapper for not open




function wrapper_for_barrier_not_open ( 
  recorded_time,
  exit_pp_function, 
  park_data,
  enter_pp_function,
  curr_action_tag,
  barrier_name,
  barrier_state 
) {

  return function() { 

    
    if ( 
      window.ble_is_off == false &&
      recorded_time == window.last_open_ramp_instance_time 
    ) {
      
      show_rampa_nije_otvorena_button(); 
      
      set_button_in_progress_state(false, '#open_barrier_btn');
      set_button_in_progress_state(false, '#close_barrier_btn');
      
      if ( exit_pp_function ) {
                                     /* (exit_pp_func, park_data, enter_pp_func, current_action_tag ) */
        popup_pass_pp_iako_ramp_not_open(exit_pp_function, park_data, null, null);
      };
      
      if ( enter_pp_function ) {
                                     /* (exit_pp_func, park_data, enter_pp_func, current_action_tag ) */
        popup_pass_pp_iako_ramp_not_open(null, park_data, enter_pp_function, curr_action_tag );
      };
      
      
    }; // kraj ifa ako radi ble i ako je isti timestamp tj ista instanca otvaranja
    

  }; // kraj return func

}; // kraj func wrapper for not open





function set_booking_state( set_booking, park, booking_obj ) {
  
  
  // write_pp_state_in_local_storage('we_booking_in_progress', set_booking, park );

  $('#ramp_not_open_btn').addClass('cit_hide').removeClass('cit_show');
  $('#book_and_stop_book_box').css('height', '60px');
  
  if ( set_booking ) {
    
    $('#book_pp_btn').addClass('cit_hide').removeClass('cit_show');
    $('#book_pp_btn').css('pointer-events', 'none');

    $('#cancel_booking_btn').addClass('cit_show').removeClass('cit_hide');
    $('#cancel_booking_btn').css('pointer-events', 'all');

    $('#book_timer').addClass('cit_show').removeClass('cit_hide');
    $('#book_timer').text( 'RESERVATION STARTED' );

    $('#select_book_duration').addClass('cit_hide').removeClass('cit_show');



    var time_passed = Date.now() - booking_obj.reservation_from_time;
    var time_in_min = Math.ceil( time_passed / (1000*60) );
    var price_so_far = time_in_min * (booking_obj.current_price/60);
    var time_string = get_time_as_hh_mm_ss(time_passed);
      // odreži sekunde !!!!
    time_string = time_string.substring(0, time_string.length - 3);


    var w = window;
    $('#book_timer').html( 
      
      `<span id="book_time_passed_span" style="margin-right: 10px;">
        ${w.we_trans( 'id__book_time_passed_span', w.we_lang) }
      </span>` + 
      time_string + 
      `<span id="book_spent_span" style="margin: 0 10px 0 30px;">
        ${w.we_trans( 'id__book_spent_span', w.we_lang) }
      </span>` +
      zaokruziIformatirajIznos(price_so_far) + '&nbsp;' +
      booking_obj.currency 

    );



    /*

    var vrijeme_isteka = booking_obj.user_set_book_duration*60*60*1000 + booking_obj.reservation_from_time;
    var jos_ostalo_vremena =  vrijeme_isteka - Date.now(); 
    var time_string = get_time_as_hh_mm_ss(jos_ostalo_vremena);
      // odreži sekunde !!!!
    time_string = time_string.substring(0, time_string.length - 3);

    $('#book_timer').html( 'do isteka: ' + time_string + '&nbsp;&nbsp; cijena: ' + zaokruziIformatirajIznos(booking_obj.reservation_paid) + ' ' + booking_obj.currency );

    */  



    // just to be sure remove book indicator and parking indicator if any present 
    $('#park_place_info .book_indicator').remove();
    $('#park_place_info .parking_indicator').remove();

    var reservation_indicator = `<div class="book_indicator">RESERVED <i class="fas fa-hourglass-half"></i></div>`;

    $('#park_place_info .park_distance_and_state').append(reservation_indicator);


  } 
  else {
      
    $('#cancel_booking_btn').addClass('cit_hide').removeClass('cit_show');
    $('#cancel_booking_btn').css('pointer-events', 'none');
    
    $('#book_pp_btn').addClass('cit_show').removeClass('cit_hide');
    $('#book_pp_btn').css('pointer-events', 'all');
    
    $('#book_timer').addClass('cit_hide').removeClass('cit_show');
    $('#select_book_duration').addClass('cit_show').removeClass('cit_hide');
    
    
    $('#park_place_info .book_indicator').remove();

  };

};


function set_parking_state( set_parking, park, parking_object, set_booking ) {
  

  // write_pp_state_in_local_storage('we_park_in_progress', set_parking, park );


  $('#ramp_not_open_btn').addClass('cit_hide').removeClass('cit_show');
  
  // ako postoji sifra ovog parkinga u local array

  // if ( $.inArray( park.pp_sifra, window.we_park_in_progress) > -1 ) {
  if ( set_parking ) {
    
    
    $('#enter_pp_btn').addClass('cit_hide').removeClass('cit_show');
    $('#enter_pp_btn').css('pointer-events', 'none');

    $('#exit_pp_btn').addClass('cit_show').removeClass('cit_hide');
    $('#exit_pp_btn').css('pointer-events', 'all');

    
    $('#book_pp_btn').addClass('cit_hide').removeClass('cit_show');
    $('#book_pp_btn').css('pointer-events', 'none');

    $('#cancel_booking_btn').addClass('cit_hide').removeClass('cit_show');
    $('#cancel_booking_btn').css('pointer-events', 'none');

    $('#book_timer').addClass('cit_hide').removeClass('cit_show');
 
    $('#select_book_duration').addClass('cit_hide').removeClass('cit_show');

    
    $('#book_and_stop_book_box').css('height', '30px');
    
        
  var time_passed = Date.now() - parking_object.from_time;
  var time_in_min = Math.ceil( time_passed / (1000*60) );
  var price_so_far = time_in_min * (parking_object.current_price/60);
  var time_string = get_time_as_hh_mm_ss(time_passed);
  // odreži sekunde !!!!
  time_string = time_string.substring(0, time_string.length - 3);
  
  var timer_html = 

    `
    ${time_string}
    <br>
    <span style="font-size: 16px;line-height: normal;color: #034072;">
    ${ zaokruziIformatirajIznos(price_so_far) + ' ' + parking_object.currency }
    </span>

    `;  
    
  $('#parking_timer').html( timer_html );
  $('#parking_timer_box').addClass('cit_show').removeClass('cit_hide');
    
  if ( $(window).height() < 600 ) {
    $('#park_place_info #park_place_row_data').css('top', '40px');
  } else {
    $('#park_place_info #park_place_row_data').css('top', '5px');
  };


  // just to be sure remove book indicator and parking indicator if any present 
  $('#park_place_info .book_indicator').remove();
  $('#park_place_info .parking_indicator').remove();

  var parking_indicator = `<div class="parking_indicator">PARKED <i class="fas fa-parking"></i></div>`;
  $('#park_place_info .park_distance_and_state').append(parking_indicator);
 
    
    
  } 
  else {
    
    
    // $('#select_book_duration').addClass('cit_show').removeClass('cit_hide');
    
    $('#book_and_stop_book_box').css('height', '60px');
    
    
    if ( !set_booking ) {
      $('#book_pp_btn').addClass('cit_show').removeClass('cit_hide');
      $('#book_pp_btn').css('pointer-events', 'all');
    };
      
    $('#exit_pp_btn').addClass('cit_hide').removeClass('cit_show');
    $('#exit_pp_btn').css('pointer-events', 'none');
    
    
    $('#enter_pp_btn').addClass('cit_show').removeClass('cit_hide');
    $('#enter_pp_btn').css('pointer-events', 'all');
    
    $('#parking_timer_box').addClass('cit_hide').removeClass('cit_show');
    
    $('#park_place_info #park_place_row_data').css('top', '5px');
    
    
    $('#parking_timer').text('');
    
    $('#park_place_info .parking_indicator').remove();
    
  };

};


function set_blink_me_on_map(bar_name) {
  // removaj blink_me class sa svih malih ikona barijera na mapi
  $('.bar_btn_on_map').removeClass('blink_me');
  
  $('#'+bar_name).addClass('blink_me');

  
};


function set_barrier_html(bar_name, new_state) {
    
  // ako treba biti spuštena prikaži crveni gumb i samo kada je PARKED
  if ( new_state == 'down' ) { 

    $('#close_barrier_btn').css('display', 'flex');
    $('#close_barrier_btn').addClass('cit_show').removeClass('cit_hide');
    $('#close_barrier_btn').css('pointer-events', 'all');

    $('#open_barrier_btn').addClass('cit_hide').removeClass('cit_show');
    $('#open_barrier_btn').css('pointer-events', 'none');
    $('#open_barrier_btn').css('display', 'none');

  } 
  else {

    // ako je treba biti podignuta prikaži plavi gumb
    // TAKOĐER AKO JE BARIJERA BOOKED ISTO PRIKAŽI PLAVI GUMB !!!!
    // user može vidjeti da je barijeru bookirao po crvenom prikazu vremena gore iznad gumba

    $('#open_barrier_btn').css('display', 'flex');
    $('#open_barrier_btn').addClass('cit_show').removeClass('cit_hide');
    $('#open_barrier_btn').css('pointer-events', 'all');

    $('#close_barrier_btn').addClass('cit_hide').removeClass('cit_show');
    $('#close_barrier_btn').css('pointer-events', 'none');
    $('#close_barrier_btn').css('display', 'none');
    
    
  };
  
  /*
  
  // zatim zapiši ovo stanje u local storage!
  window.barriers_states = window.localStorage.getItem('barriers_states');

  // ako uopće ne postoji win varijabla onda ju napravi !!!
  if ( window.barriers_states ) {
    window.barriers_states = JSON.parse(window.barriers_states);
  } else {
    window.barriers_states = {};
  };
  
  if ( window.barriers_states == null ) window.barriers_states = {};
  
  window.barriers_states[arg_bar_name] = new_state;

  window.localStorage.setItem('barriers_states', JSON.stringify(window.barriers_states) );
  
  */

};


function get_barrier_state(arg_bar_name) {

  var this_bar_state = false;

  window.barriers_states = window.localStorage.getItem('barriers_states');

  if ( window.barriers_states !== null ) {

    window.barriers_states = JSON.parse(window.barriers_states);
    $.each( window.barriers_states, function( bar_name_key, bar_state ) {
      if ( bar_name_key == arg_bar_name ) this_bar_state = bar_state;
    });

  };

  return this_bar_state;
  
  
};



function get_db_barrier_state( pp_sifra, user_number ) {
  
  var def = $.Deferred();
  
  $.ajax({
    type: 'GET',
    url: (window.remote_we_url + '/get_db_barrier_state/' + pp_sifra + '/' + parseInt(user_number) ),
    dataType: "json"
  })
  .done(function(response) {
    
    if ( response.success !== true) {
      
      if ( response.msg && typeof window[response.msg] == 'function' ) {
        window[response.msg]();
      } else {
        popup_barrier_not_open();
      };
      
    };
    
    def.resolve(response);

  })
  .fail(function(error) {
    console.error("Error on get db barrier state !!!!!");
    def.reject(error);
  });
    
  return def;
  
};


function update_db_barrier_state( pp_sifra, bar_name, user_number, new_state ) {
  
  var def = $.Deferred();
  
  
  $.ajax({
    type: 'GET',
    url: (window.remote_we_url + '/update_db_barrier_state/' + pp_sifra + '/' + bar_name + '/' + parseInt(user_number) + '/' + new_state ),
    dataType: "json"
  })
  .done(function(response) {
    
    def.resolve(response);

  })
  .fail(function(error) {
    console.error("Error on get db barrier state !!!!!");
    def.reject(error);
  });
    
  return def;
  
};


function create_ms_at_zero_and_week_day(add_days) {

  var date = new Date();
  var yyyy = date.getFullYear();
  var mnt = date.getMonth();
                              // plus or minus days    
  var day = date.getDate() + (add_days || 0 );

  // nedjelja je 0 ---> subota je 6
  var week_day = date.getDay();

  var day_at_zero = new Date(yyyy, mnt, day, 0, 0, 0, 0);
  var ms_at_zero = Number(day_at_zero); //  day_at_zero.getMilliseconds();

  return {
    ms_at_zero: ms_at_zero,
    week_day: week_day
  };

};


function set_button_in_progress_state(state, button_id) {
  
  
  if ( state ) {
  
    $(button_id).css('pointer-events', 'none' );
    $(button_id).css('opacity', '0.9' );
    $(button_id + ' i').not('.fa-cog').css('display', 'none');
    $(button_id).find('.fa-cog').css('display', 'block');
    
  } else {
    
    $(button_id).css('pointer-events', 'all' );
    $(button_id).css('opacity', '1' );
    $(button_id + ' i').not('.fa-cog').css('display', 'block');
    $(button_id).find('.fa-cog').css('display', 'none');
    
  }
  
};


function set_count_number(diff) {
  
  var count_element = $('#park_place_row_data .park_available_count');
  var current_text = count_element.text();
  
  var special_tag = $('#park_place_row_data .park_available_count .special_tag')
  var vip_user_tag = $('#park_place_row_data .park_available_count .vip_user_tag')
  
  
  count_element.addClass('big');
  
  setTimeout(function() {
    
    var next_num = null;

    if ( special_tag.length > 0 ) {
      
      var special_tag_text = special_tag.text();
      
      var current_num = Number( current_text.replace(special_tag_text, '') );
      next_num = current_num + diff;
      
      var new_html = 
      `
      ${next_num}
      <div class="special_tag">${special_tag_text}</div>
      `;    

      count_element.html(new_html);
      count_element.removeClass('big');        
      
      
    } 
    else if ( vip_user_tag.length > 0 ) {
      
      
      var vip_tag_text = vip_user_tag.text();
      var current_num = Number( current_text.replace(vip_tag_text, '') );
      next_num = current_num + diff;
      
      var new_html = 
      `
      ${next_num}
      <div class="vip_user_tag">${'VIP'}</div>
      `;    

      count_element.html(new_html);
      count_element.removeClass('big');        
      
      
    } 
    else {

      var current_num = Number(current_text);
      next_num = current_num + diff;
      
      count_element.text(next_num);
      count_element.removeClass('big');  
      
    }
    
    
    if ( next_num == 0 ) {
      count_element.css('background-color', '#e8412f');
    } else {
      count_element.css('background-color', '#06b7ff');
    };
    
    
  }, 400);
  
};


function get_user_business_data_and_start_payment(is_token_payment) {
  
  $.ajax({
    type: 'GET',
    url: window.remote_we_url + '/get_user_business_data/' + cit_user.user_number,
    dataType: "json"
  })
  .done(function(response) {
    
    if ( response.success == true ) {
      
      window.cit_user.oib = response.oib;
      window.cit_user.business_name = response.business_name;
      window.cit_user.business_address = response.business_address;
      
      window.localStorage.setItem('cit_user', JSON.stringify(window.cit_user) );
      
      if ( is_token_payment ) {
        
        
        // ----------------- TOKEN PAY VIŠE NIJE KAO PRIJE -----------------
        // ----------------- TOKEN PAY VIŠE NIJE KAO PRIJE -----------------
        // ----------------- TOKEN PAY VIŠE NIJE KAO PRIJE -----------------
        
        // 10.11.2020 sam promijenio pay with token da bude isti kao i ws form ali samo dodam token  i token number (na serveru)
        // mogu još koristiti stari token način na serveru ali samo ako uzimam useru na rate
        
        // popup_enter_pass_for_token();
        
        popup_ask_for_r1(true);
        
      } else {
        
        popup_ask_for_r1(false);
        
      };
      
    };   

  })
  .fail(function(error) {
    console.error("Error on get user business data !!!!!");
  });

}; // end of user login





function car_plate_html(car_plate_text, selector) {


    var menu_sufix = '';
    if ( selector == 'menu' ) menu_sufix = 'menu_';    

  var car_plate_row = 
`
<div class="reg_plate" id="reg_plate_row_${menu_sufix + car_plate_text}">
<div style="float: left; width: 80%; text-align: center;">${car_plate_text}</div>
<div  class="account_form_btn delete_reg_plate_btn" 
      id="delete_reg_plate_btn_${menu_sufix + car_plate_text}">

  <i class="fas fa-minus"></i>

</div>
</div>
`;
  return car_plate_row

};


function global_add_car_plate(selector, plate_string) {

  var menu_sufix = '';
  var plates_input = '#user_reg_plate';
  var place_plates_after = '#reg_plate_label_and_input_box';

  if ( selector == 'menu' ) {
    menu_sufix = 'menu_';
    plates_input = '#user_reg_plate_menu';
    place_plates_after = '#reg_plate_label_and_input_in_menu';
  };

  var car_plate_text = '';
  
  
  // ako sam dobio argumnet 
  if ( plate_string ) { 
    // ovaj regex znači ostavi samo alfanumeric ali ipak ostavi i hrvatske znakove !!!
    car_plate_text = plate_string.replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '');
  } else {
    // ako čitam string iz input polja
    // ovaj regex znači ostavi samo alfanumeric ali ipak ostavi i hrvatske znakove !!!
    car_plate_text = $(plates_input).val().replace(/(?!ć|Ć|č|Č|š|Š|đ|Đ|ž|Ž)\W/g, '');
  };
  

  // moram dodatno obrisati underscore jer iznekog razloga on se zvanično tretira kao alphanumeric ????? !!!!!!!
  car_plate_text = car_plate_text.replace(/\_/g, '');
  car_plate_text = car_plate_text.toUpperCase();

  if ( car_plate_text == '' ) return;

  // obriši tekst u inputu
  $(plates_input).val('');

  
  var user_car_plates = window.localStorage.getItem('user_car_plates');
  if ( user_car_plates ) user_car_plates = JSON.parse(user_car_plates);


  // ako uopće nema ništa u propertiju car plates u local storage
  // onda stavi da bude prazan array
  if ( !user_car_plates || $.isArray(user_car_plates) == false  ) {
    user_car_plates = [];
  };
  
  var car_plate_already_exists = false;
  $.each(user_car_plates, function(car_plate_index, plate_obj) {
    if ( plate_obj.reg == car_plate_text ) car_plate_already_exists = true;
  });
  
  
  function generate_reg_html() {
    
    var row_html = car_plate_html(car_plate_text, selector);
    $(place_plates_after).after(row_html);

    setTimeout(function() {
      $(`#reg_plate_row_${menu_sufix + car_plate_text}`).addClass('cit_show');
      $(`#delete_reg_plate_btn_${menu_sufix + car_plate_text}`).on( 'click', global_delete_car_plate(selector) );
    }, 200);
    
  };
  
  
  // upisuj u array samo ako nije forsani string tj samo ako user upisuje
  // i ako već ne postoji u local storage
  
  if ( plate_string ) {
    generate_reg_html();
  };
  
  
  if ( !plate_string && car_plate_already_exists == false ) {
    user_car_plates.push({ time: Date.now(), reg: car_plate_text });
    window.localStorage.setItem('user_car_plates', JSON.stringify(user_car_plates));
    generate_reg_html();
  };
  
  // samo ako mu nisam dao explicitno plate string
  // to znači da user ručno upisuje i onda mu nedaj jer će se poduplati
  if ( !plate_string && car_plate_already_exists == true ) {
    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
    var popup_text = window.we_trans( 'vec_upisana_rega', window.we_lang);
    // show_popup, popup_text, popup_progress, show_buttons
    show_popup_modal(true, popup_text, null, false);
    setTimeout(function() { show_popup_modal(false, popup_text, null, false); }, 2500);
  };

  // samo ako je ovo ručni upis tj ako funkciji nisam dao reg kao argument
  if ( !plate_string ) $(plates_input).trigger('click').trigger('focus');

  if ( window.cit_user && window.update_car_plates_in_db == true ) {
    update_car_plates_on_ramps( cit_user.user_number, user_car_plates );
  };
  
};

 
function check_jel_nova_rega(user_number, reg) {
  
  
  if ( !user_number ) user_number = 'none';
  
  var def = $.Deferred();
  
  $.ajax({
    type: 'GET',
    url: ( window.remote_we_url + '/check_jel_nova_rega/' + user_number + '/' + reg ),
    dataType: "json"
  })
  .done(function(response) {
    
    if ( response.success == true ) {
      
      def.resolve(true);
      
    } else {
      if ( response.msg && typeof window[response.msg] == 'function' ) window[response.msg]();
      def.resolve(false);
    };   

  })
  .fail(function(error) {
    console.error("Error check_jel_vec_postoji_reg !!!!!");
    console.log(error);
    def.reject(false);
  });
  
  return def;

}; // end of user login





function global_delete_car_plate(selector) {

  return function() {

    var menu_sufix = '';
    if ( selector == 'menu' ) menu_sufix = 'menu_';


    var car_plate_id = this.id.replace(`delete_reg_plate_btn_${menu_sufix}`, '');


    var user_car_plates = window.localStorage.getItem('user_car_plates');
    if ( user_car_plates ) user_car_plates = JSON.parse(user_car_plates);

    // ako uopće nema ništa u propertiju car plates
    if ( !user_car_plates || $.isArray(user_car_plates) == false  ) {
      user_car_plates = [];
    };


    var delete_this_index = null;
    $.each(user_car_plates, function(car_plate_index, plate_obj) {
      if ( plate_obj.reg == car_plate_id ) delete_this_index = car_plate_index;
    });

    if ( delete_this_index !== null ) {
      user_car_plates.splice(delete_this_index, 1);
      $(`#reg_plate_row_${menu_sufix + car_plate_id}`).removeClass('cit_show');
      setTimeout( function() {  $('#reg_plate_row_' + menu_sufix + car_plate_id ).remove(); }, 200);
    };
    
    // ako je array ali je prazan onda nek bude null
    if ( $.isArray(user_car_plates) && user_car_plates.length == 0 ) user_car_plates = null;
    
    // ako je undefined nek bude null !!!
    if ( !user_car_plates ) user_car_plates = null;

    window.localStorage.setItem('user_car_plates', JSON.stringify(user_car_plates));
    
    if ( window.cit_user ) update_car_plates_on_ramps( cit_user.user_number, user_car_plates );

  };

};


window.cit_user_cached = null;
window.last_cit_user_update_timestamp = 0;


function update_user_from_db(user_number) {
  
  window.user_update_done = false;
  
  return new Promise( function( resolve, reject ) { 
  

    // ako sam već napravio ovaj request u manje od 2 sec !!!
    if ( Date.now() - window.last_cit_user_update_timestamp < 2*1000 ) {
      // uzmi iz cache-a
      window.cit_user = window.cit_user_cached;
      console.log(' -------------------- uzimam cache od USER cached -------------------- ')
      resolve(true);
      return;
    };  


    $.ajax({
        headers: {
            'X-Auth-Token': cit_user.token
        },
        type: "GET",
        url: window.remote_we_url + '/update_user_from_db/' + user_number,
        dataType: "json"
    })
    .done(function(response) {

      if ( response.success == true ) {

        if ( window.cit_user ) {

          response.user.token = window.cit_user.token;
          response.user.success = window.cit_user.success;
          response.user.token_expiration = window.cit_user.token_expiration;

          window.cit_user = response.user;
          window.cit_user_cached = window.cit_user;
          
          window.last_cit_user_update_timestamp = Date.now();

          window.localStorage.setItem('cit_user', JSON.stringify(window.cit_user));
          console.log('-----> USER UPDATE-an !!!!!!!!! ------> SVE JE OK ');

          resolve(true);

        } else {

          console.error('USER NIJE UPDATE-an !!!!!!!!! ------> window.cit_user NE POSTOJI ');
          reject(null);
        };

      } 
      else {

        console.error('USER NIJE UPDATE-an !!!!!!!!! ------> response  success == false ');
        reject(null);
      };

      window.user_update_done = true;

    })
    .fail(function(error) {

      console.error('USER NIJE UPDATE-an !!!!!!!!! ------> ajax fail ');
      console.error(error);
      window.user_update_done = true;
      reject(null);

    }); // kraj ajaxa

  }); // kraj promisa
  
}; // kraj update user form db

function generate_car_plate_list(selector) {
  
  wait_for( `window.cit_user !== null && typeof window.cit_user !== 'undefined'`, function() {
    
        if ( selector == 'menu' ) {
            
          // prvo removaj sve redove za svaki slučaj da se ne duplaju
          $(`#reg_plate_menu_section .reg_plate`).remove();

          update_user_from_db(window.cit_user.user_number).then(
            function(resolved) {  

              if ( 
                resolved == true && 
                window.cit_user &&
                $.isArray(window.cit_user.car_plates) 
              ) {

                if ( window.cit_user.car_plates.length == 0 ) window.cit_user.car_plates = null;
                
                if ( window.cit_user.car_plates ) {

                  window.localStorage.setItem('user_car_plates', JSON.stringify(window.cit_user.car_plates) );
                  window.update_car_plates_in_db = false;
                  $.each( window.cit_user.car_plates, function(plate_ind, plate_obj ) {
                    global_add_car_plate(selector, plate_obj.reg );
                  }); 
                  
                };

                setTimeout( function() { window.car_plate_menu_list_in_progress = false; }, 400);
                
              } else {
                
                setTimeout( function() { window.car_plate_menu_list_in_progress = false; }, 400);
                
              }; // ako postoji user i ako ima car plates

            },
            function(rejected) {
              setTimeout( function() { window.car_plate_menu_list_in_progress = false; }, 400);
              console.log( ' NIJE USPIO UZETI USERA IZ BAZE' );
            }
          ); // end of then

        } 
        else {
          
          // prvo removaj sve redove za svaki slučaj da se ne duplaju
          $(`#reg_plate_sub_section .reg_plate`).remove();

          // ako je  ovo za registraciju tj selector je registracija !!!

          var user_car_plates = window.localStorage.getItem('user_car_plates');
          if ( user_car_plates ) user_car_plates = JSON.parse(user_car_plates);

          // ako uopće nema ništa u propertiju car plates
          if ( !user_car_plates || $.isArray(user_car_plates) == false  ) {
            user_car_plates = [];
          };

          window.update_car_plates_in_db = false;
          $.each( user_car_plates, function(plate_ind, plate_obj ) {
            global_add_car_plate(selector, plate_obj.reg );
          }); 
          
          setTimeout( function() { window.car_plate_registration_list_in_progress = false; }, 400);

        };
        
    

  }, 20*1000 );  

}; // end of generate car plate list



function update_car_plates_on_ramps(user_number, car_plates) {

  var update_obj = {
    user_number: user_number,
    car_plates: car_plates
  };  

  if ( !cit_user ) return;

  $.ajax({
    headers: {
        'X-Auth-Token': cit_user.token
    },
    type: 'POST',
    url: (window.remote_we_url + '/save_new_user_plates'),
    data: JSON.stringify(update_obj),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    })
  .done(function(response) {

    console.log('odgovor save new user plates');  
    console.log(response);

    if ( response.success == true ) {

      
      var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
      var popup_text = window.we_trans( 'plates_updated_on_all_ramps', window.we_lang);
      // show_popup, popup_text, popup_progress, show_buttons
      show_popup_modal(true, popup_text, null, false);
      // sakrij popup nakon 1 sec
      setTimeout(function() { show_popup_modal(false, popup_text, null, false); }, 2500);

      
      window.cit_user = window.localStorage.getItem('cit_user');
      window.cit_user = JSON.parse(window.cit_user);
      window.cit_user.car_plates = car_plates;
      window.localStorage.setItem('cit_user', JSON.stringify(window.cit_user));
      
      
    } else {

      var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
      var popup_text = window.we_trans( 'error_on_saving_plates', window.we_lang);

      /*
      show_popup_modal('error', popup_text, null, 'ok' );
      $('#cit_popup_modal_OK').off('click');
      $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
      */
    };
  })
  .fail(function(error) {

    console.log(error);
    var show_popup_modal = window['js/popup_modal/cit_popup_modal.js'].show_popup_modal;
    var popup_text = window.we_trans( 'error_on_saving_plates', window.we_lang);
    
    /*
    // show_popup, popup_text, popup_progress, show_buttons
    show_popup_modal('error', popup_text, null, 'ok' );
    $('#cit_popup_modal_OK').off('click');
    $('#cit_popup_modal_OK').on('click', function() { show_popup_modal(false, popup_text, null, 'ok' ); });
    */

  });

}; 



